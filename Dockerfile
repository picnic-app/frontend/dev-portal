FROM node:lts-alpine AS build

WORKDIR /build

COPY package*.json ./
RUN npm ci --quiet

COPY . .
RUN npx nx run devportal:server:production


FROM node:lts-alpine

ENV NODE_ENV=production

WORKDIR /app

COPY --from=build /build/dist/apps/devportal ./dist/apps/devportal

EXPOSE 3000

CMD ["node", "dist/apps/devportal/server/main.js"]

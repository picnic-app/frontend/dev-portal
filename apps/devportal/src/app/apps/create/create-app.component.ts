import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';


import { App, CreateAppInput } from '@portal/gql';
import { AppsService, fromAppsApi } from '@portal/data';


@Component({
  selector: 'devportal-create-app',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatDialogModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule
  ],
  templateUrl: './create-app.component.html',
  styleUrls: ['./create-app.component.scss']
})
export class CreateAppComponent implements OnInit {

  readonly nameMinLen = 4;
  readonly nameMaxLen = 48;

  form: FormGroup = new FormGroup({});

  userConsent = false;

  constructor(
    private store: Store,
    private apps: AppsService,
    private router: Router,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CreateAppComponent>
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: [null, [Validators.required, Validators.minLength(this.nameMinLen), Validators.maxLength(this.nameMaxLen)]],
      userConsent: [null, [Validators.required]]
    });
  }

  onConsent(value: boolean) {
    this.userConsent = value;
  }

  onSave() {
    if (!this.form.valid) {
      return console.error(`form state is not valid`);
    }
    const name = this.form.get('name')?.value;
    const input: CreateAppInput = {
      name: name,
      url: 'https://pods.picnic.zone/pods/picnic/hello',
      description: name,
      imageUrl: 'https://picnic-devportal-fju3grnwqq-uc.a.run.app/assets/img/picnic-logo.svg'
    };

    this.apps.createApp(input).subscribe({
      next: (app: App) => {
        this.store.dispatch(fromAppsApi.createAppSuccess({app}));
        this.router.navigate(['/apps', app.id])
          .catch(err => console.error(err));
      },
      error: (err: any) => this.store.dispatch(fromAppsApi.createAppFailure({err})),
      complete: () => this.dialogRef.close()
    });
  }
  onClose() {
    this.dialogRef.close();
  }
}

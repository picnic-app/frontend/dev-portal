import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { App } from '@portal/gql';

@Component({
  selector: 'devportal-app-item',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './app-item.component.html',
  styleUrls: ['./app-item.component.scss']
})
export class AppItemComponent {

  @Input()
  app?: App;
}

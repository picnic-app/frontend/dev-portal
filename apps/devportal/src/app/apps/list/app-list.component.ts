import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
import {Router} from '@angular/router';

import {App} from '@portal/gql';
import {AppItemComponent} from './app-item/app-item.component';

@Component({
  selector: 'devportal-app-list',
  standalone: true,
  imports: [CommonModule, AppItemComponent, MatToolbarModule],
  templateUrl: './app-list.component.html',
  styleUrls: ['./app-list.component.scss']
})
export class AppListComponent {

  @Input()
  title = 'Applications';

  @Input()
  list: App[] | null = [];

  constructor(private router: Router) {
  }

  onItemClick(app: App) {
    this.router.navigate(['apps', app.id])
      .catch(err => console.error(err));
  }
}

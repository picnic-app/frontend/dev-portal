import { createFeature, createReducer, on } from '@ngrx/store';
import { AppPageActions } from './app-page.actions';
import { deleteAppFailure, deleteAppSuccess } from '../../../../../../../libs/data/src/lib/api/apps';


export const appPageFeatureKey = 'appPage';

export interface State {
  pending: boolean;
}

export const initialState: State = {
  pending: false
};

export const reducer = createReducer(
  initialState,

  on(AppPageActions.deleteApp, state => ({
    ...state,
    pending: false
  })),

  on(deleteAppSuccess, state => ({
    ...state,
    pending: false
  })),

  on(deleteAppFailure, state => ({
    ...state,
    pending: false
  }))
);

export const appPageFeature = createFeature({
  name: appPageFeatureKey,
  reducer
});


import { createActionGroup, emptyProps, props } from '@ngrx/store';

import { App } from '@portal/gql';


export const AppPageActions = createActionGroup({
  source: 'App/Page',
  events: {
    'Delete App': props<{ appID: string }>(),
    'Delete App Confirmation': props<{ app: App }>(),
    'Delete App Confirmation Dismiss': emptyProps()
  }
});

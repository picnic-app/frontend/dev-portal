import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { exhaustMap, map, tap } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { AppActions, fromAppsApi } from '@portal/data';
import { AppPageActions } from './app-page.actions';
import { DeleteAppConfirmationDialogComponent } from '../delete-app-confirmation-dialog.component';

@Injectable()
export class AppPageEffects {

  deleteConfirmation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AppPageActions.deleteApp),
      exhaustMap(({appID}) => {
        const dialogRef = this.dialog.open<
          DeleteAppConfirmationDialogComponent,
          undefined,
          boolean
        >(DeleteAppConfirmationDialogComponent);

        return dialogRef.afterClosed().pipe(
          map((result) =>
            result
              ? fromAppsApi.deleteApp({id: appID})
              : AppPageActions.deleteAppConfirmationDismiss()
          )
        );
      })
    )
  );

  deleteSuccess$ = createEffect(() =>
      this.actions$.pipe(
        ofType(fromAppsApi.deleteAppSuccess),
        tap(() => {
          this.router.navigate(['/apps'])
            .catch(console.error);
        })
      ),
    {dispatch: false}
  );

  deleteFailure$ = createEffect(() =>
      this.actions$.pipe(
        ofType(fromAppsApi.deleteAppFailure),
        tap(() => {
          // todo: redundant, remove
        })
      ),
    {dispatch: false}
  );

  constructor(private actions$: Actions, private dialog: MatDialog, private router: Router) {
  }
}

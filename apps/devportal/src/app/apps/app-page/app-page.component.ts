import { Component, OnInit } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';
import { filter, map, Observable } from 'rxjs';

import { App, AppTag } from '@portal/gql';
import { AppsService, fromAppTagsApi, fromAppTagsData, fromAppsApi, fromAppsData } from '@portal/data';
import { AppFormComponent } from './app-form/app-form.component';
import { AppPageActions } from './+state';
import { SubscriptionsComponent } from '../../subscriptions/subscriptions.component';
import { PermissionsComponent } from '@portal/root';

@Component({
  selector: 'devportal-app-page',
  standalone: true,
  imports: [
    CommonModule,
    AppFormComponent,
    RouterLink,
    NgOptimizedImage,
    MatToolbarModule,
    MatButtonModule,
    SubscriptionsComponent,
    PermissionsComponent
  ],
  templateUrl: './app-page.component.html',
  styleUrls: ['./app-page.component.scss']
})
export class AppPageComponent implements OnInit {

  appID: string | null;

  readonly app$: Observable<App | undefined> = this.store.select(fromAppsData.selectAll).pipe(
    filter(() => !!this.appID),
    map(apps => apps.find(app => app.id == this.appID))
  );

  readonly appTags$: Observable<AppTag[] | undefined> = this.store.select(fromAppTagsData.selectAll);

  constructor(private route: ActivatedRoute, private apps: AppsService, private store: Store) {
    this.appID = this.route.snapshot.paramMap.get('app_id');
  }

  ngOnInit() {
    this.store.dispatch(fromAppsApi.loadApps());
    this.store.dispatch(fromAppTagsApi.loadAppTags());
  }

  onDelete() {
    console.log(`delete the app ${this.appID}`);
    if (this.appID) {
      this.store.dispatch(AppPageActions.deleteApp({appID: this.appID}));
    }
  }
}

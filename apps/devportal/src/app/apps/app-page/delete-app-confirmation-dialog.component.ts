import { Component } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';

/**
 * The dialog will close with true if user clicks the ok button,
 * otherwise it will close with undefined.
 */
@Component({
  standalone: true,
  template: `
    <h2 mat-dialog-title>Delete Pod</h2>
    <mat-dialog-content>Are you sure you want to delete pod?</mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button [mat-dialog-close]="false">Cancel</button>
      <button mat-button [mat-dialog-close]="true">OK</button>
    </mat-dialog-actions>
  `,
  imports: [
    MatDialogModule,
    MatButtonModule
  ],
  styles: [
    `
        :host {
            display: block;
            width: 100%;
            max-width: 300px;
        }

        mat-dialog-actions {
            display: flex;
            justify-content: flex-end;
        }

        [mat-button] {
            padding: 0;
        }
    `
  ]
})
export class DeleteAppConfirmationDialogComponent {
}

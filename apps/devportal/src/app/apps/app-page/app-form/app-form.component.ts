import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule, FormArray } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { Store } from '@ngrx/store';

import { App, AppTag } from '@portal/gql';
import { AppsService } from '@portal/data';


import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { setTagsToApp } from 'libs/data/src/lib/api/apps';

import { saveAppPermissions } from '../../../permissions/+state';
import { PermissionsComponent } from '@portal/root';


@Component({
  selector: 'devportal-app-form',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    MatRadioModule,
    MatDatepickerModule,
    MatDialogModule,
    MatButtonModule,
    PermissionsComponent
  ],
  templateUrl: './app-form.component.html',
  styleUrls: ['./app-form.component.scss']
})
export class AppFormComponent implements OnInit {

  readonly nameMinLen = 4;
  readonly nameMaxLen = 48;
  readonly descriptionMaxLen = 200;

  form: FormGroup = new FormGroup({});

  @Input()
  app!: App;

  @Input()
  appTags!: AppTag[];

  loading = false;

  hasAggChanges = false;

  constructor(private fb: FormBuilder, private store: Store, private apps: AppsService) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: [null, [Validators.required, Validators.minLength(this.nameMinLen), Validators.maxLength(this.nameMaxLen)]],
      description: [null, [Validators.required, Validators.maxLength(this.descriptionMaxLen)]],
      url: [null, [Validators.required]],
      imageUrl: [null, [Validators.required]],
      tags: this.fb.array(this.appTags.map(tag => this.app.tags?.find(t => t.id == tag.id) != undefined)),
    });

    this.patchForm(this.app);
  }

  private patchForm(app: App) {
    if (this.app != null) {
      this.form.patchValue(this.app);
      let tagsArr = <FormArray>this.form.controls["tags"];
      for (let i = 0; i < tagsArr.controls.length; i++) {
        tagsArr.controls[i].patchValue(this.app.tags?.find(t => t.id == this.appTags[i].id) != undefined);
      }
    }
  }

  saveDetails() {
    if (this.app?.id == null) {
      console.error(`app entity is null`);
      return;
    }

    this.loading = true;

    const selectedTags: AppTag[] = this.form.controls['tags'].value
      .map((value: boolean, index: number) => value ? this.appTags[index] : null)
      .filter((value: AppTag | null) => value != null);

    const currentAppTags = this.app.tags ?? [];

    const tagsToBeRemoved = currentAppTags.filter(tag => !selectedTags.some(t => t.id === tag.id));
    const tagsToBeAdded = selectedTags.filter(tag => !currentAppTags.some(t => t.id === tag.id));


    for (let tag of tagsToBeAdded) {
      this.apps.addTagToApp(this.app.id, tag.id).subscribe();
    }
    for (let tag of tagsToBeRemoved) {
      this.apps.removeTagFromApp(this.app.id, tag.id).subscribe();
    }

    this.store.dispatch(setTagsToApp({appId: this.app.id, tags: selectedTags}));

    this.store.dispatch(saveAppPermissions());

    this.loading = false;

    this.apps.updateApp({...<App>this.form.value, tags: undefined, id: this.app.id}).subscribe(app => {
      this.form.markAsPristine();
      this.hasAggChanges = false;
      this.loading = false;
    });
  }
}

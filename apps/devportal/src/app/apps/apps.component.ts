import { Component, importProvidersFrom } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Router, RouterLink } from '@angular/router';

import { CreateAppComponent } from './create/create-app.component';
import { MyAppsComponent } from './my-apps/my-apps.component';
import { AppsService } from '@portal/data';

@Component({
  selector: 'devportal-apps',
  standalone: true,
  imports: [CommonModule, MyAppsComponent, RouterLink, MatButtonModule, MatToolbarModule],
  templateUrl: './apps.component.html',
  styleUrls: ['./apps.component.scss']
})
export class AppsComponent {

  constructor(private appService: AppsService, private router: Router, private dialog: MatDialog) {
  }

  onCreateClick() {
    this.openDialog();
  }

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;

    this.dialog.open(CreateAppComponent, dialogConfig);
  }
}

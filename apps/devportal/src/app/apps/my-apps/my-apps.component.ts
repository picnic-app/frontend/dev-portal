import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppsService, fromAppsApi, fromAppsData } from '@portal/data';
import { App } from '@portal/gql';
import { AppListComponent } from '../list/app-list.component';

@Component({
  selector: 'devportal-my-apps',
  standalone: true,
  imports: [CommonModule, AppListComponent],
  templateUrl: './my-apps.component.html',
  styleUrls: ['./my-apps.component.scss']
})
export class MyAppsComponent implements OnInit {

  apps$!: Observable<App[]>;

  constructor(private appsSvc: AppsService, private store: Store) {
    this.apps$ = this.store.select(fromAppsData.selectAll);
  }

  ngOnInit() {
    this.getApps();
  }

  getApps() {
    this.store.dispatch(fromAppsApi.loadApps());
  }
}

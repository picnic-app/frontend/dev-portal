import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Store } from '@ngrx/store';

import { AppTokenActions, AppTokensService } from '@portal/data';
import { switchMap, tap } from 'rxjs';

@Component({
  selector: 'devportal-create-token',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  templateUrl: './create-token.component.html',
  styleUrls: ['./create-token.component.scss']
})
export class CreateTokenComponent implements OnInit {

  readonly nameMinLen = 3;
  readonly nameMaxLen = 48;

  form: FormGroup = new FormGroup({});

  readonly appID: string;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: {
                appID: string
              }, private fb: FormBuilder, private dialogRef: MatDialogRef<CreateTokenComponent>,
              private tokens: AppTokensService,
              private store: Store
  ) {
    this.appID = this.dialogData.appID;
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: [null, [Validators.required, Validators.minLength(this.nameMinLen), Validators.maxLength(this.nameMaxLen)]]
    });
  }

  onSave() {
    if (!this.form.valid) {
      return console.error(`form state is not valid`);
    }
    const name = this.form.get('name')?.value;
    this.tokens.generateToken(this.appID, name)
      .pipe(
        switchMap(generated => {
          console.debug(`generated token`, generated);
          return this.tokens.getTokens(this.appID).pipe(
            tap(tokens => console.debug(tokens))
            // by unknown reason `upsertAppToken({token}) removes other tokens from state
            // map(tokens => tokens.find(token => token.tokenID == generated.tokenID))
          );
        })
      )
      .subscribe((tokens) => {
        this.store.dispatch(AppTokenActions.upsertAppTokens({tokens}));
        this.dialogRef.close();
      });
  }

  onClose() {
    this.dialogRef.close();
  }
}

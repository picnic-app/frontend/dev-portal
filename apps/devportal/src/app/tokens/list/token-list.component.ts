import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { AppToken } from "@portal/gql";
import { TokenItemComponent } from './token-item';

@Component({
  selector: 'devportal-token-list',
  standalone: true,
  imports: [CommonModule, TokenItemComponent],
  templateUrl: './token-list.component.html',
  styleUrls: ['./token-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TokenListComponent {

  @Input()
  tokens: AppToken[] | null = []
}

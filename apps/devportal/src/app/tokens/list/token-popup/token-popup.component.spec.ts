import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TokenPopupComponent } from './token-popup.component';

describe('TokenPopupComponent', () => {
  let component: TokenPopupComponent;
  let fixture: ComponentFixture<TokenPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TokenPopupComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TokenPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";

import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'devportal-token-popup',
  standalone: true,
  imports: [CommonModule, FormsModule, MatButtonModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatInputModule, ReactiveFormsModule, MatIconModule],
  templateUrl: './token-popup.component.html',
  styleUrls: ['./token-popup.component.scss'],
})
export class TokenPopupComponent {

  readonly token: string

  constructor(
    @Inject(MAT_DIALOG_DATA) private dialogData: { token: string },
    private clipboard: Clipboard,
    private dialogRef: MatDialogRef<TokenPopupComponent>
  ) {
    console.log(this.dialogData)
    this.token = this.dialogData.token
  }

  onCopy() {
    this.clipboard.copy(this.token)
  }

  onClose() {
    this.dialogRef.close()
  }
}

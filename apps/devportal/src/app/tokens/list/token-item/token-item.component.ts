import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Store } from '@ngrx/store';

import { AppToken } from '@portal/gql';
import { AppTokensService, fromTokensApi } from '@portal/data';
import { TokenPopupComponent } from '../token-popup/token-popup.component';


@Component({
  selector: 'devportal-token-item',
  standalone: true,
  imports: [CommonModule, MatButtonModule],
  templateUrl: './token-item.component.html',
  styleUrls: ['./token-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TokenItemComponent {

  @Input()
  token?: AppToken;

  constructor(private tokensSrv: AppTokensService, private dialog: MatDialog, private store: Store) {
  }

  onIssueClick() {
    if (this.token == undefined) {
      console.error(`token is not defined`);
      return;
    }

    this.tokensSrv.issueToken(this.token.tokenID).subscribe(res => {
      console.log(res);
      if (!res.jwtToken) {
        console.warn(`token is empty in result`);
        return;
      }
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
        token: res.jwtToken
      };
      this.dialog.open(TokenPopupComponent, dialogConfig);
    });
  }

  onRevokeClick() {
    if (!this.token) {
      console.error(`token is not defined`);
      return;
    }
    const tokenID = this.token.tokenID;
    this.tokensSrv.revokeToken(tokenID).subscribe(() => {
      this.store.dispatch(fromTokensApi.revokeTokenSuccess({tokenID}));
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { filter, map, tap } from 'rxjs';
import { isArray } from '@apollo/client/utilities';
import { Store } from '@ngrx/store';

import { fromTokensData, fromTokensApi } from '@portal/data';
import { CreateTokenComponent } from './create/create-token.component';
import { TokenListComponent } from './list/token-list.component';


@Component({
  selector: 'devportal-tokens',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatToolbarModule, TokenListComponent, RouterLink],
  templateUrl: './tokens.component.html',
  styleUrls: ['./tokens.component.scss']
})
export class TokensComponent implements OnInit {

  readonly appID: string | null;

  readonly appTokens$ = this.store.select(fromTokensData.selectAll).pipe(
    tap(all => console.debug(all)),
    filter((tokens) => !!this.appID && isArray(tokens)),
    map(tokens => tokens.filter(token => token.appID == this.appID))
  );

  constructor(
    private route: ActivatedRoute,
    private tokensSvc: fromTokensApi.AppTokensService,
    private dialog: MatDialog,
    private store: Store
  ) {
    this.appID = this.route.snapshot.paramMap.get('app_id');
  }

  onNewToken() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      appID: this.appID
    };
    this.dialog.open(CreateTokenComponent, dialogConfig);
  }

  ngOnInit() {
    if (this.appID == null) {
      return console.error(`appID value is not defined in component/route`);
    }
    this.store.dispatch(fromTokensApi.loadAppTokens({appID: this.appID}));
  }
}

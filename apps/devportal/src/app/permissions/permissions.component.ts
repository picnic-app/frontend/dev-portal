import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
import { filter, take, withLatestFrom } from 'rxjs';
import { Store } from '@ngrx/store';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';


import { fromPermissions } from '@portal/data';
import { AppPermission } from '@portal/gql';
import { PermissionsListComponent } from './permissions-list/permissions-list.component';
import { selectCurrentApp } from '../+state';
import { checkOne, resetAll, uncheckOne } from './+state';
import { getSelectedPermissions, selectAllPermissions } from './+state/permissions.selectors';


@Component({
  selector: 'devportal-permissions',
  standalone: true,
  imports: [CommonModule, PermissionsListComponent, MatButtonModule, MatToolbarModule, RouterLink],
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {

  @Output()
  changed = new EventEmitter<void>();

  app$ = this.store.select(selectCurrentApp);

  readonly allPerms$ = this.store.select(selectAllPermissions);

  readonly selected$ = this.store.select(getSelectedPermissions);

  constructor(private store: Store) {

    this.store.dispatch(fromPermissions.ApiActions.loadAll());
  }

  ngOnInit() {
    this.allPerms$.pipe(
      withLatestFrom(this.app$),
      filter(([_, app]) => !!app),
      take(1)
    ).subscribe(() => this.store.dispatch(resetAll()));
  }

  onChecked(event: { item: AppPermission, checked: boolean }) {
    const id = event.item.id;
    this.store.dispatch(event.checked ? checkOne({id}) : uncheckOne({id}));
    this.changed.emit();
  }
}

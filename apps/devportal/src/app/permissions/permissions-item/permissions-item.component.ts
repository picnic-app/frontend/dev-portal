import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CommonModule } from '@angular/common';
import { MatCheckbox, MatCheckboxChange, MatCheckboxModule } from '@angular/material/checkbox';

import { AppPermission } from '@portal/gql';

@Component({
  selector: 'devportal-permissions-item',
  standalone: true,
  imports: [CommonModule, MatCheckboxModule, MatTooltipModule, FormsModule],
  templateUrl: './permissions-item.component.html',
  styleUrls: ['./permissions-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PermissionsItemComponent {

  @Input()
  item!: AppPermission;

  @Input()
  selected!: boolean;

  @Output()
  checked = new EventEmitter<boolean>();

  @ViewChild('checkbox')
  private checkbox!: MatCheckbox;

  onChange(change: MatCheckboxChange) {
    this.checked.emit(change.checked);
  }
}

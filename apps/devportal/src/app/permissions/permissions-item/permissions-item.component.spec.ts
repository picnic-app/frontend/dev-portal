import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PermissionsItemComponent } from './permissions-item.component';

describe('PermissionsItemComponent', () => {
  let component: PermissionsItemComponent;
  let fixture: ComponentFixture<PermissionsItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PermissionsItemComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PermissionsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

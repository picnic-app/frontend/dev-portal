import { createFeatureSelector, createSelector } from '@ngrx/store';

import { FeatureState, permissionsFeatureKey } from './permissions.reducer';
import { fromPermissions } from '@portal/data';

export const selectPermissionsState =
  createFeatureSelector<FeatureState>(permissionsFeatureKey);

export const selectPermissionsContainerState = createSelector(
  selectPermissionsState,
  state => state.container
);

export const getSelectedPermissions = createSelector(
  selectPermissionsContainerState,
  state => state.selected
);

export const selectPermissionsDataState = createSelector(
  selectPermissionsState,
  (state) => state.permissionsData
);


export const selectPermissionsEntityState = createSelector(
  selectPermissionsDataState,
  state => {
    console.debug(`selectPermissionsEntityState`, state);
    return state[fromPermissions.fromEntity.entityKey];
  }
);

export const selectAllPermissions = createSelector(
  selectPermissionsEntityState,
  fromPermissions.fromEntity.adapter.getSelectors().selectAll
);

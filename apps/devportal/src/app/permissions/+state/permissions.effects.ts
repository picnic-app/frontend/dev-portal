import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { setCurrentApp } from '../../+state/app.actions';
import { map, of, switchMap, tap, withLatestFrom } from 'rxjs';
import { Store } from '@ngrx/store';

import { fromPermissions } from '@portal/data';
import { selectCurrentApp } from '../../+state';
import { getSelectedPermissions } from './permissions.selectors';
import { resetAll, saveAppPermissions, setSelected } from './permissions.actions';


@Injectable({providedIn: 'root'})
export class PermissionsEffects {

  setCurrentApp$ = createEffect(() => this.actions$.pipe(
    ofType(setCurrentApp),
    map(action => action.app),
    map(() => resetAll())
  ));

  resetAll$ = createEffect(() => this.actions$.pipe(
    ofType(resetAll),
    withLatestFrom(this.store.select(selectCurrentApp)),
    map(([action, app]) => {
      if (app && app.permissions) {
        return app.permissions.map(p => p.id);
      } else {
        return [];
      }
    }),
    map((ids) => setSelected({ids}))
  ));

  saveForApp$ = createEffect(() => this.actions$.pipe(
    ofType(saveAppPermissions),
    withLatestFrom(this.store.select(selectCurrentApp), this.store.select(getSelectedPermissions)),
    switchMap(([_, app, selected]) => {

      const appID = app!.id;
      const origin = app!.permissions!.map(p => p.id);

      const toAdd = selected.filter(id => origin.indexOf(id) == -1);
      const toRemove = origin.filter(id => selected.indexOf(id) == -1);

      const actions = [
        ...toAdd.map(id => fromPermissions.ApiActions.addToApp({id, appID})),
        ...toRemove.map(id => fromPermissions.ApiActions.removeFromApp({id, appID}))
      ];

      return of(...actions);
    })
  ));

  constructor(private actions$: Actions, private store: Store) {
  }
}

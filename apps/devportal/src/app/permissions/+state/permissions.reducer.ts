import {
  Action,
  combineReducers,
  createFeature,
  createReducer,
  on
} from '@ngrx/store';

import { fromPermissions } from '@portal/data';
import { checkOne, setSelected, uncheckOne } from './permissions.actions';

const containerKey = 'container';

interface ContainerState {
  selected: string[];
}

const initialContainerState: ContainerState = {
  selected: []
};

const containerReducer = createReducer(
  initialContainerState,

  on(setSelected, (state, {ids}) => ({
    ...state,
    selected: ids
  })),

  on(checkOne, (state, {id}) => {
    const selected = [...state.selected];
    if (selected.indexOf(id) == -1) {
      selected.push(id);
    }
    return {
      ...state,
      selected
    };
  }),

  on(uncheckOne, (state, {id}) => {
    const selected = [...state.selected];
    return {
      ...state,
      selected: selected.indexOf(id) > -1 ? selected.filter(i => i != id) : selected
    };
  })
);

const containerFeature = createFeature({
  name: containerKey,
  reducer: containerReducer
});

export interface FeatureState {
  [containerKey]: ContainerState;
  [fromPermissions.permissionsDataKey]: fromPermissions.PermissionsDataState;
}

export function reducers(state: FeatureState | undefined, action: Action) {
  return combineReducers({
    [containerKey]: containerReducer,
    [fromPermissions.permissionsDataKey]: fromPermissions.reducers
  })(state, action);
}

export const permissionsFeatureKey = 'permissionsFeature';

export const permissionsFeature = createFeature({
  name: permissionsFeatureKey,
  reducer: reducers
});

import { createAction, props } from '@ngrx/store';

export const resetAll = createAction(
  '[Permissions] Reset Selected'
);

export const setSelected = createAction(
  '[Permissions] Set Selected',
  props<{ ids: string[] }>()
);

export const checkOne = createAction(
  '[Permission] Check One',
  props<{ id: string }>()
);

export const uncheckOne = createAction(
  '[Permission] Uncheck One',
  props<{ id: string }>()
);

export const saveAppPermissions = createAction(
  '[Permission] Save For App'
);

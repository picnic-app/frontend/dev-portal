import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppPermission } from '@portal/gql';
import { PermissionsItemComponent } from '../permissions-item/permissions-item.component';

@Component({
  selector: 'devportal-permissions-list',
  standalone: true,
  imports: [CommonModule, PermissionsItemComponent],
  templateUrl: './permissions-list.component.html',
  styleUrls: ['./permissions-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PermissionsListComponent {

  @Input()
  permissions?: AppPermission[] | null;

  @Input()
  selected?: string[] | null;

  @Output()
  checked = new EventEmitter<{ item: AppPermission, checked: boolean }>();

  isSelected(id: string): boolean {
    return this.selected != null && this.selected.indexOf(id) > -1;
  }

  onChecked(item: AppPermission, checked: boolean) {
    this.checked.emit({item, checked});
  }
}

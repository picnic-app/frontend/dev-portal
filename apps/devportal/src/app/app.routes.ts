import { Route } from '@angular/router';

import { authGuard } from '@portal/auth';
import { AppPageComponent } from './apps/app-page/app-page.component';
import { AppsComponent } from './apps/apps.component';
import { TokensComponent } from './tokens/tokens.component';
import { CirclesComponent } from './circles/circles.component';

export const appRoutes: Route[] = [
  {path: '', pathMatch: 'full', redirectTo: 'apps'},
  {
    path: 'apps',
    canActivate: [authGuard],
    children: [
      {path: '', pathMatch: 'full', component: AppsComponent},
      {path: ':app_id', component: AppPageComponent},
      {path: ':app_id/tokens', component: TokensComponent},
      {path: ':app_id/circles', component: CirclesComponent}
    ]
  }
];

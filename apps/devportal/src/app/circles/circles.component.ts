import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { CircleAppsService } from '@portal/data';
import { Circle } from '@portal/gql';
import { MatTableModule } from '@angular/material/table';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { Subscription, auditTime, fromEvent } from 'rxjs';

@Component({
  selector: 'devportal-circles',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatTableModule, MatToolbarModule, MatProgressSpinnerModule, RouterLink],
  templateUrl: './circles.component.html',
  styleUrls: ['./circles.component.scss']
})
export class CirclesComponent implements OnInit, AfterViewInit, OnDestroy {

  readonly appID: string | null;

  @ViewChild('loadingIndicator') loadingIndicator!: ElementRef;

  private scrollSubscription?: Subscription;
  private resizeSubscription?: Subscription;

  cursor = {
    id: '',
    limit: 20
  };

  displayedColumns = ["name", "id"];

  circles: Circle[] = [];
  isLoading = false;
  shouldLoadMore = false;
  hasNextPage = true;

  constructor(
    private route: ActivatedRoute,
    private circleAppsService: CircleAppsService,
  ) {
    this.appID = this.route.snapshot.paramMap.get('app_id');
  }

  ngOnInit() {
    this.loadMore();
  }

  ngAfterViewInit() {
    this.scrollSubscription = fromEvent(window, 'scroll', { capture: true })
        .pipe(auditTime(300))
        .subscribe(() => {
          this.checkLoadingVisible();
        });

      this.resizeSubscription = fromEvent(window, 'resize', { capture: true }).subscribe(() => {
        this.checkLoadingVisible();
      });
  }

  ngOnDestroy(): void {
    this.scrollSubscription?.unsubscribe();
    this.resizeSubscription?.unsubscribe();
    this.scrollSubscription = undefined;
    this.resizeSubscription = undefined;
  }

  private checkLoadingVisible() {
    if (this.loadingIndicator == null) {
      return;
    }

    const rect = this.loadingIndicator.nativeElement.getBoundingClientRect();
    const viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    const topVisible = rect.top >= 0 && rect.top < viewHeight;
    
    if (topVisible) {
      if (this.isLoading) {
        this.shouldLoadMore = true;
      } else {
        this.loadMore();
      }
    }
  }

  private loadMore() {
    if (this.appID == null) {
      return console.error(`appID value is not defined in component/route`);
    }
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;

    this.circleAppsService
      .getCircles(this.appID, this.cursor)
      .subscribe({
        next: (res) => { 
          var newCircles = res.edges.map(edge => edge.node.circle);
          
          this.circles = [...this.circles, ...newCircles.filter(c => c != null).map(c => c!)];
          this.hasNextPage = res.pageInfo.hasNextPage;
          if (this.hasNextPage) {
            this.cursor.id = res.pageInfo.lastId!;
          }
          this.isLoading = false;
          if (this.shouldLoadMore) {
            this.shouldLoadMore = false;
            this.loadMore();
          }
         },
         error: (err) => {
          this.isLoading = false;
          }
      });
  }
}

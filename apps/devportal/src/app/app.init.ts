import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { combineLatest, firstValueFrom, lastValueFrom, map, of, take } from 'rxjs';
import { FirebaseOptions } from 'firebase/app';

import { Config, ConfigService } from '@portal/core';
import { environment } from '../../enviorment';

export function getFirebaseOptions(base: FirebaseOptions, config: Config): FirebaseOptions {
  return <FirebaseOptions>{...base, apiKey: config.fbAPIKey};
}

export function initFirebaseOptions(config: ConfigService): () => Promise<void> {
  console.debug(`initFirebaseOptions start`);
  const source = combineLatest([
    of(<FirebaseOptions>environment.firebase).pipe(take(1)),
    config.config$.pipe(take(1))
  ], getFirebaseOptions).pipe(take(1));

  return () => lastValueFrom(source).then(res => {
    console.log(res);
    console.debug(`initFirebaseOptions finish`);
  });
}

export function initFirebase(opts: FirebaseOptions): () => Promise<void> {
  return () => {
    const fb = of(opts).pipe(
      map(opts => {
        provideFirebaseApp(() => initializeApp(opts));
      })
    );
    return firstValueFrom(fb);
  };
}

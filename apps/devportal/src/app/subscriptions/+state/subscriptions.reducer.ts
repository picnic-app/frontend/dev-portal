import {
  Action,
  combineReducers,
  createFeature,
  createReducer
} from '@ngrx/store';

import { fromSubscriptions } from '@portal/data';

const containerKey = 'container';

interface ContainerState {
  selected: string[];
}

const initialContainerState: ContainerState = {
  selected: []
};

const containerReducer = createReducer(
  initialContainerState
);

const containerFeature = createFeature({
  name: containerKey,
  reducer: containerReducer
});

export interface FeatureState {
  [containerKey]: ContainerState;
  [fromSubscriptions.subscriptionsDataKey]: fromSubscriptions.SubscriptionsDataState;
}

export function reducers(state: FeatureState | undefined, action: Action) {
  return combineReducers({
    [containerKey]: containerReducer,
    [fromSubscriptions.subscriptionsDataKey]: fromSubscriptions.reducers
  })(state, action);
}

export const subscriptionsFeatureKey = 'subscriptionsFeature';

export const subscriptionsFeature = createFeature({
  name: subscriptionsFeatureKey,
  reducer: reducers
});

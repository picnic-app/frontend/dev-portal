import { Component } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatMenuModule } from "@angular/material/menu";
import { RouterLink } from "@angular/router";
import { LetDirective } from "@ngrx/component";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";

import { AuthUser, AuthActions, selectUser } from "@portal/auth";

@Component({
  selector: 'devportal-user-bar',
  standalone: true,
  imports: [CommonModule, MatMenuModule, MatCardModule, MatButtonModule, LetDirective, RouterLink, NgOptimizedImage],
  templateUrl: './user-bar.component.html',
  styleUrls: ['./user-bar.component.scss'],
})
export class UserBarComponent {

  user$: Observable<AuthUser | null> = this.store.pipe(
    select(selectUser)
  )

  constructor(private store: Store) {}

  logout() {
    this.store.dispatch(AuthActions.logout())
  }
}

import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatListModule } from '@angular/material/list';
import { MatSidenav, MatSidenavModule } from '@angular/material/sidenav';
import { RouterLink } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'devportal-sidenav',
  standalone: true,
  imports: [CommonModule, MatListModule, MatSidenavModule, RouterLink, MatIconModule],
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {

  @Input() mobile!: boolean;

  @Input() open = false;
  @Output() closeMenu = new EventEmitter<void>();

  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  toggle() {
    this.sidenav.toggle()
      .catch(console.error);
  }
}

import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideHttpClient } from '@angular/common/http';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideRouter, withEnabledBlockingInitialNavigation } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { metaReducers, ROOT_REDUCERS } from './state.config';

import { provideState, provideStore } from '@ngrx/store';
import { LetDirective } from '@ngrx/component';
import { EffectsModule, provideEffects } from '@ngrx/effects';
import { FIREBASE_OPTIONS } from '@angular/fire/compat';
import { ApolloModule } from 'apollo-angular';

import { APP_CONFIG, Config, ENV_CONFIG, provideCore } from '@portal/core';
import { MaterialModule } from '@portal/material';
import { DataModule } from '@portal/data';
import { appRoutes } from './app.routes';

import { environment } from '../../enviorment';


import { getFirebaseOptions } from './app.init';

import { authFeature, AuthModule } from '@portal/auth';
import { provideRouterStore, RouterState } from '@ngrx/router-store';
import { GraphQLModule } from '@portal/graphql';


import { appsFeature } from '../../../../libs/data/src/lib/entity/app';
import { appTokensFeature } from '../../../../libs/data/src/lib/entity/token';
import { appPageFeature } from './apps/app-page/+state/app-page.reducer';
import { AppPageEffects } from './apps/app-page/+state';
import { appTagsFeature } from 'libs/data/src/lib/entity/app-tag';
import { layoutFeature } from './+state';
import { permissionsFeature } from './permissions/+state/permissions.reducer';
import { AppEffects } from './+state/app.effects';
import { PermissionsEffects } from './permissions/+state/permissions.effects';


export const appConfig: ApplicationConfig = {
  providers: [
    {provide: ENV_CONFIG, useValue: environment},

    provideHttpClient(),

    provideCore({configUrl: ['/config.json']}),

    {
      provide: FIREBASE_OPTIONS,
      useFactory: (env: any, cfg: Config) => {
        return getFirebaseOptions(env.firebase, cfg);
      },
      deps: [ENV_CONFIG, APP_CONFIG]
    },

    importProvidersFrom(
      LetDirective,
      ApolloModule,
      GraphQLModule.forRoot(),
      DataModule,
      ReactiveFormsModule,
      MaterialModule
    ),

    provideRouter(appRoutes, withEnabledBlockingInitialNavigation()),

    importProvidersFrom(
      EffectsModule.forRoot([])
    ),

    provideStore(ROOT_REDUCERS, {
      metaReducers,
      runtimeChecks: {
        strictStateSerializability: true,
        strictActionSerializability: true,
        strictActionWithinNgZone: true,
        strictActionTypeUniqueness: true
      }
    }),

    provideState(layoutFeature),
    provideState(authFeature),
    provideState(appsFeature),
    provideState(appTokensFeature),
    provideState(appTagsFeature),
    provideState(appPageFeature),
    provideState(permissionsFeature),

    provideEffects(AppPageEffects),
    provideEffects(AppEffects, PermissionsEffects),

    provideRouterStore({
      routerState: RouterState.Minimal
    }),

    // there's a bug in store devtools that forces local storage to be wiped out on navigation
    // provideStoreDevtools({
    //   logOnly: true
    // }),

    importProvidersFrom(
      AuthModule
    ),

    provideAnimations()
  ]
};

import { CommonModule } from '@angular/common';
import { ChangeDetectorRef, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { HeaderComponent } from './header/header.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import * as fromRoot from './state.config';
import { LayoutActions } from './+state';

@Component({
  standalone: true,
  imports: [CommonModule, RouterModule, HeaderComponent, SidenavComponent, MatSidenavModule],
  selector: 'devportal-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  mobileQuery: MediaQueryList;

  private readonly _mobileQueryListener: () => void;

  showSidenav$: Observable<boolean>;


  constructor(private store: Store, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.showSidenav$ = this.store.select(fromRoot.selectShowSidenav);
    this.showSidenav$.subscribe(value => {
      console.log(value)
    })
  }

  toggleSidenav() {
    this.store.dispatch(LayoutActions.toggleSidenav());
  }

  closeSidenav() {
    this.store.dispatch(LayoutActions.closeSidenav());
  }

  openSidenav() {
    this.store.dispatch(LayoutActions.openSidenav());
  }
}

import { Component, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { Observable } from 'rxjs';

import * as fromAuth from '@portal/auth';

import { UserBarComponent } from '../user-bar/user-bar.component';


@Component({
  selector: 'devportal-header',
  standalone: true,
  imports: [CommonModule, UserBarComponent, MatIconModule, MatButtonModule],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Output() openMenu = new EventEmitter<void>();
  @Output() toggleMenu = new EventEmitter<void>();

  loggedIn$: Observable<boolean>;

  constructor(private store: Store) {
    this.loggedIn$ = this.store.select(fromAuth.selectLoggedIn);
  }
}

import { createAction, props } from '@ngrx/store';

import { App } from '@portal/gql';

export const setCurrentAppID = createAction(
  '[App] Set Current AppID',
  props<{ appID: string | null }>()
);

export const setCurrentApp = createAction(
  '[App] Set Current App',
  props<{ app: App | null }>()
);

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { routerNavigatedAction } from '@ngrx/router-store';
import { exhaustMap, filter, map, take, tap, withLatestFrom } from 'rxjs';

import { selectRouteData } from '@portal/root';
import { setCurrentApp, setCurrentAppID } from './app.actions';
import { fromAppEntity } from '@portal/data';
import { selectCurrentAppID } from './app.reducer';

const getRouterParams = (route: ActivatedRouteSnapshot) => {
  if (route.children.length == 0) {
    return route.params;
  }

  const combinedChildParams: any = route.children.reduce(
    (prev, childRoute) => ({...prev, ...getRouterParams(childRoute)}),
    {}
  );
  return {
    ...route.params,
    ...combinedChildParams
  };
};

@Injectable({providedIn: 'root'})
export class AppEffects {

  setCurrentAppID$ = createEffect(() => this.actions$.pipe(
    ofType(routerNavigatedAction),
    withLatestFrom(this.store.select(selectRouteData)),
    tap(([{payload}]) => {
      const params = getRouterParams(payload.routerState.root);
    }),
    map(([{payload}]) => {
      const params = getRouterParams(payload.routerState.root);
      const appID = params['app_id'];
      return setCurrentAppID({appID: appID ?? null});
    })
  ));

  setCurrentApp$ = createEffect(() => this.actions$.pipe(
    ofType(setCurrentAppID),
    exhaustMap(() => this.store.select(fromAppEntity.selectAll)),
    withLatestFrom(this.store.select(selectCurrentAppID)),
    tap(data => console.debug(`setCurrentAppID data`, data)),
    map(([apps, appID]) => apps.find(app => app.id == appID)),
    filter(app => !!app),
    take(1),
    map(app => setCurrentApp({app: app!}))
  ));

  constructor(private store: Store, private actions$: Actions) {
  }
}

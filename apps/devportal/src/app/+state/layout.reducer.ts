import { createFeature, createReducer, on } from '@ngrx/store';
import { LayoutActions } from './index';

export const layoutFeatureKey = 'layout';

export interface LayoutState {
  showSidenav: boolean;
}

const initialState: LayoutState = {
  showSidenav: false
};

export const layoutReducer = createReducer(
  initialState,

  on(LayoutActions.toggleSidenav, (state) => ({
    showSidenav: !state.showSidenav
  })),

  on(LayoutActions.closeSidenav, () => ({
    showSidenav: false
  })),

  on(LayoutActions.openSidenav, () => ({
    showSidenav: true
  }))
);

export const layoutFeature = createFeature({
  name: layoutFeatureKey,
  reducer: layoutReducer
});

export const selectShowSidenav = (state: LayoutState) => state.showSidenav;

import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { setCurrentAppID } from './app.actions';
import { fromAppsData } from '@portal/data';

export const appRootFeatureKey = 'appRoot';

export interface AppState {
  appID: string | null;
}

const initialState: AppState = {
  appID: null
};

export const appReducer = createReducer(
  initialState,

  on(setCurrentAppID, (state: AppState, {appID}) => ({
    ...state,
    appID: appID
  }))
);

// export const selectLayoutState = createFeatureSelector<fromLayout.LayoutState>(
//   fromLayout.layoutFeatureKey
// );
//
// export const selectShowSidenav = createSelector(
//   selectLayoutState,
//   fromLayout.selectShowSidenav
// );

export const selectAppState = createFeatureSelector<AppState>(
  appRootFeatureKey
);

export const selectCurrentAppID = createSelector(
  selectAppState,
  state => state.appID
);

export const selectCurrentApp = createSelector(
  fromAppsData.selectAll,
  selectCurrentAppID,
  (apps, appID) => {
    if (appID) {
      const app = apps.find(app => app.id == appID);
      if (app) {
        return app;
      }
    }
    return null;
  }
);

import {
  ActionReducer,
  Action,
  ActionReducerMap,
  MetaReducer,
  createFeatureSelector, createSelector
} from '@ngrx/store';
import {
  getRouterSelectors,
  routerReducer,
  RouterReducerState
} from '@ngrx/router-store';


import { InjectionToken, isDevMode } from '@angular/core';
import { localStorageSync } from 'ngrx-store-localstorage';
import { authFeatureKey } from '@portal/auth';
import * as fromLayout from './+state/layout.reducer';
import * as fromAppRoot from './+state/app.reducer';

export interface State {
  [fromLayout.layoutFeatureKey]: fromLayout.LayoutState;
  [fromAppRoot.appRootFeatureKey]: fromAppRoot.AppState;
  router: RouterReducerState<any>;
}

export const ROOT_REDUCERS = new InjectionToken<
  ActionReducerMap<State, Action>
>('Root reducers token', {
  factory: () => ({
    [fromLayout.layoutFeatureKey]: fromLayout.layoutReducer,
    [fromAppRoot.appRootFeatureKey]: fromAppRoot.appReducer,
    router: routerReducer
  })
});

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return result;
  };
}

export function localStorageSyncReducer(reducer: ActionReducer<State>): ActionReducer<State> {
  return localStorageSync({
    keys: [authFeatureKey],
    rehydrate: true,
    storage: localStorage
  })(reducer);
}

export const metaReducers: MetaReducer<State>[] = isDevMode() ? [logger, localStorageSyncReducer] : [localStorageSyncReducer];

export const selectLayoutState = createFeatureSelector<fromLayout.LayoutState>(
  fromLayout.layoutFeatureKey
);

export const selectShowSidenav = createSelector(
  selectLayoutState,
  fromLayout.selectShowSidenav
);

export const {selectRouteData} = getRouterSelectors();

import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { FirebaseAuthService, ForgotPasswordCallback, LoginCallback } from '@portal/firebase-auth';
import { LoginPageActions } from '@portal/auth';


@Component({
  selector: 'devportal-login',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private store: Store, public readonly fba: FirebaseAuthService) {
  }

  readonly loginCallback: LoginCallback = {
    success: (uid: string, token: string) => {
      this.store.dispatch(LoginPageActions.loginFirebase({credentials: {uid, token}}));
    },

    error: (err: string) => {
      console.error(err);
    }
  };

  readonly resetPassCallback: ForgotPasswordCallback = {
    resetSent: () => {
      console.log(`reset password email sent`);
    },

    error: (err: string) => {
      console.error(err);
    }
  };

  onSignIn(email: string, password: string) {
    this.fba.SignIn(email, password, this.loginCallback)
      .catch(console.error);
  }

  onGoogleAuth() {
    this.fba.GoogleAuth(this.loginCallback)
      .catch(console.error);
  }
}

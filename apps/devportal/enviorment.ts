export const environment = {
  production: true,
  configUrl: './config.json',
  firebase: {
    apiKey: "",
    appId: "1:199288137804:web:ae5abc4afc9113a433ace9",
    messagingSenderId: "199288137804",
    projectId: "amber-app-supercool",
    authDomain: "amber-app-supercool.firebaseapp.com",
    databaseURL: "https://amber-app-supercool.firebaseio.com",
    storageBucket: "amber-app-supercool.appspot.com",
    measurementId: "G-CTYPR3NVJ9"
  }
};

import type { CodegenConfig } from '@graphql-codegen/cli'

const config: CodegenConfig = {
  overwrite: true,
  schema: 'https://api-stg.picnicgcp.net/query',
  documents: ['./libs/**/*.gql', './apps/**/*.gql'],
  generates: {
    './libs/gen/gql/generated.ts': {
      plugins: ['typescript', 'typescript-operations', 'typescript-apollo-angular'],
      config: {
        addExplicitOverride: true
      }
    }
  }
}
export default config

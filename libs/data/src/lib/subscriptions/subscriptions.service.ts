import { Injectable } from '@angular/core';

import {
  AppSubscription,
  GetAllSubscriptionsGQL,
  AddSubscriptionToAppGQL,
  RemoveSubscriptionFromAppGQL
} from '@portal/gql';
import { map, Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AppSubscriptionsService {

  constructor(
    private getAllSubscriptionGQL: GetAllSubscriptionsGQL,
    private addSubscriptionToAppGQL: AddSubscriptionToAppGQL,
    private removeSubscriptionFromAppGQL: RemoveSubscriptionFromAppGQL
  ) {
  }

  getAllSubscriptions(): Observable<AppSubscription[]> {
    return this.getAllSubscriptionGQL.fetch().pipe(
      map(res => <AppSubscription[]>res.data.getAppSubscriptions)
    );
  }

  addSubscriptionToApp(appID: string, subscriptionID: string): Observable<boolean> {
    return this.addSubscriptionToAppGQL.mutate({appID, subscriptionID}).pipe(
      map(res => <boolean>res.data?.addSubscriptionToApp.success)
    );
  }

  removeSubscriptionFromApp(appID: string, subscriptionID: string): Observable<boolean> {
    return this.removeSubscriptionFromAppGQL.mutate({appID, subscriptionID}).pipe(
      map(res => <boolean>res.data?.removeSubscriptionFromApp.success)
    );
  }
}

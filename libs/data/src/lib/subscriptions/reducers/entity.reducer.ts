import { createFeature, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { AppSubscription } from '@portal/gql';
import { EntityActions } from '../actions';

export const entityKey = 'entity';

export interface State extends EntityState<AppSubscription> {
  // additional entities state properties
}

export const adapter: EntityAdapter<AppSubscription> = createEntityAdapter<AppSubscription>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export const reducer = createReducer(
  initialState,
  on(EntityActions.addSubscription,
    (state, action) => adapter.addOne(action.subscription, state)
  ),
  on(EntityActions.upsertSubscription,
    (state, action) => adapter.upsertOne(action.subscription, state)
  ),
  on(EntityActions.addSubscriptions,
    (state, action) => adapter.addMany(action.subscriptions, state)
  ),
  on(EntityActions.upsertSubscriptions,
    (state, action) => adapter.upsertMany(action.subscriptions, state)
  ),
  on(EntityActions.updateSubscription,
    (state, action) => adapter.updateOne(action.subscription, state)
  ),
  on(EntityActions.updateSubscriptions,
    (state, action) => adapter.updateMany(action.subscriptions, state)
  ),
  on(EntityActions.deleteSubscription,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(EntityActions.deleteSubscriptions,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(EntityActions.loadSubscriptions,
    (state, action) => adapter.setAll(action.subscriptions, state)
  ),
  on(EntityActions.clearSubscriptions,
    state => adapter.removeAll(state)
  )
);

export const entityFeature = createFeature({
  name: entityKey,
  reducer,
  extraSelectors: ({selectEntityState}) => ({
    ...adapter.getSelectors(selectEntityState)
  })
});

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = entityFeature;

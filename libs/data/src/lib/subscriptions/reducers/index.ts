import { Action, combineReducers, createFeature, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromApi from './api.reducer';
import * as fromEntity from './entity.reducer';
import { selectAll } from './entity.reducer';

export { fromApi, fromEntity };

export const subscriptionsDataKey = 'subscriptionsData';

export interface SubscriptionsDataState {
  [fromApi.apiKey]: fromApi.State;
  [fromEntity.entityKey]: fromEntity.State;
}

export function reducers(state: SubscriptionsDataState | undefined, action: Action) {
  return combineReducers({
    [fromApi.apiKey]: fromApi.reducer,
    [fromEntity.entityKey]: fromEntity.reducer
  })(state, action);
}

export const subscriptionsDataFeature = createFeature({
  name: subscriptionsDataKey,
  reducer: reducers
});

export const selectSubscriptionsDataState =
  createFeatureSelector<SubscriptionsDataState>(subscriptionsDataKey);

export const selectSubscriptionsApiState = createSelector(
  selectSubscriptionsDataState,
  state => state.api
);

export const selectSubscriptionsEntityState = createSelector(
  selectSubscriptionsDataState,
  state => state.entity
);

export const selectAllEntities = createSelector(
  selectSubscriptionsEntityState,
  selectAll,
  (state, entities) => entities
);

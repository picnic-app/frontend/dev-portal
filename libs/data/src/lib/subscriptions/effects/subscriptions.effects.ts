import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, of } from 'rxjs';

import { ApiActions, EntityActions } from '../actions';
import { AppSubscriptionsService } from '../subscriptions.service';


@Injectable()
export class SubscriptionsEffects {

  loadSubscriptions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ApiActions.loadAll),
      concatMap(() =>
        this.appSubscriptionsService.getAllSubscriptions().pipe(
          map(data => ApiActions.loadAllSuccess({data})),
          catchError(error => of(ApiActions.loadAllFailure({error}))))
      )
    );
  });

  loadPermissionsSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ApiActions.loadAllSuccess),
      map(({data}) => EntityActions.upsertSubscriptions({subscriptions: data}))
    );
  });

  constructor(private actions$: Actions, private appSubscriptionsService: AppSubscriptionsService) {
  }
}

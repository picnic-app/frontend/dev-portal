import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { AppSubscription } from '@portal/gql';

export const EntityActions = createActionGroup({
  source: 'Subscription/Entity',
  events: {
    'Load Subscriptions': props<{ subscriptions: AppSubscription[] }>(),
    'Add Subscription': props<{ subscription: AppSubscription }>(),
    'Upsert Subscription': props<{ subscription: AppSubscription }>(),
    'Add Subscriptions': props<{ subscriptions: AppSubscription[] }>(),
    'Upsert Subscriptions': props<{ subscriptions: AppSubscription[] }>(),
    'Update Subscription': props<{ subscription: Update<AppSubscription> }>(),
    'Update Subscriptions': props<{ subscriptions: Update<AppSubscription>[] }>(),
    'Delete Subscription': props<{ id: string }>(),
    'Delete Subscriptions': props<{ ids: string[] }>(),
    'Clear Subscriptions': emptyProps(),
  }
});

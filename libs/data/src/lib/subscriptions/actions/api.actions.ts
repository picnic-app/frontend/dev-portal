import { createActionGroup, emptyProps, props } from '@ngrx/store';

import { AppSubscription } from '@portal/gql';

export const ApiActions = createActionGroup({
  source: 'Subscription/API',
  events: {
    'Load All': emptyProps(),
    'Load All Success': props<{ data: AppSubscription[] }>(),
    'Load All Failure': props<{ error: string }>()
  }
});

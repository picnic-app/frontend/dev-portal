import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

import { AppTag, GetAppTagsGQL } from '@portal/gql';


@Injectable({
  providedIn: 'root'
})
export class AppTagsService {

  constructor(
    private getAppTagsGQL: GetAppTagsGQL,
  ) {
  }
  getAppTags(): Observable<AppTag[]> {
    return this.getAppTagsGQL.fetch().pipe(
      map(value => <AppTag[]>value.data.getAppTags)
    );
  }
}

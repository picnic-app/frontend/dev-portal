import { createAction, props } from '@ngrx/store';

import { AppTag } from '@portal/gql';

export const loadAppTags = createAction(
  '[AppTags] Load'
);

export const loadAppTagsSuccess = createAction(
  '[AppTags] LoadSuccess',
  props<{ appTags: AppTag[] }>()
);

export const loadAppTagsFailure = createAction(
  '[AppTags] LoadFailure',
  props<{ error: string }>()
);

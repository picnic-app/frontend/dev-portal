import {createFeature, createFeatureSelector, createReducer, on} from '@ngrx/store';

import { AppTag } from "@portal/gql";
import { loadAppTags, loadAppTagsFailure, loadAppTagsSuccess } from "./app-tags.actions";


export const APP_TAGS_FEATURE_KEY = 'appTags';

export interface AppTagsState {
  appTags: AppTag[]
  isLoaded: boolean
  isLoading: boolean
}

const initialState: AppTagsState = {
  appTags: [],
  isLoaded: false,
  isLoading: false,
}

export const appTagsFeature = createFeature({
  name: APP_TAGS_FEATURE_KEY,
  reducer: createReducer(
    initialState,

    on(loadAppTags, (state) => {
      return {...state, isLoading: true}
    }),

    on(loadAppTagsSuccess, (state, {appTags}) => {
      return {...state, appTags, isLoading: false, isLoaded: true}
    }),

    on(loadAppTagsFailure, (state, {error}) => {
      return {...state, error, isLoading: false, isLoaded: true}
    }),
  )
})

export const getAppTagsState = createFeatureSelector<AppTagsState>(
  APP_TAGS_FEATURE_KEY
)

export const {
  name,
  reducer,
  selectAppTags,
  selectIsLoading,
  selectIsLoaded,
} = appTagsFeature;


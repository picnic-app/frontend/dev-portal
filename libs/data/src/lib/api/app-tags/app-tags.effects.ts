import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@nx/angular';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { loadAppTags, loadAppTagsFailure, loadAppTagsSuccess } from './app-tags.actions';
import { AppTagsService } from './app-tags.service';
import { AppTagActions } from '../../entity/app-tag';


@Injectable()
export class AppTagsApiEffects {
  loadAppTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAppTags),
      fetch({
        run: () => {
          return this.appTagsService.getAppTags()
            .pipe(
              map(appTags => loadAppTagsSuccess({appTags}))
            );
        },

        onError: (action, error) => {
          return loadAppTagsFailure({error: error});
        }
      })
    )
  );

  loadAppTagsSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAppTagsSuccess),
      map(({appTags}) => appTags),
      map((appTags) => AppTagActions.loadAppTags({appTags}))
    )
  );

  constructor(private actions$: Actions, private appTagsService: AppTagsService, private router: Router) {
  }
}

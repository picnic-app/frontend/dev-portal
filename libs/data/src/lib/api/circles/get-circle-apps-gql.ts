import { Injectable } from "@angular/core";
import { Query, gql } from "apollo-angular";
import { CircleAppEdge, CursorInput, PageInfo } from "libs/gen/gql/generated";

export interface CircleAppPage {
  edges: CircleAppEdge[];
  pageInfo: PageInfo;
}

export interface GetCircleAppsResponse {
  getCircleApps: CircleAppPage;
}

export interface GetCirclesByAppIdInput {
  appId?: string;
  cursor?: CursorInput;
}

@Injectable({
  providedIn: 'root',
})
export class GetCircleAppsGQL extends Query<GetCircleAppsResponse, { input: GetCirclesByAppIdInput }> {
  override document = gql`
      query ($input: GetCircleAppsInput!) {
        getCircleApps(input: $input) {
          pageInfo {
            length
            lastId
            hasPreviousPage
            hasNextPage
            firstId
          }
          edges {
            cursorId
            node {
                circleId
                circle {
                  id
                  name
                }
                appID
                createdAt
            }
          }
        }
      }
    `;
}
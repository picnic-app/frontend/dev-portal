import { Injectable } from '@angular/core';
import { Observable, map, of } from 'rxjs';

import { CursorInput } from '@portal/gql';
import { CircleAppPage, GetCircleAppsGQL } from './get-circle-apps-gql';

@Injectable({
    providedIn: 'root'
})
export class CircleAppsService {

    constructor(
        private getCircleAppsGQL: GetCircleAppsGQL,
    ) {
    }

    getCircles(appID: string, cursor: CursorInput): Observable<CircleAppPage> {
        if (appID == null || appID == '') {
            console.error(`invalid appID passed: ${appID}`);
            return of();
        }
        return this.getCircleAppsGQL.fetch({
            input: {
                appId: appID,
                cursor: cursor
            }
        }).pipe(
            map(res => res.data.getCircleApps)
        );
    }
}

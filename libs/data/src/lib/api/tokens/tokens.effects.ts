import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@nx/angular';
import { filter, map } from 'rxjs/operators';

import { AppTokensService } from './tokens.service';
import { loadAppTokens, loadAppTokensFailure, loadAppTokensSuccess } from './tokens.actions';
import { AppTokenActions } from '../../entity/token';
import { AuthActions } from '@portal/auth';

@Injectable()
export class AppTokensApiEffects {

  loadTokens$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAppTokens),
      fetch({
        run: (action) => {
          return this.tokensSrv.getTokens(action.appID)
            .pipe(
              map(tokens => loadAppTokensSuccess({appID: action.appID, tokens: tokens}))
            );
        },

        onError: (action, error) => {
          return loadAppTokensFailure({appID: action.appID, error: error});
        }
      })
    )
  );

  loadTokensSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAppTokensSuccess),
      map(({tokens}) => tokens),
      filter(tokens => !!tokens),
      map(tokens => AppTokenActions.upsertAppTokens({tokens}))
    )
  );

  clearOnLogout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logoutSuccess),
      map(() => AppTokenActions.clearAppTokens())
    )
  );

  constructor(private actions$: Actions, private tokensSrv: AppTokensService) {
  }
}

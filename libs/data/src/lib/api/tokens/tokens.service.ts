import {Injectable} from '@angular/core';
import {Observable, map, of} from 'rxjs';

import {
  AppToken,
  GenerateAppTokenGQL, GeneratedToken,
  GetAppTokensGQL,
  IssueAppTokenGQL, IssuedToken,
  RevokeAppTokenGQL
} from '@portal/gql';

@Injectable({
  providedIn: 'root'
})
export class AppTokensService {

  constructor(
    private getTokensGQL: GetAppTokensGQL,
    private generateTokenGQL: GenerateAppTokenGQL,
    private issueTokenGQL: IssueAppTokenGQL,
    private revokeTokenGQL: RevokeAppTokenGQL
  ) {
  }

  getTokens(appID: string): Observable<AppToken[]> {
    if (appID == null || appID == '') {
      console.error(`invalid appID passed: ${appID}`);
      return of([]);
    }
    return this.getTokensGQL.fetch({appID}).pipe(
      map(res => <AppToken[]>res.data.getAppTokens)
    );
  }

  generateToken(appID: string, name: string): Observable<GeneratedToken> {
    return this.generateTokenGQL.mutate({appID, name}).pipe(
      map(res => <GeneratedToken>res.data?.generateAppToken)
    );
  }

  issueToken(tokenID: string): Observable<IssuedToken> {
    return this.issueTokenGQL.mutate({tokenID}).pipe(
      map(res => <IssuedToken>res.data?.issueAppToken)
    );
  }

  revokeToken(tokenID: string): Observable<boolean> {
    return this.revokeTokenGQL.mutate({tokenID}).pipe(
      map(res => res.data?.revokeAppToken),
      map(res => res ? res.success : false)
    );
  }
}

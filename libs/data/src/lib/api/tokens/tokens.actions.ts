import {createAction, props} from '@ngrx/store';

import {AppToken} from '@portal/gql';

export const loadAppTokens = createAction(
  '[AppTokens] Load',
  props<{ appID: string }>()
);

export const loadAppTokensSuccess = createAction(
  '[AppTokens] LoadSuccess',
  props<{ appID: string, tokens: AppToken[] }>()
);

export const loadAppTokensFailure = createAction(
  '[AppTokens] LoadFailure',
  props<{ appID: string, error: string }>()
);

export const createAppToken = createAction(
  '[AppTokens] Create',
  props<{ appID: string, name: string }>()
);

export const createAppTokenSuccess = createAction(
  '[AppTokens] CreateSuccess'
);
export const createAppTokenFailure = createAction(
  '[AppTokens] CreateFailure',
  props<{ appID: string, error: string }>()
);

export const revokeTokenSuccess = createAction(
  '[AppTokens] RevokeSuccess',
  props<{ tokenID: string }>()
);

import { createFeature, createFeatureSelector, createReducer, createSelector } from '@ngrx/store';

import { AppToken } from '@portal/gql';

export const TOKENS_FEATURE_KEY = 'tokens';
export interface TokensState {
  tokens: AppToken[];
}

export interface TokensPartialState {
  readonly [TOKENS_FEATURE_KEY]: TokensState;
}

const initialState: TokensState = {
  tokens: []
};

export const appTokensFeature = createFeature({
  name: TOKENS_FEATURE_KEY,
  reducer: createReducer(
    initialState

    // on(revokeTokenSuccess, (state, {tokenID}) => {
    // TODO: update token in state
    // })
  )
});

export const getTokensState = createFeatureSelector<TokensState>(
  TOKENS_FEATURE_KEY
);

export const selectAllTokens = createSelector(
  getTokensState,
  state => state.tokens
);

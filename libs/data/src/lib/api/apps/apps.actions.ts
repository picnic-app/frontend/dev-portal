import { createAction, emptyProps, props } from '@ngrx/store';

import { App, AppTag, CreateAppInput } from '@portal/gql';


export const loadApps = createAction(
  '[Apps] Load'
);

export const loadAppsSuccess = createAction(
  '[Apps] LoadSuccess',
  props<{ apps: App[] }>()
);

export const loadAppsFailure = createAction(
  '[Apps] LoadFailure',
  props<{ error: string }>()
);

export const createApp = createAction(
  '[Apps] Create',
  props<{ input: CreateAppInput }>()
);

export const createAppSuccess = createAction(
  '[Apps] CreateSuccess',
  props<{ app: App }>()
);

export const createAppFailure = createAction(
  '[Apps] CreateFailure',
  props<{ err: string }>()
);

export const deleteApp = createAction(
  '[Apps] Delete',
  props<{ id: string }>()
);

export const deleteAppSuccess = createAction(
  '[Apps] DeleteSuccess',
  props<{ id: string }>()
);

export const deleteAppFailure = createAction(
  '[Apps] DeleteFailure',
  props<{ err: string }>()
);

export const setTagsToApp = createAction(
  '[Apps] SetTagsToApp',
  props<{ appId: string, tags: AppTag[] }>()
);

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetch } from '@nx/angular';
import { map } from 'rxjs';

import { AppsService } from './apps.service';
import {
  createApp,
  createAppFailure,
  createAppSuccess,
  deleteApp,
  deleteAppFailure,
  deleteAppSuccess,
  loadApps,
  loadAppsFailure,
  loadAppsSuccess
} from './apps.actions';
import { AppActions } from '../../entity/app';
import { AuthActions } from '@portal/auth';


@Injectable()
export class AppsApiEffects {

  loadApps$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadApps),
      fetch({
        run: () => {
          return this.appsService.myApps()
            .pipe(
              map(apps => loadAppsSuccess({apps}))
            );
        },

        onError: (action, error) => {
          return loadAppsFailure({error: error});
        }
      })
    )
  );

  loadAppsSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadAppsSuccess),
      map(({apps}) => apps.filter(app => !app.deletedAt)),
      map((apps) => AppActions.upsertApps({apps}))
    )
  );

  createApp$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createApp),
      fetch({
        run: (action) => {
          return this.appsService.createApp(action.input)
            .pipe(
              map(app => createAppSuccess({app}))
            );
        },

        onError: (action, err) => {
          return createAppFailure({err});
        }
      })
    )
  );

  createAppSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createAppSuccess),
      map(({app}) => AppActions.addApp({app}))
    )
  );

  deleteApp$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteApp),
      fetch({
        run: (action) => {
          return this.appsService.deleteApp(action.id).pipe(
            map(() => deleteAppSuccess({id: action.id}))
          );
        },

        onError: (action, err) => {
          return deleteAppFailure({err});
        }
      })
    )
  );

  deleteAppSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteAppSuccess),
      map(action => action.id),
      map(id => AppActions.deleteApp({id}))
    )
  );

  clearOnLogout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logoutSuccess),
      map(() => AppActions.clearApps())
    )
  );

  constructor(private actions$: Actions, private appsService: AppsService, private router: Router) {
  }
}

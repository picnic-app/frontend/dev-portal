import {createFeature, createFeatureSelector, createReducer, on} from '@ngrx/store';

import { App } from "@portal/gql";
import { createAppSuccess, loadApps, loadAppsFailure, loadAppsSuccess, setTagsToApp } from "./apps.actions";


export const APPS_FEATURE_KEY = 'apps';

export interface AppsState {
  apps: App[]
  isLoaded: boolean
  isLoading: boolean
}

const initialState: AppsState = {
  apps: [],
  isLoaded: false,
  isLoading: false,
}

export const appsFeature = createFeature({
  name: APPS_FEATURE_KEY,
  reducer: createReducer(
    initialState,

    on(loadApps, (state) => {
      return {...state, isLoading: true}
    }),

    on(loadAppsSuccess, (state, {apps}) => {
      return {...state, apps, isLoading: false, isLoaded: true}
    }),

    on(loadAppsFailure, (state, {error}) => {
      return {...state, error, isLoading: false, isLoaded: true}
    }),

    on(createAppSuccess, (state, {app}) => {
      return {...state, apps: [...state.apps, app]}
    }),

    on(setTagsToApp, (state, {appId, tags}) => {
      const apps = state.apps.map(app => {
        if (app.id === appId) {
          return {...app, tags}
        }
        return app
      })
      return {...state, apps}
    })
)})

export const getAppsState = createFeatureSelector<AppsState>(
  APPS_FEATURE_KEY
)

export const {
  name,
  reducer,
  selectApps,
  selectIsLoading,
  selectIsLoaded,
} = appsFeature;


import { Injectable } from '@angular/core';
import { Observable, map, tap } from 'rxjs';

import {
  AddTagToAppGQL,
  App,
  CreateAppGQL,
  CreateAppInput,
  DeleteAppGQL,
  MyAppsGQL,
  RemoveTagFromAppGQL,
  UpdateAppGQL,
  UpdateAppInput
} from '@portal/gql';


@Injectable({
  providedIn: 'root'
})
export class AppsService {

  constructor(private myAppsGQL: MyAppsGQL,
              private createAppGQL: CreateAppGQL,
              private updateAppGQL: UpdateAppGQL,
              private deleteAppGQL: DeleteAppGQL,
              private addTagToAppGQL: AddTagToAppGQL,
              private removeTagFromAppGQL: RemoveTagFromAppGQL
  ) {
  }

  myApps(): Observable<App[]> {
    return this.myAppsGQL.fetch().pipe(
      map(value => <App[]>value.data.myApps)
    );
  }

  createApp(input: CreateAppInput): Observable<App> {
    return this.createAppGQL.mutate({in: input}).pipe(
      map(value => {
        return <App>value.data?.createApp;
      })
    );
  }

  updateApp(app: App): Observable<App> {
    return this.updateAppGQL.mutate({in: <UpdateAppInput>app}).pipe(
      map(value => app)
    );
  }

  deleteApp(id: string): Observable<boolean> {
    return this.deleteAppGQL.mutate({id}).pipe(
      map(value => <boolean>value.data?.deleteApp.success)
    );
  }


  addTagToApp(appId: string, tagId: string): Observable<boolean> {
    return this.addTagToAppGQL.mutate({appID: appId, tagID: tagId}).pipe(
      map(value => <boolean>value.data?.addTagToApp.success)
    );
  }

  removeTagFromApp(appId: string, tagId: string): Observable<boolean> {
    return this.removeTagFromAppGQL.mutate({appID: appId, tagID: tagId}).pipe(
      map(value => <boolean>value.data?.removeTagFromApp.success)
    );
  }
}

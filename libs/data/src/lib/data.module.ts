import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { provideEntityData } from '@ngrx/data';

import { AppsService } from './api/apps';
import { AppTokensService } from './api/tokens';

import { provideEffects } from '@ngrx/effects';
import { AppsApiEffects } from './api/apps';
import { AppTokensApiEffects } from './api/tokens';
import { EntityDataModuleConfig } from '@ngrx/data/src/entity-data-config';
import { appEntityMetadata } from './entity/app';
import { tokenEntityMetadata } from './entity/token';

import { AppTagsApiEffects, AppTagsService } from './api/app-tags';
import { appTagEntityMetadata } from './entity/app-tag';

import { PermissionsEffects } from './permissions';
import { SubscriptionsEffects } from './subscriptions';

const entityDataConfig: EntityDataModuleConfig = {
  entityMetadata: {
    App: appEntityMetadata,
    AppToken: tokenEntityMetadata,
    AppTag: appTagEntityMetadata,
  }
};

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    AppsService,
    AppTokensService,
    AppTagsService,
    provideEntityData(entityDataConfig),
    provideEffects([AppsApiEffects, AppTokensApiEffects, AppTagsApiEffects, PermissionsEffects, SubscriptionsEffects])
  ]
})
export class DataModule {
}

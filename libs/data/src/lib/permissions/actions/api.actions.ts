import { createActionGroup, emptyProps, props } from '@ngrx/store';

import { AppPermission } from '@portal/gql';

export const ApiActions = createActionGroup({
  source: 'Permission/API',
  events: {
    'Load All': emptyProps(),
    'Load All Success': props<{ data: AppPermission[] }>(),
    'Load All Failure': props<{ error: string }>(),
    'Add To App': props<{ id: string, appID: string }>(),
    'Remove From App': props<{ id: string, appID: string }>()
  }
});

import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { AppPermission } from '@portal/gql';


export const EntityActions = createActionGroup({
  source: 'AppPermission/Entity',
  events: {
    'Load AppPermissions': props<{ permissions: AppPermission[] }>(),
    'Add AppPermission': props<{ permission: AppPermission }>(),
    'Upsert AppPermission': props<{ permission: AppPermission }>(),
    'Add AppPermissions': props<{ permissions: AppPermission[] }>(),
    'Upsert AppPermissions': props<{ permissions: AppPermission[] }>(),
    'Update AppPermission': props<{ permission: Update<AppPermission> }>(),
    'Update AppPermissions': props<{ permissions: Update<AppPermission>[] }>(),
    'Delete AppPermission': props<{ id: string }>(),
    'Delete AppPermissions': props<{ ids: string[] }>(),
    'Clear AppPermissions': emptyProps()
  }
});

import { Action, combineReducers, createFeature, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromApi from './api.reducer';
import * as fromEntity from './entity.reducer';
import { entityKey } from './entity.reducer';

export { fromApi, fromEntity };

export const permissionsDataKey = 'permissionsData';

export interface PermissionsDataState {
  [fromApi.apiKey]: fromApi.State;
  [fromEntity.entityKey]: fromEntity.State;
}

export function reducers(state: PermissionsDataState | undefined, action: Action) {
  return combineReducers({
    [fromApi.apiKey]: fromApi.reducer,
    [fromEntity.entityKey]: fromEntity.reducer
  })(state, action);
}

export const permissionsDataFeature = createFeature({
  name: permissionsDataKey,
  reducer: reducers
});

export const selectPermissionsDataState =
  createFeatureSelector<PermissionsDataState>(permissionsDataKey);

export const selectPermissionsApiState = createSelector(
  selectPermissionsDataState,
  state => state.api
);

export const selectPermissionsEntityState = createSelector(
  selectPermissionsDataState,
  state => state[entityKey]
);

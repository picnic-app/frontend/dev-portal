import { createFeature, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { AppPermission } from '@portal/gql';
import { EntityActions } from '../actions';

export const entityKey = 'entity';

export interface State extends EntityState<AppPermission> {
  // additional entities state properties
}

export const adapter: EntityAdapter<AppPermission> = createEntityAdapter<AppPermission>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export const reducer = createReducer(
  initialState,
  on(EntityActions.addAppPermission,
    (state, action) => adapter.addOne(action.permission, state)
  ),
  on(EntityActions.upsertAppPermission,
    (state, action) => adapter.upsertOne(action.permission, state)
  ),
  on(EntityActions.addAppPermissions,
    (state, action) => adapter.addMany(action.permissions, state)
  ),
  on(EntityActions.upsertAppPermissions,
    (state, action) => adapter.upsertMany(action.permissions, state)
  ),
  on(EntityActions.updateAppPermission,
    (state, action) => adapter.updateOne(action.permission, state)
  ),
  on(EntityActions.updateAppPermissions,
    (state, action) => adapter.updateMany(action.permissions, state)
  ),
  on(EntityActions.deleteAppPermission,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(EntityActions.deleteAppPermissions,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(EntityActions.loadAppPermissions,
    (state, action) => adapter.setAll(action.permissions, state)
  ),
  on(EntityActions.clearAppPermissions,
    state => adapter.removeAll(state)
  )
);

export const entityFeature = createFeature({
  name: entityKey,
  reducer,
  extraSelectors: ({selectEntityState}) => ({
    ...adapter.getSelectors(selectEntityState)
  })
});

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = entityFeature;

import { createFeature, createReducer, on } from '@ngrx/store';

import { ApiActions } from '../actions';
export const apiKey = 'api';

export interface State {
  pending: boolean;
  loaded: boolean;
}

export const initialState: State = {
  pending: false,
  loaded: false
};

export const reducer = createReducer(
  initialState,

  on(ApiActions.loadAll, state => ({
    ...state,
    pending: true
  })),

  on(ApiActions.loadAllSuccess, (state, action) => ({
    ...state,
    pending: false,
    loaded: true
  })),

  on(ApiActions.loadAllFailure, (state, action) => ({
    ...state,
    pending: false,
    loaded: false
  }))
);

export const permissionsApiFeature = createFeature({
  name: apiKey,
  reducer
});

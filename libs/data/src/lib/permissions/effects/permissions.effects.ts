import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, of } from 'rxjs';

import { ApiActions, EntityActions } from '../actions';
import { AppPermissionService } from '../permission.service';

@Injectable()
export class PermissionsEffects {

  loadPermissions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ApiActions.loadAll),
      concatMap(() =>
        this.appPermissionsService.getAllPermissions().pipe(
          map(data => ApiActions.loadAllSuccess({data})),
          catchError(error => of(ApiActions.loadAllFailure({error}))))
      )
    );
  });

  loadPermissionsSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ApiActions.loadAllSuccess),
      map(({data}) => EntityActions.upsertAppPermissions({permissions: data}))
    );
  });

  addPermissionToApp$ = createEffect(() => this.actions$.pipe(
    ofType(ApiActions.addToApp),
    map(({id, appID}) => {
      console.debug(`add to app data: ${id}, ${appID}`);
      this.appPermissionsService.addPermissionToApp(appID, id).subscribe();
    })
  ), {dispatch: false});

  removePermissionFromApp$ = createEffect(() => this.actions$.pipe(
    ofType(ApiActions.removeFromApp),
    map(({id, appID}) => {
      console.debug(`remove from app data: ${id}, ${appID}`);
      this.appPermissionsService.removePermissionFromApp(appID, id).subscribe();
    })
  ), {dispatch: false});

  constructor(private actions$: Actions, private appPermissionsService: AppPermissionService) {
  }
}

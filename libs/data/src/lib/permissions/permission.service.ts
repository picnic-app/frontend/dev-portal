import { Injectable } from '@angular/core';

import {
  AppPermission,
  AddPermissionToAppGQL,
  GetAllPermissionsGQL,
  RemovePermissionFromAppGQL
} from '@portal/gql';
import { map, Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AppPermissionService {

  constructor(
    private getAllPermissionsGQL: GetAllPermissionsGQL,
    private addPermissionToAppGQL: AddPermissionToAppGQL,
    private removePermissionFromAppGQL: RemovePermissionFromAppGQL
  ) {
  }

  getAllPermissions(): Observable<AppPermission[]> {
    return this.getAllPermissionsGQL.fetch().pipe(
      map(res => <AppPermission[]>res.data.getAppPermissions)
    );
  }

  addPermissionToApp(appID: string, permissionID: string): Observable<boolean> {
    console.log(`add permission`);
    return this.addPermissionToAppGQL.mutate({appID, permissionID}).pipe(
      map(res => <boolean>res.data?.addPermissionToApp.success)
    );
  }

  removePermissionFromApp(appID: string, permissionID: string): Observable<boolean> {
    console.log(`remove permission`);
    return this.removePermissionFromAppGQL.mutate({appID, permissionID}).pipe(
      map(res => <boolean>res.data?.removePermissionFromApp.success)
    );
  }
}

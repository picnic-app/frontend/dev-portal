import {createActionGroup, emptyProps, props} from '@ngrx/store';
import {Update} from '@ngrx/entity';

import {AppToken} from '@portal/gql';

export const AppTokenActions = createActionGroup({
  source: 'AppToken/API',
  events: {
    'Load AppTokens': props<{ tokens: AppToken[] }>(),
    'Add AppToken': props<{ token: AppToken }>(),
    'Upsert AppToken': props<{ token: AppToken }>(),
    'Add AppTokens': props<{ tokens: AppToken[] }>(),
    'Upsert AppTokens': props<{ tokens: AppToken[] }>(),
    'Update AppToken': props<{ token: Update<AppToken> }>(),
    'Update AppTokens': props<{ tokens: Update<AppToken>[] }>(),
    'Delete AppToken': props<{ id: string }>(),
    'Delete AppTokens': props<{ ids: string[] }>(),
    'Clear AppTokens': emptyProps()
  }
});

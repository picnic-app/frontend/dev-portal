import {createFeature, createReducer, on} from '@ngrx/store';
import {EntityState, EntityAdapter, createEntityAdapter} from '@ngrx/entity';

import {AppTokenActions} from './app-token.actions';
import {AppToken} from '@portal/gql';
import {EntityMetadata} from '@ngrx/data';

export const appTokensFeatureKey = 'appTokens';

export const tokenEntityMetadata: EntityMetadata<AppToken> = {
  entityName: 'AppToken',
  selectId: (entity: AppToken): string => entity.tokenID,
  sortComparer: (a, b) =>
    new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
  filterFn: (entities) =>
    entities.filter(
      (entity) => !entity.revokedAt
    )
};

export interface State extends EntityState<AppToken> {
  // additional entities state properties
}

export const adapter: EntityAdapter<AppToken> = createEntityAdapter<AppToken>(tokenEntityMetadata);

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export const reducer = createReducer(
  initialState,
  on(AppTokenActions.addAppToken,
    (state, action) => adapter.addOne(action.token, state)
  ),
  on(AppTokenActions.upsertAppToken,
    (state, action) => adapter.upsertOne(action.token, state)
  ),
  on(AppTokenActions.addAppTokens,
    (state, action) => adapter.addMany(action.tokens, state)
  ),
  on(AppTokenActions.upsertAppTokens,
    (state, action) => adapter.upsertMany(action.tokens, state)
  ),
  on(AppTokenActions.updateAppToken,
    (state, action) => adapter.updateOne(action.token, state)
  ),
  on(AppTokenActions.updateAppTokens,
    (state, action) => adapter.updateMany(action.tokens, state)
  ),
  on(AppTokenActions.deleteAppToken,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(AppTokenActions.deleteAppTokens,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(AppTokenActions.loadAppTokens,
    (state, action) => adapter.setAll(action.tokens, state)
  ),
  on(AppTokenActions.clearAppTokens,
    state => adapter.removeAll(state)
  )
);

export const appTokensFeature = createFeature({
  name: appTokensFeatureKey,
  reducer,
  extraSelectors: ({selectAppTokensState}) => ({
    ...adapter.getSelectors(selectAppTokensState)
  })
});

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = appTokensFeature;

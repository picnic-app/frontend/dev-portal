import {createActionGroup, props} from '@ngrx/store';

import {AppTag} from '@portal/gql';

export const AppTagActions = createActionGroup({
  source: 'AppTag/API',
  events: {
    'Load AppTags': props<{ appTags: AppTag[] }>(),
  }
});

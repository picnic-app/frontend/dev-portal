import {createFeature, createReducer, on} from '@ngrx/store';
import {EntityState, EntityAdapter, createEntityAdapter} from '@ngrx/entity';

import {AppTagActions} from './app-tag.actions';
import {AppTag} from '@portal/gql';
import {EntityMetadata} from '@ngrx/data';

export const appTagsFeatureKey = 'appTags';

export const appTagEntityMetadata: EntityMetadata<AppTag> = {
  entityName: 'AppTag',
  selectId: (entity: AppTag): string => entity.id,
  sortComparer: (a, b) =>
    a.name.localeCompare(b.name),
};

export interface State extends EntityState<AppTag> {
  // additional entities state properties
}

export const adapter: EntityAdapter<AppTag> = createEntityAdapter<AppTag>(appTagEntityMetadata);

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export const reducer = createReducer(
  initialState,
  on(AppTagActions.loadAppTags,
    (state, action) => adapter.setMany(action.appTags, state)
  ),
);

export const appTagsFeature = createFeature({
  name: appTagsFeatureKey,
  reducer,
  extraSelectors: ({selectAppTagsState}) => ({
    ...adapter.getSelectors(selectAppTagsState)
  })
});

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = appTagsFeature;

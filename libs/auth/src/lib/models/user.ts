export interface FirebaseCredentials {
  uid: string;
  token: string;
}

export interface AuthUser {
  id: string;
  username: string;
  fullName: string;
  profileImage: string;
}

export interface LoginResult {
  user: AuthUser;
  token: string;
}

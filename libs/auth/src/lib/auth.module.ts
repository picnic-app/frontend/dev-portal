import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

import { AuthEffects } from './+state';

import { AuthRoutingModule } from './auth-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from '@portal/graphql';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    GraphQLModule,
    AuthRoutingModule,
    EffectsModule.forFeature(AuthEffects)
  ]
})
export class AuthModule {
}

// import { inject } from "@angular/core";
// import { ActivatedRouteSnapshot, Router } from "@angular/router";
// import { select, Store } from "@ngrx/store";
// import { Observable, map } from "rxjs";
//
// import { selectLoggedIn } from "./+state";

/**
 * @deprecated
 * @param _
 */
// export function _authCanActivate(_: ActivatedRouteSnapshot): Observable<boolean> {
//   const router = inject(Router);
//   return inject(Store<any>).pipe(
//     select(selectLoggedIn),
//     map(isAuthenticated => {
//       if (!isAuthenticated) {
//         router.navigateByUrl("/login").catch(err => {
//           console.error(err);
//         });
//       }
//       return isAuthenticated;
//     })
//   );
// }

import { inject, PLATFORM_ID } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';

import * as fromAuth from '@portal/auth';
import { AuthApiActions } from '@portal/auth';
import { isPlatformServer } from '@angular/common';

export const authGuard = (): Observable<boolean> => {
  const store = inject(Store);
  const platformId = inject(PLATFORM_ID)

  if (isPlatformServer(platformId)) {
    return of(true)
  }

  return store.select(fromAuth.selectLoggedIn).pipe(
    map((authed) => {
      if (!authed) {
        store.dispatch(AuthApiActions.loginRedirect());
        return false;
      }

      return true;
    }),
    take(1)
  );
};

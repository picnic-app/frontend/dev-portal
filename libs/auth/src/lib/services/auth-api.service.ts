import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';

import { SignInFirebaseGQL } from '@portal/gql';
import { AuthUser, LoginResult } from '../models';

@Injectable({
  providedIn: 'root'
})
export class AuthApiService {

  constructor(private signInGQL: SignInFirebaseGQL) {
  }

  signInFirebase(uid: string, token: string): Observable<LoginResult> {
    return this.signInGQL.mutate({firebaseUserID: uid, firebaseToken: token}).pipe(
      map(res => {
        if (res.errors) {
          const err = (/desc = account not found/).test(res.errors[0]?.message)
            ? 'Please sign up to the Picnic mobile/web application first to be able to log in'
            : res.errors[0]?.message;
          throw new Error(err);
        }
        if (res.data == null) {
          throw new Error('login result data is null');
        }
        return {
          token: res.data.signInWithFirebase.authInfo.accessToken,
          user: <AuthUser>res.data.signInWithFirebase.user
        };
      })
    );
  }
}

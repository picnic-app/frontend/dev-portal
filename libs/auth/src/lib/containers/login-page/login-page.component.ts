import { Component } from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { Store } from '@ngrx/store';

import { FirebaseAuthService, ForgotPasswordCallback, LoginCallback } from '@portal/firebase-auth';
import { loginFirebase, resetForm, submitForm } from '../../+state/actions/login-page.actions';
import { LoginFormComponent } from '../../components/login-form/login-form.component';
import * as fromAuth from '@portal/auth';
import { MatButtonModule } from '@angular/material/button';

import { loginFailure } from '../../+state/actions/auth-api.actions';

@Component({
  selector: 'devportal-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  imports: [
    LoginFormComponent,
    AsyncPipe,
    MatButtonModule
  ],
  standalone: true
})
export class LoginPageComponent {

  pending$ = this.store.select(fromAuth.selectLoginPagePending);
  error$ = this.store.select(fromAuth.selectLoginPageError);

  constructor(private store: Store, private fb: FirebaseAuthService) {
    this.store.dispatch(resetForm());
  }

  readonly loginCallback: LoginCallback = {
    success: (uid: string, token: string) => {
      this.store.dispatch(loginFirebase({credentials: {uid, token}}));
    },

    error: (error: any) => {
      this.store.dispatch(loginFailure({error}));
    }
  };

  readonly resetPassCallback: ForgotPasswordCallback = {
    resetSent: () => {
      console.log(`reset password email sent`);
    },

    error: (err: string) => {
      console.error(err);
    }
  };

  onSignIn(email: string, password: string) {
    this.store.dispatch(submitForm({email, password}));
    this.fb.SignIn(email, password, this.loginCallback)
      .catch(console.error);
  }

  onGoogleAuth() {
    this.fb.GoogleAuth(this.loginCallback)
      .catch(console.error);
  }

  onAppleAuth() {
    this.fb.AppleAuth(this.loginCallback)
      .catch(console.error);
  }

  onSubmit(credentials: { email: string, password: string }) {
    this.onSignIn(credentials.email, credentials.password);
  }
}

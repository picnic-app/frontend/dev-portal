import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';

import { AuthActions, AuthApiActions, LoginPageActions } from '../actions';
import { AuthApiService } from '@portal/auth';
import { FirebaseCredentials } from '../../models';
import { logout, logoutSuccess } from '../actions/auth.actions';


@Injectable()
export class AuthEffects {

  loginFirebase$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoginPageActions.loginFirebase),
      map((action) => action.credentials),
      exhaustMap((auth: FirebaseCredentials) =>
        this.authService.signInFirebase(auth.uid, auth.token).pipe(
          map((result) => {
            return AuthApiActions.loginSuccess({...result});
          }),
          catchError((err: any) => {
            return of(AuthApiActions.loginFailure({error: String(err)}));
          })
        )
      )
    )
  );

  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthApiActions.loginSuccess),
        tap(() => this.router.navigate(['/']))
      ),
    {dispatch: false}
  );

  loginRedirect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthApiActions.loginRedirect, AuthActions.logout),
        tap(() => {
          this.router.navigate(['/login'])
            .catch(console.error);
        })
      ),
    {dispatch: false}
  );

  logoutSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(logout),
        map(() => logoutSuccess())
      )
  );

  // logoutConfirmation$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(AuthActions.logoutConfirmation),
  //     exhaustMap(() => {
  //       const dialogRef = this.dialog.open<
  //         LogoutConfirmationDialogComponent,
  //         undefined,
  //         boolean
  //       >(LogoutConfirmationDialogComponent);
  //
  //       return dialogRef.afterClosed();
  //     }),
  //     map((result) =>
  //       result ? AuthActions.logout() : AuthActions.logoutConfirmationDismiss()
  //     )
  //   )
  // );

  // logoutIdleUser$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(UserActions.idleTimeout),
  //     map(() => AuthActions.logout())
  //   )
  // );

  constructor(
    private actions$: Actions,
    private authService: AuthApiService,
    private router: Router
  ) {
  }
}

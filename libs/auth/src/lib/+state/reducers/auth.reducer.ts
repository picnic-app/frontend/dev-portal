import { createReducer, on } from '@ngrx/store';

import { AuthUser } from '../../models';
import { AuthActions, AuthApiActions } from '../actions';

export const statusFeatureKey = 'status';

export interface State {
  token: string | null;
  user: AuthUser | null;
}

export const initialState: State = {
  token: null,
  user: null
};

export const reducer = createReducer(
  initialState,
  on(AuthApiActions.loginSuccess, (state, {token, user}) => ({...state, token, user})),
  on(AuthActions.logout, () => initialState)
);

export const getUser = (state: State) => {
  return state.user;
};

export const getToken = (state: State) => state.token;

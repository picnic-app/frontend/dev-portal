import { createReducer, on } from '@ngrx/store';

import { AuthApiActions, LoginPageActions } from '../actions';

export const loginPageFeatureKey = 'loginPage';

export interface State {
  error: string | null;
  pending: boolean;
}

export const initialState: State = {
  error: null,
  pending: false
};

export const reducer = createReducer(
  initialState,

  on(LoginPageActions.submitForm, (state) => ({
    ...state,
    error: null,
    pending: true
  })),

  on(LoginPageActions.loginFirebase, (state) => ({
    ...state,
    error: null,
    pending: true
  })),

  on(AuthApiActions.loginSuccess, (state) => ({
    ...state,
    error: null,
    pending: false
  })),

  on(AuthApiActions.loginFailure, (state, {error}) => {
    return ({
      ...state,
      error,
      pending: false
    });
  }),

  on(LoginPageActions.resetForm, (state) => ({
    ...state,
    error: null
  }))
);

export const getError = (state: State) => state.error;
export const getPending = (state: State) => state.pending;

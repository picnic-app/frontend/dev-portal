import { createAction } from '@ngrx/store';

export const logout = createAction(
  '[Auth] Logout'
);

export const logoutSuccess = createAction(
  '[Auth] LogoutSuccess'
);

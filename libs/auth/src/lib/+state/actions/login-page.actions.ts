import { createAction, props } from '@ngrx/store';

import { FirebaseCredentials } from '../../models';

export const submitForm = createAction(
  '[Login Page] Submit Form',
  props<{ email: string, password: string }>()
);

export const loginFirebase = createAction(
  '[Login Page] Login',
  props<{ credentials: FirebaseCredentials }>()
);

export const resetForm = createAction(
  '[Login Page] Reset'
);

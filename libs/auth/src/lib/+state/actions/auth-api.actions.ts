import { props, createAction } from '@ngrx/store';

import {AuthUser} from '../../models';

export const loginSuccess = createAction(
  '[Auth/API] Login Success',
  props<{ token: string, user: AuthUser }>()
);

export const loginFailure = createAction(
  '[Auth/API] Login Failure',
  props<{ error: string }>()
);

export const loginRedirect = createAction('[Auth/API] Login Redirect');

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FIREBASE_OPTIONS } from '@angular/fire/compat';
import { AngularFireAuth } from '@angular/fire/compat/auth';

import { FirebaseAuthService } from "./firebase-auth.service";

@NgModule({
  imports: [CommonModule],
  providers: [
    {
      provide: AngularFireAuth,
      useClass: AngularFireAuth,
      deps: [FIREBASE_OPTIONS]
    },
    FirebaseAuthService
  ]
})
export class FirebaseAuthModule {
}

import {Injectable} from '@angular/core';
import * as auth from 'firebase/auth';
import firebase from 'firebase/compat';
import {AngularFireAuth} from '@angular/fire/compat/auth';

export type fbErr = {
  code: number
  message: string
}

export interface LoginCallback {
  success: (uid: string, token: string) => void;
  error: (err: string) => void;
}

export interface EmailSignUpCallback {
  verificationSent: () => void;
  error: (err: string) => void;
}

export interface ForgotPasswordCallback {
  resetSent: () => void;
  error: (err: string) => void;
}

@Injectable({
  providedIn: 'root'
})
export class FirebaseAuthService {

  constructor(
    public afAuth: AngularFireAuth
  ) {
  }

  SignIn(email: string, password: string, cb: LoginCallback) {
    return this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {

        const user = result.user;
        if (user == null) {
          throw Error(`empty user data in auth credentials result`);
        }
        user.getIdToken().then((token: string) => {
          cb.success(user.uid, token);
        });

      })
      .catch((err: fbErr) => {
        console.error(err);
        cb.error(err.message);
      });
  }

  SignUp(email: string, password: string, cb: EmailSignUpCallback) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        return this.SendVerificationMail(cb);
      });
  }

  SendVerificationMail(cb: EmailSignUpCallback) {
    return this.afAuth.currentUser
      .then((user: firebase.User | null) => {
        if (user == null) {
          throw Error(`current user is null`);
        }
        return user.sendEmailVerification()
          .then(() => cb.verificationSent());
      })
      .catch((err: fbErr) => {
        console.error(err);
        cb.error(err.message);
      });
  }

  ForgotPassword(passwordResetEmail: string, cb: ForgotPasswordCallback) {
    return this.afAuth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        cb.resetSent();
      })
      .catch((err: fbErr) => {
        console.error(err);
        cb.error(err.message);
      });
  }

  GoogleAuth(cb: LoginCallback) {
    return this.AuthLogin(new auth.GoogleAuthProvider(), cb);
  }

  AppleAuth(cb: LoginCallback) {
    return this.AuthLogin(new auth.OAuthProvider('apple.com'), cb);
  }

  AuthLogin(provider: any, cb: LoginCallback) {
    return this.afAuth
      .signInWithPopup(provider)
      .then((result) => {

        const user = result.user;
        if (user == null) {
          throw Error(`empty user data in auth credentials result`);
        }
        user.getIdToken().then((token: string) => {
          cb.success(user.uid, token);
        });

      })
      .catch((err: fbErr) => {
        console.error(err);
        cb.error(err.message);
      });
  }

  SignOut() {
    return this.afAuth.signOut()
      .catch((err: fbErr) => console.log(err));
  }
}

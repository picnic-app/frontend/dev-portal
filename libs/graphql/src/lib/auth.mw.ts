import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ApolloLink } from '@apollo/client/core';

import { selectToken } from '@portal/auth';
import { ConfigService } from '@portal/core';

@Injectable()
export class GQLMiddleware {

  readonly token$ = this.store.pipe(
    select(selectToken)
  );

  private token: string | null = null;

  private baseURL?: string;

  constructor(private store: Store, private config: ConfigService) {
    this.token$.subscribe(token => {
      this.token = token;
    });
    this.config.config$.subscribe(cfg => {
      this.baseURL = cfg.apiBaseURI;
    });
  }

  get uriMW(): ApolloLink {
    return new ApolloLink((operation, forward) => {
      const baseURL = this.baseURL?.replace(/\/$/, '');
      operation.setContext({uri: `${baseURL}/query`});
      return forward(operation);
    });
  }

  get authMW(): ApolloLink {
    return new ApolloLink((operation, forward) => {
      if (this.token) {
        operation.setContext({
          headers: new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.token}`
          )
        });
      }
      return forward(operation);
    });
  }
}

import { inject, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApolloLink, DefaultOptions, InMemoryCache } from '@apollo/client/core';
import { HttpLink } from 'apollo-angular/http';
import { APOLLO_OPTIONS, ApolloModule } from 'apollo-angular';
import { ApolloClientOptions } from '@apollo/client/core/ApolloClient';

import { GQLMiddleware } from './auth.mw';
import { HTTPAuthInterceptor } from './auth.interceptor';
import { ConfigService } from '@portal/core';

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore'
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all'
  },
  mutate: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all'
  }
};

@NgModule({
  imports: [CommonModule, ApolloModule]
})
export class GraphQLModule {

  static forRoot(): ModuleWithProviders<GraphQLModule> {
    return {
      ngModule: GraphQLModule,
      providers: [
        GQLMiddleware,
        {
          provide: APOLLO_OPTIONS,
          useFactory(httpLink: HttpLink, cfg: ConfigService) {

            const uriMW: ApolloLink = inject(GQLMiddleware).uriMW;
            const authMW: ApolloLink = inject(GQLMiddleware).authMW;

            const uri = `${cfg.config?.apiBaseURI?.replace(/\/$/, '')}/query`;
            const http = httpLink.create({uri: uri});
            const link = uriMW
              .concat(authMW)
              .concat(http);

            return <ApolloClientOptions<any>>{
              link,
              cache: new InMemoryCache(),
              defaultOptions
            };
          },
          deps: [HttpLink, ConfigService]
        },
        {
          provide: HTTP_INTERCEPTORS,
          multi: true,
          useClass: HTTPAuthInterceptor
        }
      ]
    };
  }
}

import { InjectionToken } from "@angular/core";

export interface GQLConfig {
  apiBaseURI: string;
}

export const GQL_CONFIG = new InjectionToken<GQLConfig>("GQL_CONFIG");

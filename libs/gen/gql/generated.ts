import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
  DateTime: any;
  ImageUrl: any;
  Map: any;
  /** a file to be sent as input */
  Upload: any;
};

export type AcceptInvitationInput = {
  circleId: Scalars['ID'];
};

export type AcceptInvitationRequest = {
  sliceId: Scalars['ID'];
  userRequestedToJoinID: Scalars['ID'];
};

export type ActionReason = {
  __typename?: 'ActionReason';
  id: Scalars['ID'];
  reason: Scalars['String'];
};

export type ActionStatistic = {
  __typename?: 'ActionStatistic';
  ban_user: Scalars['Int'];
  delete_message: Scalars['Int'];
  resolve_circle_report: Scalars['Int'];
};

export type AddCustomBlWordsInput = {
  circleId: Scalars['ID'];
  words?: InputMaybe<Array<Scalars['String']>>;
};

/** input used in addCustomRoleToUserInCircle mutation */
export type AddCustomRoleToUserInCircleInput = {
  circleId: Scalars['String'];
  roleId: Scalars['String'];
  userId: Scalars['String'];
};

export type AddModeratorInput = {
  password: Scalars['String'];
  permissions?: InputMaybe<Array<NewModeratorPermission>>;
  username: Scalars['String'];
};

export type AddPermissionToModeratorInput = {
  custom_indexes?: InputMaybe<Array<Scalars['String']>>;
  full_access: Scalars['Boolean'];
  moderator_id: Scalars['String'];
  permission_id: Scalars['String'];
};

export type AddUsersToSliceRequest = {
  sliceId: Scalars['ID'];
  userIds: Array<Scalars['ID']>;
};

export type AdminAuthInfo = {
  __typename?: 'AdminAuthInfo';
  accessToken: Scalars['String'];
};

export type App = {
  __typename?: 'App';
  counters: AppCounters;
  createdAt: Scalars['DateTime'];
  deletedAt?: Maybe<Scalars['DateTime']>;
  deletedBy?: Maybe<Scalars['ID']>;
  description: Scalars['String'];
  id: Scalars['ID'];
  imageUrl: Scalars['String'];
  name: Scalars['String'];
  owner?: Maybe<AppOwner>;
  ownerId: Scalars['ID'];
  permissions?: Maybe<Array<AppPermission>>;
  score: Scalars['Int'];
  subscriptions?: Maybe<Array<AppSubscription>>;
  tags?: Maybe<Array<AppTag>>;
  url: Scalars['String'];
  userContext: AppUserContext;
  webhookUrl?: Maybe<Scalars['String']>;
};

export type AppCounters = {
  __typename?: 'AppCounters';
  avgRating: Scalars['Float'];
  circles: Scalars['Int'];
  ratings: Scalars['Map'];
  reviews: Scalars['Int'];
  saves: Scalars['Int'];
  upvotes: Scalars['Int'];
};

export type AppEdge = {
  __typename?: 'AppEdge';
  cursorId: Scalars['ID'];
  node: App;
};

export type AppImageInput = {
  payload: Scalars['Upload'];
};

export type AppMention = {
  __typename?: 'AppMention';
  appId: Scalars['ID'];
  label: Scalars['String'];
};

export enum AppOrder {
  ByCreatedAt = 'ByCreatedAt',
  ByScore = 'ByScore'
}

export type AppOwner = {
  __typename?: 'AppOwner';
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type AppPermission = {
  __typename?: 'AppPermission';
  description: Scalars['String'];
  descriptors: Array<Scalars['String']>;
  dxName: Scalars['String'];
  id: Scalars['ID'];
  uxName: Scalars['String'];
};

export type AppReview = {
  __typename?: 'AppReview';
  appID: Scalars['ID'];
  body: Scalars['String'];
  createdAt: Scalars['DateTime'];
  createdBy: Scalars['ID'];
  deletedAt?: Maybe<Scalars['DateTime']>;
  deletedBy?: Maybe<Scalars['ID']>;
  id: Scalars['ID'];
  rating: Scalars['Int'];
  upvotes: Scalars['Int'];
};

export type AppReviewEdge = {
  __typename?: 'AppReviewEdge';
  cursorId: Scalars['ID'];
  node: AppReview;
};

export type AppSubscription = {
  __typename?: 'AppSubscription';
  descriptor: Scalars['String'];
  id: Scalars['ID'];
};

export type AppTag = {
  __typename?: 'AppTag';
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type AppToken = {
  __typename?: 'AppToken';
  appID: Scalars['ID'];
  createdAt: Scalars['DateTime'];
  expiresAt: Scalars['DateTime'];
  name: Scalars['String'];
  revokedAt?: Maybe<Scalars['DateTime']>;
  tokenID: Scalars['ID'];
};

export type AppUserContext = {
  __typename?: 'AppUserContext';
  reviews?: Maybe<Array<AppReview>>;
  savedAt?: Maybe<Scalars['DateTime']>;
  upvotedAt?: Maybe<Scalars['DateTime']>;
  usedAt?: Maybe<Scalars['DateTime']>;
};

export type AppealInput = {
  anyId: Scalars['ID'];
  circleId: Scalars['ID'];
};

export type Attachment = {
  __typename?: 'Attachment';
  createdAt: Scalars['DateTime'];
  fileType: Scalars['String'];
  filename: Scalars['String'];
  /** attachment id */
  id: Scalars['String'];
  size: Scalars['Int'];
  /** const url, for avatars and such */
  url: Scalars['String'];
};

/** Input for upload attachment */
export type AttachmentInput = {
  payload: Scalars['Upload'];
};

export type AuthInfo = {
  __typename?: 'AuthInfo';
  accessToken: Scalars['String'];
  refreshToken: Scalars['String'];
};

export type BlwConnectionRequest = {
  circleId: Scalars['ID'];
  searchQuery?: InputMaybe<Scalars['String']>;
};

export type BlwConnectionResponse = {
  __typename?: 'BLWConnectionResponse';
  edges: Array<Maybe<BlWord>>;
  pageInfo: PageInfo;
};

export type BlWord = {
  __typename?: 'BLWord';
  word: Scalars['String'];
};

export type BanUserContext = {
  purge_collections: Scalars['Boolean'];
  purge_comments: Scalars['Boolean'];
  purge_posts: Scalars['Boolean'];
  purge_private_messages: Scalars['Boolean'];
  purge_public_messages: Scalars['Boolean'];
};

export type BanUserInput = {
  bannedTime?: InputMaybe<Scalars['Int']>;
  circleId: Scalars['ID'];
  userId: Scalars['ID'];
};

export type CancelVoteInput = {
  electionId: Scalars['ID'];
};

/** denotes a Chat between 2 or more users, can be optionally connected to a circle */
export type Chat = Node & {
  __typename?: 'Chat';
  /** optional chat image */
  chatImage?: Maybe<Scalars['ImageUrl']>;
  chatType: ChatType;
  /** circle to which the chat belongs to, can be null if its not related to circle (i.e: group chat or dm) */
  circle?: Maybe<Circle>;
  id: Scalars['ID'];
  /** context user last sent message timestamp (dm only) we need this for the read indicators */
  lastMessageSentAt?: Maybe<Scalars['DateTime']>;
  /** lists all members of this given chat, should also include current user if its participant */
  membersConnection: ChatMembersConnection;
  /**
   * sorted list of messages, by not specifiying any parameters, should return 10 latest messages in the conversation,
   * where last one is the newest one.
   *
   * `last = 10, before = "someCursorValue"` - gets 10 most recent messages posted BEFORE the one specified by the cursor
   * `first = 10, after = "someCursorValue"` - gets 10 most recent messages posted AFTER the one specified by the cursor
   */
  messagesConnection: ChatMessagesConnection;
  name: Scalars['String'];
  /** lists all participants of this given chat, should also include current user if its participant */
  participantsConnection: UsersConnection;
  /** number of people in the chat */
  participantsCount: Scalars['Int'];
  /** context user unread messages count */
  unreadMessagesCount: Scalars['Int'];
};


/** denotes a Chat between 2 or more users, can be optionally connected to a circle */
export type ChatMembersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


/** denotes a Chat between 2 or more users, can be optionally connected to a circle */
export type ChatMessagesConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


/** denotes a Chat between 2 or more users, can be optionally connected to a circle */
export type ChatParticipantsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};

export type ChatCircleInvite = {
  __typename?: 'ChatCircleInvite';
  circle: Circle;
  circleId: Scalars['ID'];
  userId: Scalars['ID'];
};

export type ChatComponentData = {
  __typename?: 'ChatComponentData';
  payload: ChatComponentPayload;
  type: ChatComponentType;
};

export type ChatComponentPayload = ChatCircleInvite | ChatGlitterBomb | ChatMessageApp | ChatMessageCircle | ChatMessagePost | ChatMessageUser | ChatSeedsExchange;

export enum ChatComponentType {
  App = 'APP',
  Circle = 'CIRCLE',
  CircleInvite = 'CIRCLE_INVITE',
  GlitterBomb = 'GLITTER_BOMB',
  Post = 'POST',
  SeedsExchange = 'SEEDS_EXCHANGE',
  User = 'USER'
}

/** chat excerpt provides general info about chat and a list of the most interesting messages */
export type ChatExcerpt = {
  __typename?: 'ChatExcerpt';
  /** type of the chat */
  chatType: ChatType;
  /** circle to which the chat belongs to, can be null if its not related to circle (i.e: group chat or dm) */
  circle?: Maybe<Circle>;
  /** id of the chat */
  id: Scalars['ID'];
  /** chat icon image url */
  imageUrl: Scalars['String'];
  /** chat language */
  language: Scalars['String'];
  /** short list of the most interesting messages */
  messages: Array<ChatMessage>;
  /** displayed name of the chat */
  name: Scalars['String'];
  /** number of people in the chat */
  participantsCount: Scalars['Int'];
};

/** chat excerpt connection node container */
export type ChatExcerptEdge = {
  __typename?: 'ChatExcerptEdge';
  cursorId: Scalars['ID'];
  node: ChatExcerpt;
};

/** paginated connection for chat excerpts */
export type ChatExcerptsConnection = {
  __typename?: 'ChatExcerptsConnection';
  edges?: Maybe<Array<ChatExcerptEdge>>;
  pageInfo: PageInfo;
};

/** chat feed container with an excerpts connection and additional info */
export type ChatFeed = {
  __typename?: 'ChatFeed';
  excerptsConnection: ChatExcerptsConnection;
  updatesCount: Scalars['Int'];
  userId: Scalars['ID'];
};


/** chat feed container with an excerpts connection and additional info */
export type ChatFeedExcerptsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};

export type ChatGlitterBomb = {
  __typename?: 'ChatGlitterBomb';
  sender: PublicProfile;
  senderId: Scalars['ID'];
};

export type ChatMember = {
  __typename?: 'ChatMember';
  lastReadMessage?: Maybe<Scalars['DateTime']>;
  role: ChatRole;
  user: PublicProfile;
  userId: Scalars['ID'];
};

export type ChatMembersConnection = {
  __typename?: 'ChatMembersConnection';
  edges?: Maybe<Array<ChatMembersEdge>>;
  pageInfo: PageInfo;
};

export type ChatMembersEdge = {
  __typename?: 'ChatMembersEdge';
  cursorId: Scalars['ID'];
  node: ChatMember;
};

export type ChatMessage = Node & {
  __typename?: 'ChatMessage';
  attachmentIds?: Maybe<Array<Scalars['ID']>>;
  attachments?: Maybe<Array<Attachment>>;
  /** user who created the message */
  author: User;
  /** id of the user who created the message */
  authorId: Scalars['ID'];
  /** id of the chat this message belongs to */
  chatId: Scalars['ID'];
  component?: Maybe<ChatComponentData>;
  /** content of the message, either text, link to image or link to video */
  content: Scalars['String'];
  /** time when this message has been created */
  createdAt: Scalars['DateTime'];
  embeds?: Maybe<Array<Embed>>;
  id: Scalars['ID'];
  /** user who created the message */
  member?: Maybe<ChatMember>;
  mentions: Mentions;
  /** list of message reactions */
  reactions?: Maybe<Array<ChatMessageReaction>>;
  /** content of the message to which this one has replied */
  replyContent?: Maybe<ChatMessage>;
  /** id of the message to which this one has replied */
  replyToId?: Maybe<Scalars['ID']>;
  /** type of message */
  type: ChatMessageType;
};

export type ChatMessageApp = {
  __typename?: 'ChatMessageApp';
  app?: Maybe<App>;
  appId: Scalars['ID'];
};

export type ChatMessageCircle = {
  __typename?: 'ChatMessageCircle';
  circle?: Maybe<Circle>;
  circleId: Scalars['ID'];
};

export type ChatMessageCircleInviteInput = {
  circleId: Scalars['ID'];
  userId: Scalars['ID'];
};

export type ChatMessageComponentInput = {
  circleInvite?: InputMaybe<ChatMessageCircleInviteInput>;
  entity?: InputMaybe<ChatMessageEntityInput>;
  glitterBomb?: InputMaybe<ChatMessageGlitterBombInput>;
  seedsExchange?: InputMaybe<ChatMessageSeedsExchangeInput>;
  type: ChatComponentType;
};

export type ChatMessageEntityInput = {
  entityId: Scalars['ID'];
};

export type ChatMessageGlitterBombInput = {
  senderId: Scalars['ID'];
};

/** input used when sending chat message */
export type ChatMessageInput = {
  attachmentIds?: InputMaybe<Array<Scalars['ID']>>;
  component?: InputMaybe<ChatMessageComponentInput>;
  content: Scalars['String'];
  mentions?: InputMaybe<MentionsInput>;
  repliedToMessageId?: InputMaybe<Scalars['ID']>;
  type: ChatMessageType;
};

export type ChatMessagePost = {
  __typename?: 'ChatMessagePost';
  post?: Maybe<Post>;
  postId: Scalars['ID'];
};

/** reaction on the message, simply an emoji under the message, like :like: */
export type ChatMessageReaction = {
  __typename?: 'ChatMessageReaction';
  /** count of users that reacted with this reaction */
  count: Scalars['Int'];
  /** whether current user used this reaction aswell */
  hasReacted: Scalars['Boolean'];
  /** the emoji used as a reaction */
  reaction: Scalars['String'];
};

export type ChatMessageSeedsExchangeInput = {
  offerId: Scalars['ID'];
  userId: Scalars['ID'];
};

export enum ChatMessageType {
  Component = 'COMPONENT',
  Text = 'TEXT'
}

export type ChatMessageUser = {
  __typename?: 'ChatMessageUser';
  user?: Maybe<PublicProfile>;
  userId: Scalars['ID'];
};

export type ChatMessagesConnection = {
  __typename?: 'ChatMessagesConnection';
  edges?: Maybe<Array<ChatMessagesEdge>>;
  pageInfo: PageInfo;
};

export type ChatMessagesEdge = {
  __typename?: 'ChatMessagesEdge';
  cursorId: Scalars['ID'];
  node: ChatMessage;
};

export enum ChatRecommendationKind {
  SharingApp = 'SharingApp',
  SharingCircle = 'SharingCircle',
  SharingPost = 'SharingPost',
  SharingUser = 'SharingUser'
}

export enum ChatRole {
  Director = 'DIRECTOR',
  Member = 'MEMBER',
  Moderator = 'MODERATOR',
  None = 'NONE'
}

export type ChatSeedsExchange = {
  __typename?: 'ChatSeedsExchange';
  offerId: Scalars['ID'];
  userId: Scalars['ID'];
};

export type ChatSettings = {
  __typename?: 'ChatSettings';
  isMuted: Scalars['Boolean'];
};

export type ChatSettingsInput = {
  isMuted?: InputMaybe<Scalars['Boolean']>;
  name?: InputMaybe<Scalars['String']>;
};

export enum ChatType {
  Circle = 'CIRCLE',
  Group = 'GROUP',
  Single = 'SINGLE'
}

export type ChatUnreadMetaData = {
  __typename?: 'ChatUnreadMetaData';
  chatId: Scalars['String'];
  chatType: ChatType;
  lastMessageAt: Scalars['DateTime'];
};

export type ChatsConnection = {
  __typename?: 'ChatsConnection';
  edges?: Maybe<Array<ChatsEdge>>;
  pageInfo: PageInfo;
};

export type ChatsEdge = {
  __typename?: 'ChatsEdge';
  cursorId: Scalars['ID'];
  node: Chat;
};

export type CheckVerificationCodePayload = {
  __typename?: 'CheckVerificationCodePayload';
  authInfo: AuthInfo;
  user: PrivateProfile;
};

export type Circle = Node & {
  __typename?: 'Circle';
  chat?: Maybe<Chat>;
  coverImageFile?: Maybe<Scalars['ImageUrl']>;
  creator: User;
  description?: Maybe<Scalars['String']>;
  group: Scalars['ID'];
  iJoined: Scalars['Boolean'];
  id: Scalars['ID'];
  image?: Maybe<Scalars['ImageUrl']>;
  imageFile?: Maybe<Scalars['ImageUrl']>;
  isBanned: Scalars['Boolean'];
  isVerified: Scalars['Boolean'];
  kind: Kind;
  languageCode: Scalars['String'];
  membersConnection: UsersConnection;
  membersCount: Scalars['Int'];
  metaWords: Array<Scalars['String']>;
  name: Scalars['String'];
  options?: Maybe<Array<Maybe<CircleOptions>>>;
  permissions?: Maybe<CirclePermissions>;
  postsConnection: PostsConnection;
  postsCount: Scalars['Int'];
  reportsCount: Scalars['Int'];
  role: CircleRole;
  roles?: Maybe<CircleMemberCustomRoles>;
  rulesText?: Maybe<Scalars['String']>;
  rulesType?: Maybe<RulesType>;
  seedsAmount?: Maybe<Seed>;
  /** list of all seed amounts belonging to this circle divided on per-user basis */
  seedsConnection: SeedsConnection;
  shareLink: Scalars['String'];
  urlName?: Maybe<Scalars['String']>;
  viewsCount: Scalars['Int'];
  visibility: Visibility;
};


export type CircleMembersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  role?: InputMaybe<CircleRole>;
};


export type CirclePostsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type CircleSeedsAmountArgs = {
  userid: Scalars['ID'];
};


export type CircleSeedsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};

export type CircleApp = {
  __typename?: 'CircleApp';
  app?: Maybe<App>;
  appID: Scalars['ID'];
  circle?: Maybe<Circle>;
  circleId: Scalars['ID'];
  createdAt: Scalars['DateTime'];
};

export type CircleAppEdge = {
  __typename?: 'CircleAppEdge';
  cursorId: Scalars['ID'];
  node: CircleApp;
};

export type CircleAppsInput = {
  circleId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
};

export type CircleAppsResponse = {
  __typename?: 'CircleAppsResponse';
  edges?: Maybe<Array<CircleAppEdge>>;
  pageInfo: PageInfo;
};

export type CircleCustomRole = {
  __typename?: 'CircleCustomRole';
  canAttachFiles: Scalars['Boolean'];
  canEmbedLinks: Scalars['Boolean'];
  canManageCircle: Scalars['Boolean'];
  canManageComments: Scalars['Boolean'];
  canManageMessages: Scalars['Boolean'];
  canManagePosts: Scalars['Boolean'];
  canManageReports: Scalars['Boolean'];
  canManageRoles: Scalars['Boolean'];
  canManageUsers: Scalars['Boolean'];
  canPost: Scalars['Boolean'];
  canSendMsg: Scalars['Boolean'];
  circleId: Scalars['ID'];
  color?: Maybe<Scalars['String']>;
  emoji: Scalars['String'];
  meta?: Maybe<CircleCustomRoleMeta>;
  name: Scalars['String'];
  roleId: Scalars['ID'];
};

export type CircleCustomRoleMeta = {
  __typename?: 'CircleCustomRoleMeta';
  assignable: Scalars['Boolean'];
  configurable: Scalars['Boolean'];
  deletable: Scalars['Boolean'];
};

export type CircleCustomRoleResponse = {
  __typename?: 'CircleCustomRoleResponse';
  roleId: Scalars['ID'];
};

export type CircleCustomRolesResponse = {
  __typename?: 'CircleCustomRolesResponse';
  edges?: Maybe<Array<CircleCustomRole>>;
};

export type CircleData = {
  __typename?: 'CircleData';
  circleId: Scalars['ID'];
  createdAt: Scalars['DateTime'];
  membersCount: Scalars['Int'];
  postsCount: Scalars['Int'];
  viewsCount: Scalars['Int'];
};

export type CircleEdge = {
  __typename?: 'CircleEdge';
  cursorId: Scalars['ID'];
  node: Circle;
};

export type CircleFeed = {
  __typename?: 'CircleFeed';
  edges?: Maybe<Array<CircleFeedEdge>>;
};

export type CircleFeedEdge = {
  __typename?: 'CircleFeedEdge';
  node: Post;
};

export type CircleJoinRequestsConnectionRequest = {
  circleId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
};

export type CircleJoinRequestsConnectionResponse = {
  __typename?: 'CircleJoinRequestsConnectionResponse';
  edges?: Maybe<Array<JoinRequestEdge>>;
  pageInfo: PageInfo;
};

export type CircleMember = {
  __typename?: 'CircleMember';
  circleId: Scalars['ID'];
  isBanned: Scalars['Boolean'];
  role: CircleRole;
  userId: Scalars['ID'];
};

export type CircleMemberCustomRoles = {
  __typename?: 'CircleMemberCustomRoles';
  circleId: Scalars['String'];
  mainRoleId: Scalars['String'];
  roles?: Maybe<Array<CircleCustomRole>>;
  unassigned?: Maybe<Array<CircleCustomRole>>;
  userId: Scalars['String'];
};

export type CircleMention = {
  __typename?: 'CircleMention';
  circleId: Scalars['ID'];
  label: Scalars['String'];
};

export type CircleMetricsData = {
  __typename?: 'CircleMetricsData';
  circle: Circle;
  dataNow: CircleData;
  dataPrevious: CircleData;
};

export type CircleMetricsDataInput = {
  period: Period;
};

export type CircleNominee = {
  __typename?: 'CircleNominee';
  userId: Scalars['ID'];
  votesCount: Scalars['Int'];
  votesPercent: Scalars['Int'];
};

export type CircleOptions = {
  __typename?: 'CircleOptions';
  description: Scalars['String'];
  displayName: Scalars['String'];
  emoji: Scalars['String'];
  name: Option;
  value: Scalars['Boolean'];
};

export type CircleParticipants = {
  __typename?: 'CircleParticipants';
  edges?: Maybe<Array<ElectionParticipantEdge>>;
  pageInfo: PageInfo;
};

export type CirclePermissions = {
  __typename?: 'CirclePermissions';
  canAttachFiles: Scalars['Boolean'];
  canEmbedLinks: Scalars['Boolean'];
  canManageCircle: Scalars['Boolean'];
  canManageComments: Scalars['Boolean'];
  canManageMessages: Scalars['Boolean'];
  canManagePosts: Scalars['Boolean'];
  canManageReports: Scalars['Boolean'];
  canManageRoles: Scalars['Boolean'];
  canManageUsers: Scalars['Boolean'];
  canPost: Scalars['Boolean'];
  canSendMsg: Scalars['Boolean'];
};

export type CirclePushNotificationInput = {
  body: Scalars['String'];
  circleId: Scalars['ID'];
  postId: Scalars['ID'];
  postLink: Scalars['String'];
};

export type CircleReport = {
  __typename?: 'CircleReport';
  anyId: Scalars['ID'];
  circleId: Scalars['ID'];
  comment: Scalars['String'];
  contentAuthor: MemberProfile;
  moderator: MemberProfile;
  reason: Reason;
  reportId: Scalars['ID'];
  reportType: ReportType;
  reporter: MemberProfile;
  resolvedAt: Scalars['DateTime'];
  status: ResolveStatus;
  userId: Scalars['ID'];
};

export type CircleReports = {
  __typename?: 'CircleReports';
  edges?: Maybe<Array<CircleReportsEdge>>;
  pageInfo: PageInfo;
};

export type CircleReportsConnectionInput = {
  circleId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
  filterBy?: InputMaybe<CircleReportsFilterBy>;
};

export type CircleReportsEdge = {
  __typename?: 'CircleReportsEdge';
  cursorId: Scalars['ID'];
  node: CircleReport;
};

export enum CircleReportsFilterBy {
  All = 'ALL',
  Resolved = 'RESOLVED',
  Unresolved = 'UNRESOLVED'
}

export enum CircleRole {
  Director = 'DIRECTOR',
  Member = 'MEMBER',
  Moderator = 'MODERATOR',
  None = 'NONE'
}

export enum CircleSortBy {
  PostsNumber = 'PostsNumber',
  ViewsNumber = 'ViewsNumber'
}

export type CircleUserCustomRolesResponse = {
  __typename?: 'CircleUserCustomRolesResponse';
  assigned?: Maybe<Array<CircleCustomRole>>;
  unassigned?: Maybe<Array<CircleCustomRole>>;
};

export type Circles = {
  __typename?: 'Circles';
  edges?: Maybe<Array<Circle>>;
  pageInfo: PageInfo;
};

export type CirclesConnection = {
  __typename?: 'CirclesConnection';
  edges?: Maybe<Array<CirclesEdge>>;
  pageInfo: PageInfo;
};

export type CirclesEdge = {
  __typename?: 'CirclesEdge';
  cursorId: Scalars['ID'];
  node: Circle;
};

export type CirclesInput = {
  coverImageFile?: InputMaybe<Scalars['Upload']>;
  description?: InputMaybe<Scalars['String']>;
  groupId?: InputMaybe<Scalars['String']>;
  hidden?: InputMaybe<Scalars['Boolean']>;
  image?: InputMaybe<Scalars['ImageUrl']>;
  imageFile?: InputMaybe<Scalars['Upload']>;
  kind?: InputMaybe<Kind>;
  languageCode?: InputMaybe<Scalars['String']>;
  metaWords?: InputMaybe<Array<Scalars['String']>>;
  name?: InputMaybe<Scalars['String']>;
  options?: InputMaybe<Array<InputMaybe<InputOptions>>>;
  parentId?: InputMaybe<Scalars['String']>;
  private?: InputMaybe<Scalars['Boolean']>;
  rulesText?: InputMaybe<Scalars['String']>;
  urlName?: InputMaybe<Scalars['String']>;
  visibility?: InputMaybe<Visibility>;
};

export type CirclesResponse = {
  __typename?: 'CirclesResponse';
  edges?: Maybe<Array<CircleEdge>>;
  pageInfo: PageInfo;
};

export enum CirclesSortType {
  NewestFirst = 'NEWEST_FIRST',
  OldestFirst = 'OLDEST_FIRST',
  PopularFirst = 'POPULAR_FIRST'
}

export type Collection = Node & {
  __typename?: 'Collection';
  counters: CollectionCounter;
  createdAt: Scalars['DateTime'];
  description: Scalars['String'];
  id: Scalars['ID'];
  isPublic: Scalars['Boolean'];
  owner: PublicProfile;
  ownerId: Scalars['ID'];
  postsConnection: PostsConnection;
  previewPosts?: Maybe<Array<Post>>;
  title: Scalars['String'];
};


export type CollectionPostsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};

export type CollectionCounter = {
  __typename?: 'CollectionCounter';
  posts: Scalars['Int'];
};

export type CollectionsConnection = {
  __typename?: 'CollectionsConnection';
  edges?: Maybe<Array<CollectionsEdge>>;
  pageInfo: PageInfo;
};

export type CollectionsEdge = {
  __typename?: 'CollectionsEdge';
  cursorId: Scalars['ID'];
  node: Collection;
};

export enum CollectionsSortType {
  NewestFirst = 'NEWEST_FIRST',
  OldestFirst = 'OLDEST_FIRST',
  PopularFirst = 'POPULAR_FIRST'
}

export type Comment = Node & {
  __typename?: 'Comment';
  attachmentIds?: Maybe<Array<Scalars['ID']>>;
  attachments?: Maybe<Array<Attachment>>;
  author?: Maybe<User>;
  authorId: Scalars['ID'];
  commentReactionsConnection: ReactionsConnection;
  context: CommentContext;
  createdAt: Scalars['DateTime'];
  deletedAt?: Maybe<Scalars['DateTime']>;
  embeds?: Maybe<Array<Embed>>;
  iReacted?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  /**
   * Deprecated: use Reactions instead
   * @deprecated use reactions instead
   */
  likesCount: Scalars['Int'];
  mentions: Mentions;
  parent?: Maybe<Comment>;
  parentId?: Maybe<Scalars['ID']>;
  pin?: Maybe<Pin>;
  postId?: Maybe<Scalars['ID']>;
  reactions: Scalars['Map'];
  repliesConnection: CommentsConnection;
  repliesCount: Scalars['Int'];
  text: Scalars['String'];
};


export type CommentCommentReactionsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type CommentRepliesConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};

export type CommentContext = {
  __typename?: 'CommentContext';
  reaction: Scalars['String'];
};

export type CommentsConnection = {
  __typename?: 'CommentsConnection';
  edges?: Maybe<Array<CommentsEdge>>;
  pageInfo: PageInfo;
  pinnedComments?: Maybe<Array<Maybe<Comment>>>;
  postId: Scalars['ID'];
  returnPinned?: Maybe<Scalars['Boolean']>;
};

export type CommentsEdge = ConnectionEdge & {
  __typename?: 'CommentsEdge';
  cursorId: Scalars['ID'];
  node: Comment;
};

export type ConfigureDiscordWebhookInput = {
  circleId: Scalars['ID'];
  webhookUrl: Scalars['String'];
};

export type Connection = {
  edges?: Maybe<Array<ConnectionEdge>>;
  pageInfo: PageInfo;
};

export type ConnectionEdge = {
  cursorId: Scalars['ID'];
  node: Node;
};

export type ContactMention = {
  __typename?: 'ContactMention';
  contactId: Scalars['ID'];
  label: Scalars['String'];
};

export type ContactPhoneNumber = {
  __typename?: 'ContactPhoneNumber';
  label: Scalars['String'];
  number: Scalars['String'];
};

export type ContactPhoneNumberInput = {
  label: Scalars['String'];
  number: Scalars['String'];
};

export type ContentStatsForCircle = {
  __typename?: 'ContentStatsForCircle';
  comments: Scalars['Int'];
  likes: Scalars['Int'];
  members: Scalars['Int'];
  posts: Scalars['Int'];
  views: Scalars['Int'];
};

export type ContentStatsForContent = {
  __typename?: 'ContentStatsForContent';
  comments: Scalars['Int'];
  impressions: Scalars['Int'];
  /**
   * Deprecated: use Reactions instead.
   * @deprecated Use `reactions` instead
   */
  likes: Scalars['Int'];
  reactions: Scalars['Map'];
  saves: Scalars['Int'];
  shares: Scalars['Int'];
  /**
   * Deprecated: use Impressions instead.
   * @deprecated Use `impressions` instead
   */
  views: Scalars['Int'];
};

export type ContentStatsForProfile = {
  __typename?: 'ContentStatsForProfile';
  comments: Scalars['Int'];
  followers: Scalars['Int'];
  following: Scalars['Int'];
  likes: Scalars['Int'];
  posts: Scalars['Int'];
  views: Scalars['Int'];
};

export type CountData = {
  __typename?: 'CountData';
  count: Scalars['Int'];
  time: Scalars['DateTime'];
};

export type CountDataInput = {
  from?: InputMaybe<Scalars['String']>;
  period?: InputMaybe<Period>;
};

/** input used in createCircle mutation */
export type CreatCirclesInput = {
  payload: CirclesInput;
};

export type CreatSliceInput = {
  payload: SliceInput;
};

export type CreateAppInput = {
  description: Scalars['String'];
  imageUrl: Scalars['String'];
  name: Scalars['String'];
  permissionIds?: InputMaybe<Array<Scalars['ID']>>;
  subscriptionIds?: InputMaybe<Array<Scalars['ID']>>;
  tagIds?: InputMaybe<Array<Scalars['ID']>>;
  url: Scalars['String'];
  webhookUrl?: InputMaybe<Scalars['String']>;
};

export type CreateAppReviewInput = {
  appID: Scalars['ID'];
  body?: InputMaybe<Scalars['String']>;
  rating: Scalars['Int'];
};

export type CreateCircleCustomRoleInput = {
  canAttachFiles: Scalars['Boolean'];
  canEmbedLinks: Scalars['Boolean'];
  canManageCircle: Scalars['Boolean'];
  canManageComments: Scalars['Boolean'];
  canManageMessages: Scalars['Boolean'];
  canManagePosts: Scalars['Boolean'];
  canManageReports: Scalars['Boolean'];
  canManageRoles: Scalars['Boolean'];
  canManageUsers: Scalars['Boolean'];
  canPost: Scalars['Boolean'];
  canSendMsg: Scalars['Boolean'];
  circleId: Scalars['ID'];
  color: Scalars['String'];
  emoji?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
};

export type CreateCollectionInput = {
  description: Scalars['String'];
  isPublic: Scalars['Boolean'];
  title: Scalars['String'];
};

/** CreateCommentInput is input used to create a comment */
export type CreateCommentInput = {
  attachmentIds?: InputMaybe<Array<Scalars['ID']>>;
  mentions?: InputMaybe<MentionsInput>;
  parentId?: InputMaybe<Scalars['ID']>;
  postId: Scalars['ID'];
  text: Scalars['String'];
};

/** input used when creating post */
export type CreatePostInput = {
  /** circle to which the given is being posted. User must be a member of that circle */
  circleId: Scalars['ID'];
  imageContent?: InputMaybe<ImagePostContentInput>;
  linkContent?: InputMaybe<LinkPostContentInput>;
  mentions?: InputMaybe<MentionsInput>;
  pollContent?: InputMaybe<PollPostContentInput>;
  /** optional sound id */
  soundId?: InputMaybe<Scalars['ID']>;
  textContent?: InputMaybe<TextPostContentInput>;
  title: Scalars['String'];
  /** based on the type, provide one of the following contents. it can be only one content per input */
  type: PostType;
  videoContent?: InputMaybe<VideoPostContentInput>;
};

export type CreateReportInput = {
  description: Scalars['String'];
  entity: ReportEntity;
  entityID: Scalars['ID'];
  reason: Scalars['String'];
};

export type CreateReportReasonInput = {
  entityType: ReportEntity;
  reason: Scalars['String'];
};

export enum CursorDirection {
  Back = 'back',
  Forward = 'forward'
}

/** Used in graphql connection to indicate the element from which to start pagination. specified in `Edge` type for given pagination */
export type CursorInput = {
  /** defaults to 'forward' */
  dir?: InputMaybe<CursorDirection>;
  /** Edge's cursor id value after/before which we want to start receiving results in the page */
  id: Scalars['ID'];
  /** defaults to 10 */
  limit?: InputMaybe<Scalars['Int']>;
};

export type DebugOptionInput = {
  accessTokenTtlSeconds: Scalars['Int'];
  refreshTokenTtlSeconds: Scalars['Int'];
};

export type DeclineInvitationInput = {
  circleId: Scalars['ID'];
};

export type DeclineInvitationRequest = {
  sliceId: Scalars['ID'];
  userRequestedToJoinID: Scalars['ID'];
};

export type DefaultCircleConfigOptionsResponse = {
  __typename?: 'DefaultCircleConfigOptionsResponse';
  options?: Maybe<Array<Maybe<CircleOptions>>>;
};

export type DeleteCircleCustomRoleInput = {
  circleId: Scalars['ID'];
  roleId: Scalars['ID'];
};

/** input used in deleteCircle mutation */
export type DeleteCirclesInput = {
  circleId: Scalars['ID'];
};

export type DeleteCommentInput = {
  id: Scalars['ID'];
};

/** input used in deleteCustomRoleFromUserInCircle mutation */
export type DeleteCustomRoleFromUserInCircleInput = {
  circleId: Scalars['String'];
  roleId: Scalars['String'];
  userId: Scalars['String'];
};

export type DeletePostInput = {
  id: Scalars['String'];
  reason?: InputMaybe<Scalars['String']>;
};

export type DeletePostsInput = {
  ids?: InputMaybe<Array<Scalars['String']>>;
  reason?: InputMaybe<Scalars['String']>;
};

export type DeleteSliceInput = {
  sliceID: Scalars['ID'];
};

export type DeleteTestUserPayload = {
  __typename?: 'DeleteTestUserPayload';
  success: Scalars['Boolean'];
};

export type DeleteUserMessagesInCirclesInput = {
  author_id: Scalars['String'];
  circle_id?: InputMaybe<Scalars['String']>;
  from?: InputMaybe<Scalars['String']>;
  to?: InputMaybe<Scalars['String']>;
};

export type DirectorVote = {
  __typename?: 'DirectorVote';
  candidate: VoteCandidate;
  candidateId: Scalars['ID'];
  circleId: Scalars['ID'];
};

export type DisableAppInput = {
  appId: Scalars['ID'];
  circleId: Scalars['ID'];
};

export type DiscordConfigResponse = {
  __typename?: 'DiscordConfigResponse';
  webhookConfigured: Scalars['Boolean'];
};

export type DiscordTokenInput = {
  code: Scalars['String'];
  redirectUri?: InputMaybe<Scalars['String']>;
};

export type Election = {
  __typename?: 'Election';
  circleId: Scalars['ID'];
  circleMembersCount: Scalars['Int'];
  dueTo: Scalars['DateTime'];
  iVoted: Scalars['Boolean'];
  id: Scalars['ID'];
  isSeedHolder: Scalars['Boolean'];
  maxSeedsVoted: Scalars['Int'];
  membersVoted: Scalars['Int'];
  seedsVoted: Scalars['Int'];
  votesCount: Scalars['Int'];
  votesPercent: Scalars['Int'];
};

export type ElectionInput = {
  circleId: Scalars['ID'];
};

export type ElectionParticipant = {
  __typename?: 'ElectionParticipant';
  addModerators: Scalars['Boolean'];
  bannedAt?: Maybe<Scalars['DateTime']>;
  bannedTime?: Maybe<Scalars['Int']>;
  circleId: Scalars['ID'];
  iVoted: Scalars['Boolean'];
  isBanned: Scalars['Boolean'];
  role: CircleRole;
  roles?: Maybe<CircleMemberCustomRoles>;
  user: MemberProfile;
  userId: Scalars['ID'];
  votesCount: Scalars['Int'];
  votesPercent: Scalars['Int'];
};

export type ElectionParticipantEdge = {
  __typename?: 'ElectionParticipantEdge';
  cursorId: Scalars['ID'];
  node: ElectionParticipant;
};

export type ElectionParticipantsConnectionInput = {
  circleId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
  votes: Scalars['Boolean'];
};

export type ElectionVoteInput = {
  electionId: Scalars['ID'];
  nomineeId: Scalars['ID'];
  seedsVoted?: InputMaybe<Scalars['Int']>;
};

export type Embed = {
  __typename?: 'Embed';
  id: Scalars['ID'];
  linkMetaData: LinkMetaData;
  status: EmbedStatus;
};

export enum EmbedStatus {
  Error = 'ERROR',
  Loading = 'LOADING',
  Success = 'SUCCESS'
}

export type EnableAppInput = {
  appId: Scalars['ID'];
  circleId: Scalars['ID'];
};

export type Endpoint = {
  __typename?: 'Endpoint';
  endpoint: Scalars['String'];
  permissions?: Maybe<Array<Scalars['String']>>;
};

export enum EntityType {
  Chat = 'CHAT',
  Comment = 'COMMENT',
  Post = 'POST'
}

/** Error type returned in response payload */
export type Error = {
  __typename?: 'Error';
  /** string code that identifies this type of an error. i.e: 'post-doesnt-exist' */
  code: Scalars['String'];
  message?: Maybe<Scalars['String']>;
};

/** ExchangeSeed type containing info about amount, circle to which it belongs to */
export type ExchangeSeed = {
  __typename?: 'ExchangeSeed';
  amount: Scalars['Int'];
  circleId: Scalars['ID'];
};

export type Feed = {
  __typename?: 'Feed';
  /** circle this feed is originating from, or null */
  circle?: Maybe<Circle>;
  circleId?: Maybe<Scalars['ID']>;
  id: Scalars['ID'];
  /** title of the feed, probably circle's name, but can also be 'Popular' for example */
  name: Scalars['String'];
  posts: PostsConnection;
  type: FeedType;
};


export type FeedPostsArgs = {
  cursor?: InputMaybe<CursorInput>;
};

/** contains top trending circles for specified group */
export type FeedGroup = {
  __typename?: 'FeedGroup';
  group: Group;
  topCircles?: Maybe<Array<Circle>>;
};

export enum FeedType {
  Circle = 'CIRCLE',
  Custom = 'CUSTOM',
  Explore = 'EXPLORE',
  Slice = 'SLICE',
  User = 'USER'
}

export type FeedsConnection = {
  __typename?: 'FeedsConnection';
  edges?: Maybe<Array<FeedsEdge>>;
  pageInfo: PageInfo;
};

export type FeedsEdge = {
  __typename?: 'FeedsEdge';
  cursorId: Scalars['ID'];
  node: Feed;
};

/** Auth input with firebase-related auth info */
export type FirebaseAuthInput = {
  accessToken: Scalars['String'];
  thirdPartyUserid: Scalars['ID'];
};

export type FirebaseTokenResponse = {
  __typename?: 'FirebaseTokenResponse';
  firebaseToken: Scalars['String'];
};

export type GeneratedToken = {
  __typename?: 'GeneratedToken';
  jwtToken: Scalars['String'];
  tokenID: Scalars['ID'];
};

export type GetAppReviewsResponse = {
  __typename?: 'GetAppReviewsResponse';
  edges?: Maybe<Array<AppReviewEdge>>;
  pageInfo: PageInfo;
};

export type GetAppsEnabledByDefaultInput = {
  cursor?: InputMaybe<CursorInput>;
};

export type GetAppsEnabledByDefaultResponse = {
  __typename?: 'GetAppsEnabledByDefaultResponse';
  edges?: Maybe<Array<AppEdge>>;
  pageInfo: PageInfo;
};

export type GetCircleAppsInput = {
  appId?: InputMaybe<Scalars['ID']>;
  circleId?: InputMaybe<Scalars['ID']>;
  cursor?: InputMaybe<CursorInput>;
};

export type GetCircleAppsResponse = {
  __typename?: 'GetCircleAppsResponse';
  edges?: Maybe<Array<CircleAppEdge>>;
  pageInfo: PageInfo;
};

export type GetCircleByIdRequest = {
  circleId: Scalars['ID'];
  permissions?: InputMaybe<Scalars['Boolean']>;
};

export type GetCircleByNameRequest = {
  name: Scalars['String'];
};

export type GetCommentReactionsInput = {
  commentId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
};

export type GetCommentsInput = {
  cursor?: InputMaybe<CursorInput>;
  parentId: Scalars['String'];
  postId: Scalars['ID'];
  returnPinned?: InputMaybe<Scalars['Boolean']>;
};

export type GetFeaturedAppsInput = {
  cursor?: InputMaybe<CursorInput>;
};

export type GetFeaturedAppsResponse = {
  __typename?: 'GetFeaturedAppsResponse';
  edges?: Maybe<Array<AppEdge>>;
  pageInfo: PageInfo;
};

export type GetFeaturedCirclesResponse = {
  __typename?: 'GetFeaturedCirclesResponse';
  edges?: Maybe<Array<CircleEdge>>;
  pageInfo: PageInfo;
};

export type GetLastUsedAppsResponse = {
  __typename?: 'GetLastUsedAppsResponse';
  edges?: Maybe<Array<AppEdge>>;
  pageInfo: PageInfo;
};

export type GetLastViewedCirclesResponse = {
  __typename?: 'GetLastViewedCirclesResponse';
  edges?: Maybe<Array<CircleEdge>>;
  pageInfo: PageInfo;
};

export type GetMembersInput = {
  circleId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
  isBanned?: InputMaybe<Scalars['Boolean']>;
  roles?: InputMaybe<Array<InputMaybe<CircleRole>>>;
  searchQuery?: InputMaybe<Scalars['String']>;
  withCustomRoles?: InputMaybe<Scalars['Boolean']>;
};

export type GetPostReactionsInput = {
  cursor?: InputMaybe<CursorInput>;
  postId: Scalars['ID'];
};

export type GetRecommendedChatsContext = {
  appId?: InputMaybe<Scalars['ID']>;
  circleId?: InputMaybe<Scalars['ID']>;
  postId?: InputMaybe<Scalars['ID']>;
  userId?: InputMaybe<Scalars['ID']>;
};

export type GetRecommendedChatsInput = {
  context?: InputMaybe<GetRecommendedChatsContext>;
  cursor?: InputMaybe<CursorInput>;
  kind: ChatRecommendationKind;
  search?: InputMaybe<Scalars['String']>;
};

export type GetRecommendedChatsResponse = {
  __typename?: 'GetRecommendedChatsResponse';
  edges?: Maybe<Array<ChatsEdge>>;
  pageInfo: PageInfo;
};

export type GetRecommendedCirclesContext = {
  appId?: InputMaybe<Scalars['ID']>;
};

export type GetRecommendedCirclesInput = {
  context?: InputMaybe<GetRecommendedCirclesContext>;
  cursor?: InputMaybe<CursorInput>;
  kind: GetRecommendedCirclesRecommendationKind;
  search?: InputMaybe<Scalars['String']>;
};

export enum GetRecommendedCirclesRecommendationKind {
  CreatingPost = 'CreatingPost',
  EnablingPod = 'EnablingPod',
  LaunchingPod = 'LaunchingPod'
}

export type GetRecommendedCirclesResponse = {
  __typename?: 'GetRecommendedCirclesResponse';
  edges?: Maybe<Array<CircleEdge>>;
  pageInfo: PageInfo;
};

export type GetSavedAppsInput = {
  cursor?: InputMaybe<CursorInput>;
};

export type GetSavedAppsResponse = {
  __typename?: 'GetSavedAppsResponse';
  edges?: Maybe<Array<AppEdge>>;
  pageInfo: PageInfo;
};

export type GetSavedSearchResultsResponse = {
  __typename?: 'GetSavedSearchResultsResponse';
  edges?: Maybe<Array<SearchResultEdge>>;
  pageInfo: PageInfo;
};

export type GetSignInCaptchaParamsPayload = {
  __typename?: 'GetSignInCaptchaParamsPayload';
  recaptchaSiteKey: Scalars['String'];
};

export type GetSliceByIdInputRequest = {
  sliceId: Scalars['ID'];
};

export type GetTrendingAppsInput = {
  cursor?: InputMaybe<CursorInput>;
};

export type GetTrendingAppsResponse = {
  __typename?: 'GetTrendingAppsResponse';
  edges?: Maybe<Array<AppEdge>>;
  pageInfo: PageInfo;
};

export type GetUserCirclesRequest = {
  cursor?: InputMaybe<CursorInput>;
  permissions?: InputMaybe<Scalars['Boolean']>;
  roles?: InputMaybe<Array<InputMaybe<CircleRole>>>;
  searchQuery?: InputMaybe<Scalars['String']>;
  /** if ID = 0, will be picked up from context */
  userId?: InputMaybe<Scalars['ID']>;
};

export type GetUserContactsResponse = {
  __typename?: 'GetUserContactsResponse';
  contacts?: Maybe<Array<UserContact>>;
  info: PageInfo;
};

/** object containing all the information about circle governance */
export type Governance = {
  __typename?: 'Governance';
  allVotes: Array<DirectorVote>;
  allVotesTotal: Scalars['Int'];
  circleId: Scalars['ID'];
  mySeedsCount: Scalars['Int'];
  myVotes: Array<DirectorVote>;
  myVotesTotal: Scalars['Int'];
};

export type Group = {
  __typename?: 'Group';
  circles?: Maybe<Array<Maybe<Circle>>>;
  groupId: Scalars['ID'];
  name: Scalars['String'];
};

export type GroupChatInput = {
  /** chat name */
  name: Scalars['String'];
  /** ids of participants  */
  userIds: Array<Scalars['ID']>;
};

export type Groups = {
  __typename?: 'Groups';
  edges?: Maybe<Array<GroupsEdge>>;
};

export type GroupsEdge = {
  __typename?: 'GroupsEdge';
  node: Group;
};

/** Response payload interface declaring optionally successful operation with no error */
export type ISuccessPayload = {
  success: Scalars['Boolean'];
};

export type ImagePostContent = {
  __typename?: 'ImagePostContent';
  /** image caption */
  text: Scalars['String'];
  thumbnailUrl?: Maybe<Scalars['ImageUrl']>;
  url: Scalars['String'];
};

/** input for creating image post */
export type ImagePostContentInput = {
  imageFile: Scalars['Upload'];
  text?: InputMaybe<Scalars['String']>;
};

export type InitCircleGovInput = {
  circleId: Scalars['ID'];
  directorId: Scalars['ID'];
  kind: Scalars['Int'];
};

export type InputOptions = {
  description: Scalars['String'];
  displayName: Scalars['String'];
  emoji: Scalars['String'];
  name: Option;
  value: Scalars['Boolean'];
};

export type InviteToCircleInput = {
  circleId: Scalars['ID'];
  userIDs?: InputMaybe<Array<Scalars['ID']>>;
};

export type IsTokenBlacklistResponse = {
  __typename?: 'IsTokenBlacklistResponse';
  is_blacklisted: Scalars['Boolean'];
};

export type IssuedToken = {
  __typename?: 'IssuedToken';
  jwtToken: Scalars['String'];
};

/** input used in joinCircles mutation */
export type JoinCirclesInput = {
  circleIds: Array<Scalars['ID']>;
};

export type JoinRequest = {
  __typename?: 'JoinRequest';
  acceptedAt?: Maybe<Scalars['DateTime']>;
  circleId: Scalars['ID'];
  createdAt: Scalars['DateTime'];
  declinedAt?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  userId: Scalars['ID'];
};

export type JoinRequestEdge = {
  __typename?: 'JoinRequestEdge';
  cursorId: Scalars['ID'];
  node: JoinRequest;
};

export type JoinSlicesRequest = {
  sliceId: Scalars['ID'];
};

export enum Kind {
  Democratic = 'DEMOCRATIC',
  Director = 'DIRECTOR'
}

export type Language = {
  __typename?: 'Language';
  code: Scalars['String'];
  name: Scalars['String'];
};

export enum LanguageAction {
  Disable = 'DISABLE',
  Enable = 'ENABLE'
}

export enum LanguageFilter {
  AllAvailable = 'ALL_AVAILABLE',
  Enabled = 'ENABLED',
  NotEnabled = 'NOT_ENABLED',
  Unspecified = 'UNSPECIFIED'
}

export type LanguageInput = {
  base: Scalars['String'];
  flag: Scalars['String'];
  iso3: Scalars['String'];
  name: Scalars['String'];
  nativeName: Scalars['String'];
  tag: Scalars['String'];
};

export type LanguageTag = {
  /** language tag */
  tag: Scalars['String'];
};

export type LeaveSliceRequest = {
  sliceId: Scalars['ID'];
};

export type LinkAccountsPayload = {
  __typename?: 'LinkAccountsPayload';
  discord?: Maybe<LinkedDiscordAccount>;
  roblox?: Maybe<LinkedRobloxAccount>;
};

export type LinkDiscordAccountInput = {
  code: Scalars['String'];
  redirectUri?: InputMaybe<Scalars['String']>;
};

export type LinkDiscordAccountPayload = {
  __typename?: 'LinkDiscordAccountPayload';
  account: LinkedDiscordAccount;
};

export type LinkMetaData = {
  __typename?: 'LinkMetaData';
  /** opengraph-like description for the link */
  description?: Maybe<Scalars['String']>;
  /** host of the link */
  host?: Maybe<Scalars['String']>;
  /** opengraph-like image for the link */
  imageUrl?: Maybe<Scalars['String']>;
  /** opengraph-like tile for the link */
  title?: Maybe<Scalars['String']>;
  /** full url */
  url?: Maybe<Scalars['String']>;
};

export type LinkMetaDataResponse = {
  __typename?: 'LinkMetaDataResponse';
  data: LinkMetaData;
};

export type LinkPostContent = {
  __typename?: 'LinkPostContent';
  linkMetaData?: Maybe<LinkMetaData>;
  /** link caption */
  text: Scalars['String'];
  url: Scalars['String'];
};

/** input for creating link post */
export type LinkPostContentInput = {
  text?: InputMaybe<Scalars['String']>;
  url: Scalars['String'];
};

export type LinkRobloxAccountInput = {
  code: Scalars['String'];
  redirectUri?: InputMaybe<Scalars['String']>;
};

export type LinkRobloxAccountPayload = {
  __typename?: 'LinkRobloxAccountPayload';
  account: LinkedRobloxAccount;
};

export type LinkedDiscordAccount = {
  __typename?: 'LinkedDiscordAccount';
  discriminator: Scalars['String'];
  linkedDate: Scalars['Date'];
  profileURL: Scalars['String'];
  username: Scalars['String'];
};

export type LinkedRobloxAccount = {
  __typename?: 'LinkedRobloxAccount';
  createdAt: Scalars['DateTime'];
  linkedDate: Scalars['Date'];
  name: Scalars['String'];
  nickname: Scalars['String'];
  preferredUsername: Scalars['String'];
  profileURL: Scalars['String'];
};

export type ListGroupsInput = {
  isTrending?: InputMaybe<Scalars['Boolean']>;
  isWithCircles?: InputMaybe<Scalars['Boolean']>;
};

/** Melon type containing info about amount and user possessing the melons */
export type Melon = {
  __typename?: 'Melon';
  amountAvailable: Scalars['Int'];
  amountLocked: Scalars['Int'];
  amountTotal: Scalars['Int'];
  owner?: Maybe<User>;
};

export type MemberProfile = {
  __typename?: 'MemberProfile';
  age: Scalars['Int'];
  bio?: Maybe<Scalars['String']>;
  followers: Scalars['Int'];
  followsMe: Scalars['Boolean'];
  fullName: Scalars['String'];
  id: Scalars['ID'];
  isBlocked: Scalars['Boolean'];
  isFollowing: Scalars['Boolean'];
  isVerified: Scalars['Boolean'];
  likes: Scalars['Int'];
  profileImage: Scalars['ImageUrl'];
  username: Scalars['String'];
  views: Scalars['Int'];
};

export type Mention = {
  __typename?: 'Mention';
  avatar?: Maybe<Scalars['String']>;
  followersCount?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  phoneNumber?: Maybe<ContactPhoneNumber>;
  userType: UserType;
};

export type Mentions = {
  __typename?: 'Mentions';
  apps?: Maybe<Array<AppMention>>;
  circles?: Maybe<Array<CircleMention>>;
  contacts?: Maybe<Array<ContactMention>>;
  users?: Maybe<Array<UserMention>>;
};

export type MentionsConnection = {
  __typename?: 'MentionsConnection';
  edges?: Maybe<Array<MentionsEdge>>;
  pageInfo?: Maybe<PageInfo>;
};

export type MentionsEdge = {
  __typename?: 'MentionsEdge';
  cursorId: Scalars['ID'];
  node: Mention;
};

export type MentionsInput = {
  apps?: InputMaybe<Array<Scalars['ID']>>;
  circles?: InputMaybe<Array<Scalars['ID']>>;
  contacts?: InputMaybe<Array<Scalars['ID']>>;
  users?: InputMaybe<Array<Scalars['ID']>>;
};

export type Moderator = {
  __typename?: 'Moderator';
  Role?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  username: Scalars['String'];
};

export type ModeratorAction = {
  __typename?: 'ModeratorAction';
  Reason?: Maybe<Scalars['String']>;
  action: Scalars['String'];
  created_at: Scalars['DateTime'];
  entity_id: Scalars['ID'];
  id: Scalars['ID'];
  moderator: Moderator;
};

export type ModeratorActionId = {
  __typename?: 'ModeratorActionID';
  id: Scalars['ID'];
};

export type ModeratorActionsConnection = {
  __typename?: 'ModeratorActionsConnection';
  edges?: Maybe<Array<ModeratorAction>>;
  pageInfo: PageInfo;
};

export type ModeratorPermission = {
  __typename?: 'ModeratorPermission';
  custom_indexes?: Maybe<Array<Scalars['String']>>;
  full_access: Scalars['Boolean'];
  id: Scalars['String'];
  permission_id: Scalars['String'];
  permission_title: Scalars['String'];
};

export type ModeratorRoleSettingsInput = {
  addModerators?: InputMaybe<Scalars['Boolean']>;
  /** whether given mod can add other users as mods to circle */
  isModerator?: InputMaybe<Scalars['Boolean']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  UnverifyUser: ModeratorActionId;
  /** updates custom role in circle */
  UpdateCountersInBackground?: Maybe<SuccessPayload>;
  /** accept request */
  accept: Slice;
  /** accept invitation to circle */
  acceptInvitation: Circle;
  /** Accept an offer. This will consecutively change offer status in this order: accepted -> processing -> completed */
  acceptOffer: SuccessPayload;
  addCircleToTrack: SuccessPayload;
  /** adds new blacklisted words to circle */
  addCustomBLWords?: Maybe<SuccessPayload>;
  /** add custom role to user in circle */
  addCustomRoleToUserInCircle?: Maybe<SuccessPayload>;
  addLanguage?: Maybe<SuccessPayload>;
  addModerator: SuccessPayload;
  addPermissionToApp: SuccessPayload;
  addPermissionToModerator?: Maybe<SuccessPayload>;
  /** adds post to a collection */
  addPostToCollection: SuccessPayload;
  addReasonToModeratorAction?: Maybe<SuccessPayload>;
  addSubscriptionToApp: SuccessPayload;
  addTagToApp: SuccessPayload;
  addTagToCircle: SuccessPayload;
  addTagToGender: SuccessPayload;
  /** adds user contacts */
  addUserContacts: SuccessPayload;
  /** add user to chat */
  addUserToChat: SuccessPayload;
  /** add user to slice */
  addUsersToSlice: Slice;
  /** appellation to report on post */
  appeal?: Maybe<SuccessPayload>;
  /** bans user in circle */
  banUserInCircle: CircleMember;
  /** Add new words to the global collection of blacklisted words. */
  blacklistCreateGlobalWords?: Maybe<Array<Scalars['String']>>;
  /** Add new urls to the blacklisted urls. */
  blacklistCreateUrls?: Maybe<Array<Scalars['String']>>;
  /** Delete words from the global collection of blacklisted words by IDs. */
  blacklistDeleteGlobalWords: SuccessPayload;
  /** Delete urls from the blacklisted urls. */
  blacklistDeleteUrls: SuccessPayload;
  /** Search for a blacklisted url. */
  blacklistSearchForUrls: UrlsList;
  /** Update existing word */
  blacklistUpdateGlobalWord: SuccessPayload;
  /** Update existing url */
  blacklistUpdateUrl: SuccessPayload;
  /** Cancel an offer */
  cancelOffer: SuccessPayload;
  /** @deprecated Unsupported */
  cancelVote: SuccessPayload;
  /** makes current user block/unblock given user based on shouldBlock bool value */
  changeBlockStatus: SuccessPayload;
  /** makes current user follow/unfollow given user based on shouldFollow bool value */
  changeFollowStatus: SuccessPayload;
  /** confirms user verification code */
  checkVerificationCode: CheckVerificationCodePayload;
  /** configure discord webhook */
  configureDiscordWebhook?: Maybe<SuccessPayload>;
  createActionReason: SuccessPayload;
  createApp: App;
  createAppReview: AppReview;
  /** create circle */
  createCircle: Circle;
  /** creates new custom role in circle */
  createCircleCustomRole: CircleCustomRoleResponse;
  /** creates new collection */
  createCollection: Collection;
  /** creates new Comment */
  createComment: Comment;
  createFirebaseToken: FirebaseTokenResponse;
  /** creates new group chat */
  createGroupChat: Chat;
  createPermission?: Maybe<SuccessPayload>;
  /** creates new post */
  createPost: Post;
  createReport?: Maybe<SuccessPayload>;
  createReportReason?: Maybe<SuccessPayload>;
  /** creates new single chat */
  createSingleChat: Chat;
  /** create slice */
  createSlice: Slice;
  createTag: Tag;
  /** decline request */
  decline: Slice;
  /** decline invitation to circle */
  declineInvitation: Circle;
  /** delete account request for a user */
  deleteAccountRequest: SuccessPayload;
  deleteActionReason: SuccessPayload;
  deleteApp: SuccessPayload;
  deleteAppReview: SuccessPayload;
  deleteAppToken: SuccessPayload;
  /** deletes given chat */
  deleteChat: SuccessPayload;
  deleteCircle: ModeratorActionId;
  /** delete custom role in circle */
  deleteCircleCustomRole?: Maybe<SuccessPayload>;
  /** deletes collection */
  deleteCollection: SuccessPayload;
  /** Delete comment */
  deleteComment: SuccessPayload;
  deleteCommentAdmin: ModeratorActionId;
  deleteContacts: SuccessPayload;
  /** deletes custom role from user in circle */
  deleteCustomRoleFromUserInCircle?: Maybe<SuccessPayload>;
  /** deletes given message from chat */
  deleteMessage: SuccessPayload;
  deleteMessageAdmin: ModeratorActionId;
  deleteModerator?: Maybe<SuccessPayload>;
  deleteModeratorPermission?: Maybe<SuccessPayload>;
  deletePermission?: Maybe<SuccessPayload>;
  /** deletes a post */
  deletePost: SuccessPayload;
  deletePostAdmin: ModeratorActionId;
  /** deletes posts */
  deletePosts: SuccessPayload;
  /** removes posts from a collection */
  deletePostsFromCollection: SuccessPayload;
  /** delete slice */
  deleteSlice: SuccessPayload;
  deleteTag: SuccessPayload;
  deleteTagFromCircle: SuccessPayload;
  deleteTagFromGender: SuccessPayload;
  /** delete test user by a whitelisted phone number */
  deleteTestUser: DeleteTestUserPayload;
  deleteTrackedCircle: SuccessPayload;
  deleteUserMessagesInCircles: ModeratorActionId;
  disableApp: SuccessPayload;
  /** Deletes value for a given key */
  documentDelete: SuccessPayload;
  /** Upserts the value for a given key */
  documentPut: SuccessPayload;
  /** @deprecated Use 'voteForDirector' instead */
  electionVote: CircleNominee;
  enableApp: SuccessPayload;
  featureApp: SuccessPayload;
  /** feature circle */
  featureCircle: SuccessPayload;
  generateAppToken: GeneratedToken;
  generateUserScopedAppToken: GeneratedToken;
  getMessageAdmin: ChatMessage;
  getModeratorPermissions?: Maybe<Array<ModeratorPermission>>;
  /** get privacy settings for a user */
  getPrivacySettings: PrivacySettings;
  getUserStats: UserStats;
  grantAppPermissionsInCircle: SuccessPayload;
  grantAppSubscriptionsInCircle: SuccessPayload;
  /** invite user to chat */
  inviteUserToChat: SuccessPayload;
  /** invade users to circle */
  inviteUsersToCircle: Circle;
  isTokenBlacklisted: IsTokenBlacklistResponse;
  issueAppToken: IssuedToken;
  /** joins given chat */
  joinChat: SuccessPayload;
  /** joins given user to the specified list of circles */
  joinCircles: SuccessPayload;
  /** joins slice */
  joinSlices: SuccessPayload;
  /** leaves from given chat */
  leaveChat: SuccessPayload;
  /** removes given user from members of specified list of circles */
  leaveCircles: SuccessPayload;
  /** leaves slice */
  leaveSlice: SuccessPayload;
  linkDiscordAccount: LinkDiscordAccountPayload;
  linkRobloxAccount: LinkRobloxAccountPayload;
  makeAppDisabledByDefault: SuccessPayload;
  makeAppEnabledByDefault: SuccessPayload;
  /** mark all notifications as read */
  markAllNotificationsAsRead: SuccessPayload;
  /** mark message as read */
  markMessageAsRead: SuccessPayload;
  notificationDelete: SuccessPayload;
  notificationSaveDeviceToken: SuccessPayload;
  notificationUpdateSettings: SuccessPayload;
  notifyAllContacts?: Maybe<SuccessPayload>;
  /** notify your contact by contactID */
  notifyContact: SuccessPayload;
  notifyMention: SuccessPayload;
  /** pin comment */
  pinComment: SuccessPayload;
  /** pin post */
  pinPost: SuccessPayload;
  /** puts circle into group by linking them */
  putCircleIntoGroup: Circle;
  /** react to comment */
  reactToComment: SuccessPayload;
  /** react on message, reaction is the emoji like :heart:, :joy:, :like: */
  reactToMessage: ChatMessageReaction;
  /** react to post */
  reactToPost: SuccessPayload;
  /** refresh all tokens for the user */
  refreshTokens: UserAuthPayload;
  /** Reject an offer */
  rejectOffer: SuccessPayload;
  /** removes blacklisted words from circle */
  removeCustomBLWords?: Maybe<SuccessPayload>;
  removePermissionFromApp: SuccessPayload;
  removeSubscriptionFromApp: SuccessPayload;
  removeTagFromApp: SuccessPayload;
  /** remove user from chat */
  removeUserFromChat: SuccessPayload;
  /** remove user from slice */
  removeUsersFromSlice: Slice;
  /** reorder pinned comments */
  reorderPinnedComments: SuccessPayload;
  /** reorder pinned posts */
  reorderPinnedPosts: SuccessPayload;
  /** reports post|comment|user */
  report?: Maybe<SuccessPayload>;
  /** reports given post */
  reportPost: SuccessPayload;
  resolveCircleReport: SuccessPayload;
  /** resolve report */
  resolveReport?: Maybe<SuccessPayload>;
  revokeAppPermissionsInCircle: SuccessPayload;
  revokeAppSubscriptionsInCircle: SuccessPayload;
  revokeAppToken: SuccessPayload;
  /** revoke discord webhook */
  revokeDiscordWebhook?: Maybe<SuccessPayload>;
  saveApp: SuccessPayload;
  /** saves meta words to circle */
  saveMetaWords?: Maybe<SuccessPayload>;
  /** save post screen time, duration in milliseconds */
  savePostScreenTime: SuccessPayload;
  /** save a post */
  savePostStatus: SuccessPayload;
  saveSearchResult: SuccessPayload;
  /** sends new chat message */
  sendChatMessage: ChatMessage;
  /** Create new offer */
  sendOffer: Offer;
  sendPushToCircle: SuccessPayload;
  setBanStatus: ModeratorActionId;
  setCircleModeratorAdmin: SuccessPayload;
  /** sets circle rules type and text */
  setCircleRules: Circle;
  /**
   * react to comment
   * @deprecated Use reactToComment/unreactToComment instead
   */
  setReactComment: SuccessPayload;
  /**
   * react to post
   * @deprecated Use reactToPost/unreactToPost instead
   */
  setReactPost: SuccessPayload;
  /** sets a role to given users in circle */
  setUsersRoleInCircle: SuccessPayload;
  /** share post */
  sharePost: SuccessPayload;
  signInModeration: AdminAuthInfo;
  /** signs in user using discord authorization grant code */
  signInWithDiscord: UserAuthPayload;
  /** signs in user using firebase credentials */
  signInWithFirebase: UserAuthPayload;
  /** signs in user using mobile phone number */
  signInWithPhoneNumber: SignInWithPhoneNumberPayload;
  /** signs in user using username */
  signInWithUsername: SignInWithUsernamePayload;
  /** sign up guest */
  signUpGuest: SignUpGuestPayload;
  /** signs up new user using firebase credentials */
  signUpWithFirebase: UserAuthPayload;
  /** Transfer seeds to another user in the same circle */
  transferSeeds: SuccessPayload;
  /** unbans user in circle */
  unbanUserInCircle: CircleMember;
  unfeatureApp: SuccessPayload;
  /** unfeature circle */
  unfeatureCircle: SuccessPayload;
  unlinkDiscordAccount: SuccessPayload;
  unlinkRobloxAccount: SuccessPayload;
  /** unpin comment */
  unpinComment: SuccessPayload;
  /** unpin post */
  unpinPost: SuccessPayload;
  /** remove reaction from comment */
  unreactToComment: SuccessPayload;
  /** delete reaction on message */
  unreactToMessage: ChatMessageReaction;
  /** remove reaction from post */
  unreactToPost: SuccessPayload;
  unsaveApp: SuccessPayload;
  unsaveSearchResult: SuccessPayload;
  unupvoteApp: SuccessPayload;
  unupvoteAppReview: SuccessPayload;
  updateApp: SuccessPayload;
  /** update chat settings */
  updateChatSettings: SuccessPayload;
  /** update circle */
  updateCircle: Circle;
  /** updates custom role in circle */
  updateCircleCustomRole: CircleCustomRoleResponse;
  /** updates collection's data (like name). to add or remove items from collection use separate mutation */
  updateCollection: SuccessPayload;
  /** Update comment */
  updateComment: Comment;
  updateEndpointPermissions?: Maybe<SuccessPayload>;
  updateLanguages?: Maybe<SuccessPayload>;
  updateModeratorPassword?: Maybe<SuccessPayload>;
  updateModeratorPermission?: Maybe<SuccessPayload>;
  /** Update existing offer */
  updateOffer: Offer;
  /** update privacy settings for a user */
  updatePrivacySettings: SuccessPayload;
  /** updates current user's profile info */
  updateProfileImage: Scalars['ImageUrl'];
  /** update profile info for a user */
  updateProfileInfo: SuccessPayload;
  updateReportStatus: SuccessPayload;
  /** updates settings for a user */
  updateSettings: SuccessPayload;
  /** update slice */
  updateSlice: Slice;
  updateTag: SuccessPayload;
  /** update user's preferred languages */
  updateUserPreferredLanguages: SuccessPayload;
  uploadAppImage: Scalars['ImageUrl'];
  /** upload attachment to chat */
  uploadAttachment: Attachment;
  /** upsert user's notifications settings */
  upsertNotificationSettings: SuccessPayload;
  upvoteApp: SuccessPayload;
  upvoteAppReview: SuccessPayload;
  /** glitter bomb a user */
  userGlitterBomb: SuccessPayload;
  verifyUser: SuccessPayload;
  /** view circle */
  viewCircle: SuccessPayload;
  /** view post by user */
  viewPost: SuccessPayload;
  /** vote for director */
  voteForDirector: SuccessPayload;
  /** votes for a given answer(variant) in the poll */
  voteInPoll: SuccessPayload;
};


export type MutationUnverifyUserArgs = {
  user_id: Scalars['ID'];
};


export type MutationUpdateCountersInBackgroundArgs = {
  updatePerIteration?: InputMaybe<Scalars['Int']>;
};


export type MutationAcceptArgs = {
  acceptRequestInput: AcceptInvitationRequest;
};


export type MutationAcceptInvitationArgs = {
  acceptInvitationInput?: InputMaybe<AcceptInvitationInput>;
};


export type MutationAcceptOfferArgs = {
  offerId: Scalars['ID'];
  userId: Scalars['ID'];
};


export type MutationAddCircleToTrackArgs = {
  circleId: Scalars['ID'];
};


export type MutationAddCustomBlWordsArgs = {
  addCustomBLWordsInput?: InputMaybe<AddCustomBlWordsInput>;
};


export type MutationAddCustomRoleToUserInCircleArgs = {
  addCustomRoleToUserInCircleInput?: InputMaybe<AddCustomRoleToUserInCircleInput>;
};


export type MutationAddLanguageArgs = {
  language: LanguageInput;
};


export type MutationAddModeratorArgs = {
  addModeratorInput: AddModeratorInput;
};


export type MutationAddPermissionToAppArgs = {
  appID: Scalars['ID'];
  permissionID: Scalars['ID'];
};


export type MutationAddPermissionToModeratorArgs = {
  addPermissionToModeratorInput: AddPermissionToModeratorInput;
};


export type MutationAddPostToCollectionArgs = {
  collectionId: Scalars['ID'];
  postId: Scalars['ID'];
};


export type MutationAddReasonToModeratorActionArgs = {
  id: Scalars['ID'];
  reason: Scalars['String'];
};


export type MutationAddSubscriptionToAppArgs = {
  appID: Scalars['ID'];
  subscriptionID: Scalars['ID'];
};


export type MutationAddTagToAppArgs = {
  appID: Scalars['ID'];
  tagID: Scalars['ID'];
};


export type MutationAddTagToCircleArgs = {
  circleID: Scalars['ID'];
  tagID: Scalars['ID'];
};


export type MutationAddTagToGenderArgs = {
  gender: Scalars['String'];
  tagID: Scalars['ID'];
};


export type MutationAddUserContactsArgs = {
  userContactsInput?: InputMaybe<Array<InputMaybe<UserContactsInput>>>;
};


export type MutationAddUserToChatArgs = {
  chatId: Scalars['ID'];
  userId: Scalars['ID'];
};


export type MutationAddUsersToSliceArgs = {
  addUserToSliceInput: AddUsersToSliceRequest;
};


export type MutationAppealArgs = {
  appealInput?: InputMaybe<AppealInput>;
};


export type MutationBanUserInCircleArgs = {
  banUserInput: BanUserInput;
};


export type MutationBlacklistCreateGlobalWordsArgs = {
  words?: InputMaybe<Array<Scalars['String']>>;
};


export type MutationBlacklistCreateUrlsArgs = {
  urls?: InputMaybe<Array<Scalars['String']>>;
};


export type MutationBlacklistDeleteGlobalWordsArgs = {
  words?: InputMaybe<Array<Scalars['String']>>;
};


export type MutationBlacklistDeleteUrlsArgs = {
  urls?: InputMaybe<Array<Scalars['String']>>;
};


export type MutationBlacklistSearchForUrlsArgs = {
  cursor?: InputMaybe<CursorInput>;
  filter: Scalars['String'];
};


export type MutationBlacklistUpdateGlobalWordArgs = {
  newWord: Scalars['String'];
  word: Scalars['String'];
};


export type MutationBlacklistUpdateUrlArgs = {
  newUrl: Scalars['String'];
  url: Scalars['String'];
};


export type MutationCancelOfferArgs = {
  offerId: Scalars['ID'];
  userId: Scalars['ID'];
};


export type MutationCancelVoteArgs = {
  cancelVoteInput: CancelVoteInput;
};


export type MutationChangeBlockStatusArgs = {
  shouldBlock: Scalars['Boolean'];
  userId: Scalars['ID'];
};


export type MutationChangeFollowStatusArgs = {
  shouldFollow: Scalars['Boolean'];
  userId: Scalars['ID'];
};


export type MutationCheckVerificationCodeArgs = {
  code: Scalars['String'];
  debugOption?: InputMaybe<DebugOptionInput>;
  sessionInfo: Scalars['String'];
};


export type MutationConfigureDiscordWebhookArgs = {
  configureDiscordWebhookInput?: InputMaybe<ConfigureDiscordWebhookInput>;
};


export type MutationCreateActionReasonArgs = {
  reason: Scalars['String'];
};


export type MutationCreateAppArgs = {
  in: CreateAppInput;
};


export type MutationCreateAppReviewArgs = {
  in: CreateAppReviewInput;
};


export type MutationCreateCircleArgs = {
  createCircleInput: CreatCirclesInput;
};


export type MutationCreateCircleCustomRoleArgs = {
  createCircleCustomRoleInput?: InputMaybe<CreateCircleCustomRoleInput>;
};


export type MutationCreateCollectionArgs = {
  collection: CreateCollectionInput;
};


export type MutationCreateCommentArgs = {
  data?: InputMaybe<CreateCommentInput>;
};


export type MutationCreateFirebaseTokenArgs = {
  userId: Scalars['ID'];
};


export type MutationCreateGroupChatArgs = {
  chatConfiguration?: InputMaybe<GroupChatInput>;
};


export type MutationCreatePermissionArgs = {
  custom_index_option: Scalars['Boolean'];
  permission_title: Scalars['String'];
};


export type MutationCreatePostArgs = {
  data: CreatePostInput;
};


export type MutationCreateReportArgs = {
  info: CreateReportInput;
};


export type MutationCreateReportReasonArgs = {
  createReportReasonInput: CreateReportReasonInput;
};


export type MutationCreateSingleChatArgs = {
  chatConfiguration?: InputMaybe<SingleChatInput>;
};


export type MutationCreateSliceArgs = {
  createSliceInput: CreatSliceInput;
};


export type MutationCreateTagArgs = {
  genders?: InputMaybe<Array<Scalars['String']>>;
  name: Scalars['String'];
};


export type MutationDeclineArgs = {
  declineRequestInput: DeclineInvitationRequest;
};


export type MutationDeclineInvitationArgs = {
  declineInvitationInput?: InputMaybe<DeclineInvitationInput>;
};


export type MutationDeleteAccountRequestArgs = {
  reason: Scalars['String'];
};


export type MutationDeleteActionReasonArgs = {
  id: Scalars['ID'];
};


export type MutationDeleteAppArgs = {
  appID: Scalars['ID'];
};


export type MutationDeleteAppReviewArgs = {
  reviewID: Scalars['ID'];
};


export type MutationDeleteAppTokenArgs = {
  tokenID: Scalars['ID'];
};


export type MutationDeleteChatArgs = {
  chatId: Scalars['ID'];
};


export type MutationDeleteCircleArgs = {
  deleteCircleInput: DeleteCirclesInput;
};


export type MutationDeleteCircleCustomRoleArgs = {
  deleteCircleCustomRoleInput?: InputMaybe<DeleteCircleCustomRoleInput>;
};


export type MutationDeleteCollectionArgs = {
  collectionId: Scalars['ID'];
};


export type MutationDeleteCommentArgs = {
  data: DeleteCommentInput;
};


export type MutationDeleteCommentAdminArgs = {
  circleID?: InputMaybe<Scalars['ID']>;
  comment_id: Scalars['ID'];
};


export type MutationDeleteCustomRoleFromUserInCircleArgs = {
  deleteCustomRoleFromUserInCircleInput?: InputMaybe<DeleteCustomRoleFromUserInCircleInput>;
};


export type MutationDeleteMessageArgs = {
  messageId: Scalars['ID'];
};


export type MutationDeleteMessageAdminArgs = {
  message_id: Scalars['ID'];
};


export type MutationDeleteModeratorArgs = {
  moderator_id: Scalars['ID'];
};


export type MutationDeleteModeratorPermissionArgs = {
  id: Scalars['ID'];
};


export type MutationDeletePermissionArgs = {
  permission_id: Scalars['ID'];
};


export type MutationDeletePostArgs = {
  data: DeletePostInput;
};


export type MutationDeletePostAdminArgs = {
  post_id: Scalars['ID'];
};


export type MutationDeletePostsArgs = {
  data: DeletePostsInput;
};


export type MutationDeletePostsFromCollectionArgs = {
  collectionId: Scalars['ID'];
  postIds?: InputMaybe<Array<Scalars['ID']>>;
  unsave: Scalars['Boolean'];
};


export type MutationDeleteSliceArgs = {
  deleteSliceInput: DeleteSliceInput;
};


export type MutationDeleteTagArgs = {
  id: Scalars['ID'];
};


export type MutationDeleteTagFromCircleArgs = {
  circleID: Scalars['ID'];
  tagID: Scalars['ID'];
};


export type MutationDeleteTagFromGenderArgs = {
  gender: Scalars['String'];
  tagID: Scalars['ID'];
};


export type MutationDeleteTestUserArgs = {
  username: Scalars['String'];
};


export type MutationDeleteTrackedCircleArgs = {
  circleId: Scalars['ID'];
};


export type MutationDeleteUserMessagesInCirclesArgs = {
  deleteUserMessagesInCirclesInput: DeleteUserMessagesInCirclesInput;
};


export type MutationDisableAppArgs = {
  input: DisableAppInput;
};


export type MutationDocumentDeleteArgs = {
  key: Scalars['String'];
};


export type MutationDocumentPutArgs = {
  key: Scalars['String'];
  value: Scalars['String'];
};


export type MutationElectionVoteArgs = {
  electionVoteInput: ElectionVoteInput;
};


export type MutationEnableAppArgs = {
  input: EnableAppInput;
};


export type MutationFeatureAppArgs = {
  appID: Scalars['ID'];
};


export type MutationFeatureCircleArgs = {
  circleId: Scalars['ID'];
};


export type MutationGenerateAppTokenArgs = {
  appID: Scalars['ID'];
  name: Scalars['String'];
};


export type MutationGenerateUserScopedAppTokenArgs = {
  appID: Scalars['ID'];
};


export type MutationGetMessageAdminArgs = {
  message_id: Scalars['ID'];
};


export type MutationGetModeratorPermissionsArgs = {
  moderator_id: Scalars['ID'];
};


export type MutationGetUserStatsArgs = {
  userID: Scalars['String'];
};


export type MutationGrantAppPermissionsInCircleArgs = {
  appID: Scalars['ID'];
  circleID: Scalars['ID'];
  permissionIDs?: InputMaybe<Array<Scalars['ID']>>;
};


export type MutationGrantAppSubscriptionsInCircleArgs = {
  appID: Scalars['ID'];
  circleID: Scalars['ID'];
  subscriptionIDs?: InputMaybe<Array<Scalars['ID']>>;
};


export type MutationInviteUserToChatArgs = {
  invite: Scalars['ID'];
};


export type MutationInviteUsersToCircleArgs = {
  inviteToCircleInput?: InputMaybe<InviteToCircleInput>;
};


export type MutationIsTokenBlacklistedArgs = {
  token_id: Scalars['ID'];
};


export type MutationIssueAppTokenArgs = {
  tokenID: Scalars['ID'];
};


export type MutationJoinChatArgs = {
  chatId: Scalars['ID'];
};


export type MutationJoinCirclesArgs = {
  joinCircleInput: JoinCirclesInput;
};


export type MutationJoinSlicesArgs = {
  joinSlicesInput: JoinSlicesRequest;
};


export type MutationLeaveChatArgs = {
  chatId: Scalars['ID'];
};


export type MutationLeaveCirclesArgs = {
  joinCircleInput: JoinCirclesInput;
};


export type MutationLeaveSliceArgs = {
  leaveSliceInput: LeaveSliceRequest;
};


export type MutationLinkDiscordAccountArgs = {
  request: LinkDiscordAccountInput;
};


export type MutationLinkRobloxAccountArgs = {
  request: LinkRobloxAccountInput;
};


export type MutationMakeAppDisabledByDefaultArgs = {
  appID: Scalars['ID'];
};


export type MutationMakeAppEnabledByDefaultArgs = {
  appID: Scalars['ID'];
};


export type MutationMarkMessageAsReadArgs = {
  messageId: Scalars['ID'];
};


export type MutationNotificationDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationNotificationSaveDeviceTokenArgs = {
  platform: Platform;
  token: Scalars['String'];
};


export type MutationNotificationUpdateSettingsArgs = {
  settings: NotificationsSettingsInput;
};


export type MutationNotifyAllContactsArgs = {
  entityID?: InputMaybe<Scalars['ID']>;
  notifyType: NotifyType;
};


export type MutationNotifyContactArgs = {
  contactID: Scalars['ID'];
  entityID?: InputMaybe<Scalars['ID']>;
  notifyType: NotifyType;
};


export type MutationNotifyMentionArgs = {
  input: NotifyMentionInput;
};


export type MutationPinCommentArgs = {
  commentId: Scalars['ID'];
  order: Scalars['Int'];
  until?: InputMaybe<Scalars['DateTime']>;
};


export type MutationPinPostArgs = {
  order: Scalars['Int'];
  postId: Scalars['ID'];
  until?: InputMaybe<Scalars['DateTime']>;
};


export type MutationPutCircleIntoGroupArgs = {
  putCircleInput: PutCircleIntoGroupInput;
};


export type MutationReactToCommentArgs = {
  commentId: Scalars['ID'];
  reaction: Scalars['String'];
};


export type MutationReactToMessageArgs = {
  messageId: Scalars['ID'];
  reaction: Scalars['String'];
};


export type MutationReactToPostArgs = {
  postId: Scalars['String'];
  reaction: Scalars['String'];
};


export type MutationRefreshTokensArgs = {
  accessToken: Scalars['String'];
  debugOption?: InputMaybe<DebugOptionInput>;
  refreshToken: Scalars['String'];
};


export type MutationRejectOfferArgs = {
  offerId: Scalars['ID'];
  userId: Scalars['ID'];
};


export type MutationRemoveCustomBlWordsArgs = {
  removeCustomBLWordInput?: InputMaybe<RemoveCustomBlWordInput>;
};


export type MutationRemovePermissionFromAppArgs = {
  appID: Scalars['ID'];
  permissionID: Scalars['ID'];
};


export type MutationRemoveSubscriptionFromAppArgs = {
  appID: Scalars['ID'];
  subscriptionID: Scalars['ID'];
};


export type MutationRemoveTagFromAppArgs = {
  appID: Scalars['ID'];
  tagID: Scalars['ID'];
};


export type MutationRemoveUserFromChatArgs = {
  chatId: Scalars['ID'];
  userId: Scalars['ID'];
};


export type MutationRemoveUsersFromSliceArgs = {
  removeUserFromSliceInput: RemoveUsersFromSliceRequest;
};


export type MutationReorderPinnedCommentsArgs = {
  orders?: InputMaybe<Array<PinOrder>>;
};


export type MutationReorderPinnedPostsArgs = {
  orders?: InputMaybe<Array<PinOrder>>;
};


export type MutationReportArgs = {
  reportInput?: InputMaybe<ReportInput>;
};


export type MutationReportPostArgs = {
  postReport?: InputMaybe<PostReportInput>;
};


export type MutationResolveCircleReportArgs = {
  resolveCircleReportInput: ResolveCircleReportInput;
};


export type MutationResolveReportArgs = {
  resolveReportInput?: InputMaybe<ResolveReportInput>;
};


export type MutationRevokeAppPermissionsInCircleArgs = {
  appID: Scalars['ID'];
  circleID: Scalars['ID'];
  permissionIDs?: InputMaybe<Array<Scalars['ID']>>;
};


export type MutationRevokeAppSubscriptionsInCircleArgs = {
  appID: Scalars['ID'];
  circleID: Scalars['ID'];
  subscriptionIDs?: InputMaybe<Array<Scalars['ID']>>;
};


export type MutationRevokeAppTokenArgs = {
  tokenID: Scalars['ID'];
};


export type MutationRevokeDiscordWebhookArgs = {
  revokeDiscordWebhookInput?: InputMaybe<RevokeDiscordWebhookInput>;
};


export type MutationSaveAppArgs = {
  appID: Scalars['ID'];
};


export type MutationSaveMetaWordsArgs = {
  saveMetaWordsInput?: InputMaybe<SaveMetaWordsInput>;
};


export type MutationSavePostScreenTimeArgs = {
  duration: Scalars['Int'];
  postID: Scalars['ID'];
};


export type MutationSavePostStatusArgs = {
  postId: Scalars['ID'];
  saveStatus: Scalars['Boolean'];
};


export type MutationSaveSearchResultArgs = {
  entityID: Scalars['ID'];
  kind: SearchEntityKind;
};


export type MutationSendChatMessageArgs = {
  chatId: Scalars['ID'];
  message: ChatMessageInput;
};


export type MutationSendOfferArgs = {
  offerInput: SendOfferInput;
};


export type MutationSendPushToCircleArgs = {
  input?: InputMaybe<SendPushToCircleInput>;
};


export type MutationSetBanStatusArgs = {
  ban: Scalars['Boolean'];
  ban_context?: InputMaybe<BanUserContext>;
  user_id: Scalars['ID'];
};


export type MutationSetCircleModeratorAdminArgs = {
  input: RoleAssignmentsInput;
};


export type MutationSetCircleRulesArgs = {
  setCircleRulesInput?: InputMaybe<SetCircleRulesInput>;
};


export type MutationSetReactCommentArgs = {
  data: ReactionInput;
};


export type MutationSetReactPostArgs = {
  data: ReactionInput;
};


export type MutationSetUsersRoleInCircleArgs = {
  userRoleInCircleInput?: InputMaybe<RoleAssignmentsInput>;
};


export type MutationSharePostArgs = {
  postId: Scalars['ID'];
};


export type MutationSignInModerationArgs = {
  credentials: SignInModerationInput;
};


export type MutationSignInWithDiscordArgs = {
  credentials: DiscordTokenInput;
  debugOption?: InputMaybe<DebugOptionInput>;
};


export type MutationSignInWithFirebaseArgs = {
  credentials: FirebaseAuthInput;
  debugOption?: InputMaybe<DebugOptionInput>;
};


export type MutationSignInWithPhoneNumberArgs = {
  phoneNumber: Scalars['String'];
  recaptchaToken: Scalars['String'];
  useCaptchaV3?: InputMaybe<Scalars['Boolean']>;
};


export type MutationSignInWithUsernameArgs = {
  recaptchaToken: Scalars['String'];
  username: Scalars['String'];
};


export type MutationSignUpWithFirebaseArgs = {
  credentials: FirebaseAuthInput;
  debugOption?: InputMaybe<DebugOptionInput>;
  userInfo: UserSignUpInput;
};


export type MutationTransferSeedsArgs = {
  transferInput: TransferInput;
};


export type MutationUnbanUserInCircleArgs = {
  banUserInput: BanUserInput;
};


export type MutationUnfeatureAppArgs = {
  appID: Scalars['ID'];
};


export type MutationUnfeatureCircleArgs = {
  circleId: Scalars['ID'];
};


export type MutationUnpinCommentArgs = {
  commentId: Scalars['ID'];
};


export type MutationUnpinPostArgs = {
  postId: Scalars['ID'];
};


export type MutationUnreactToCommentArgs = {
  commentId: Scalars['ID'];
};


export type MutationUnreactToMessageArgs = {
  messageId: Scalars['ID'];
  reaction: Scalars['String'];
};


export type MutationUnreactToPostArgs = {
  postId: Scalars['String'];
};


export type MutationUnsaveAppArgs = {
  appID: Scalars['ID'];
};


export type MutationUnsaveSearchResultArgs = {
  entityID: Scalars['ID'];
  kind: SearchEntityKind;
};


export type MutationUnupvoteAppArgs = {
  appID: Scalars['ID'];
};


export type MutationUnupvoteAppReviewArgs = {
  reviewID: Scalars['ID'];
};


export type MutationUpdateAppArgs = {
  in: UpdateAppInput;
};


export type MutationUpdateChatSettingsArgs = {
  chatId: Scalars['ID'];
  settings: ChatSettingsInput;
};


export type MutationUpdateCircleArgs = {
  updateCircleInput: UpdateCirclesInput;
};


export type MutationUpdateCircleCustomRoleArgs = {
  updateCircleCustomRoleInput?: InputMaybe<UpdateCircleCustomRoleInput>;
};


export type MutationUpdateCollectionArgs = {
  collectionId: Scalars['ID'];
  data: UpdateCollectionInput;
};


export type MutationUpdateCommentArgs = {
  data: UpdateCommentInput;
};


export type MutationUpdateEndpointPermissionsArgs = {
  updateEndpointPermissionsInput: UpdateEndpointPermissionsInput;
};


export type MutationUpdateLanguagesArgs = {
  action: LanguageAction;
  tags?: InputMaybe<Array<InputMaybe<LanguageTag>>>;
};


export type MutationUpdateModeratorPasswordArgs = {
  password: Scalars['String'];
};


export type MutationUpdateModeratorPermissionArgs = {
  updateModeratorPermissionInput: UpdateModeratorPermissionInput;
};


export type MutationUpdateOfferArgs = {
  offerInput: SendOfferInput;
};


export type MutationUpdatePrivacySettingsArgs = {
  OnlyDMFromFollowed: Scalars['Boolean'];
};


export type MutationUpdateProfileImageArgs = {
  image: UserProfileImageInput;
};


export type MutationUpdateProfileInfoArgs = {
  profile: UpdateProfileInput;
};


export type MutationUpdateReportStatusArgs = {
  report_id: Scalars['ID'];
  status: ReportStatus;
};


export type MutationUpdateSettingsArgs = {
  userSettings: UserSettingsInput;
};


export type MutationUpdateSliceArgs = {
  updateSliceInput: UpdateSliceInput;
};


export type MutationUpdateTagArgs = {
  id: Scalars['ID'];
  name?: InputMaybe<Scalars['String']>;
};


export type MutationUpdateUserPreferredLanguagesArgs = {
  languagesCodes?: InputMaybe<Array<Scalars['String']>>;
};


export type MutationUploadAppImageArgs = {
  input: AppImageInput;
};


export type MutationUploadAttachmentArgs = {
  file: AttachmentInput;
};


export type MutationUpsertNotificationSettingsArgs = {
  settings: NotificationsSettingsInput;
};


export type MutationUpvoteAppArgs = {
  appID: Scalars['ID'];
};


export type MutationUpvoteAppReviewArgs = {
  reviewID: Scalars['ID'];
};


export type MutationUserGlitterBombArgs = {
  targetUserID: Scalars['ID'];
};


export type MutationVerifyUserArgs = {
  user_id: Scalars['ID'];
};


export type MutationViewCircleArgs = {
  circleId: Scalars['ID'];
};


export type MutationViewPostArgs = {
  id: Scalars['ID'];
};


export type MutationVoteForDirectorArgs = {
  circleId: Scalars['ID'];
  userId: Scalars['ID'];
};


export type MutationVoteInPollArgs = {
  data: VoteInPollInput;
};

export type NewModeratorPermission = {
  custom_indexes?: InputMaybe<Array<Scalars['String']>>;
  full_access: Scalars['Boolean'];
  permission_id: Scalars['String'];
};

export type Node = {
  id: Scalars['ID'];
};

export type Notification = Node & {
  __typename?: 'Notification';
  action: Scalars['String'];
  createdAt: Scalars['DateTime'];
  fromInfo: PopulatedData;
  id: Scalars['ID'];
  imageURL: Scalars['String'];
  readAt: Scalars['DateTime'];
  receiverInfo: PopulatedData;
  sourceInfo: PopulatedData;
  subSourceInfo: PopulatedData;
};

export type NotificationUnreadCountResponse = {
  __typename?: 'NotificationUnreadCountResponse';
  count: Scalars['Int'];
};

export type NotificationsConnection = {
  __typename?: 'NotificationsConnection';
  edges?: Maybe<Array<NotificationsEdge>>;
  pageInfo: PageInfo;
};

export type NotificationsEdge = {
  __typename?: 'NotificationsEdge';
  cursorId: Scalars['ID'];
  node: Notification;
};

/** notification settings for given user */
export type NotificationsSettings = {
  __typename?: 'NotificationsSettings';
  circleChats: Scalars['Boolean'];
  circleInvites: Scalars['Boolean'];
  circleJoins: Scalars['Boolean'];
  commentLikes: Scalars['Boolean'];
  comments: Scalars['Boolean'];
  directMessages: Scalars['Boolean'];
  glitterBombs: Scalars['Boolean'];
  groupChats: Scalars['Boolean'];
  likes: Scalars['Boolean'];
  mentions: Scalars['Boolean'];
  newFollowers: Scalars['Boolean'];
  notificationId: Scalars['ID'];
  postSaves: Scalars['Boolean'];
  postShares: Scalars['Boolean'];
  postsFromAccountsYouFollow: Scalars['Boolean'];
  seeds: Scalars['Boolean'];
};

export type NotificationsSettingsInput = {
  circleChats: Scalars['Boolean'];
  circleInvites?: InputMaybe<Scalars['Boolean']>;
  circleJoins?: InputMaybe<Scalars['Boolean']>;
  commentLikes?: InputMaybe<Scalars['Boolean']>;
  comments: Scalars['Boolean'];
  directMessages: Scalars['Boolean'];
  glitterBombs?: InputMaybe<Scalars['Boolean']>;
  groupChats: Scalars['Boolean'];
  likes: Scalars['Boolean'];
  mentions: Scalars['Boolean'];
  newFollowers: Scalars['Boolean'];
  notificationId: Scalars['ID'];
  postSaves?: InputMaybe<Scalars['Boolean']>;
  postShares?: InputMaybe<Scalars['Boolean']>;
  postsFromAccountsYouFollow: Scalars['Boolean'];
  seeds?: InputMaybe<Scalars['Boolean']>;
};

export type NotifyMentionInput = {
  entityId: Scalars['ID'];
  id: Scalars['ID'];
  notifyType: NotifyType;
  userType: UserType;
};

export type NotifyMeta = {
  /** optional chatID if the entityType is CHAT */
  chatID?: InputMaybe<Scalars['ID']>;
  /** optional commentID if the entityType is COMMENT */
  commentID?: InputMaybe<Scalars['ID']>;
  entityType: EntityType;
  /** optional postID if the entityType is POST */
  postID?: InputMaybe<Scalars['ID']>;
};

export enum NotifyType {
  AddFriend = 'ADD_FRIEND',
  InviteChat = 'INVITE_CHAT',
  InviteCircle = 'INVITE_CIRCLE',
  InviteFriend = 'INVITE_FRIEND',
  SharePost = 'SHARE_POST',
  TagComment = 'TAG_COMMENT',
  TagPoll = 'TAG_POLL',
  TagPost = 'TAG_POST',
  Unknown = 'UNKNOWN'
}

/** Offer type containing info about exchange */
export type Offer = {
  __typename?: 'Offer';
  id: Scalars['ID'];
  melonsAmount: Scalars['Int'];
  recipientId: Scalars['ID'];
  seed: ExchangeSeed;
  status: OfferStatus;
};

/** Offer status type */
export enum OfferStatus {
  Accepted = 'ACCEPTED',
  Active = 'ACTIVE',
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
  Processing = 'PROCESSING',
  Rejected = 'REJECTED'
}

export enum OfferType {
  Buy = 'BUY',
  Sell = 'SELL'
}

export type OnBoardingCirclesConnectionResponse = {
  __typename?: 'OnBoardingCirclesConnectionResponse';
  edges?: Maybe<Array<OnBoardingCirclesEdge>>;
};

export type OnBoardingCirclesEdge = {
  __typename?: 'OnBoardingCirclesEdge';
  circles?: Maybe<Array<Maybe<Circle>>>;
  name: Scalars['String'];
};

export enum Option {
  Chatting = 'Chatting',
  Comments = 'Comments',
  DisplayName = 'DisplayName',
  Emoji = 'Emoji',
  LinkPosts = 'LinkPosts',
  None = 'None',
  PhotoPosts = 'PhotoPosts',
  PollPosts = 'PollPosts',
  ThoughtPosts = 'ThoughtPosts',
  VideoPosts = 'VideoPosts',
  Visibility = 'Visibility'
}

export type PLanguage = {
  __typename?: 'PLanguage';
  /** base code of the language */
  base: Scalars['String'];
  /** if the language is enabled */
  enabled: Scalars['Boolean'];
  /** emoji flag of the country representing the language */
  flag: Scalars['String'];
  /** iso3 code of the language */
  iso3: Scalars['String'];
  /** name display English name */
  name: Scalars['String'];
  /** native name of the language */
  nativeName: Scalars['String'];
  /** tag BCP 47 language tag */
  tag: Scalars['String'];
};

export type PageInfo = {
  __typename?: 'PageInfo';
  firstId?: Maybe<Scalars['ID']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  lastId?: Maybe<Scalars['ID']>;
  length: Scalars['Int'];
};

export enum Period {
  LastDay = 'LAST_DAY',
  LastMonth = 'LAST_MONTH',
  LastWeek = 'LAST_WEEK',
  Unknown = 'UNKNOWN'
}

export type Permission = {
  __typename?: 'Permission';
  custom_index_option: Scalars['Boolean'];
  id: Scalars['String'];
  permission_title: Scalars['String'];
};

export type Pin = {
  __typename?: 'Pin';
  order: Scalars['Int'];
  until?: Maybe<Scalars['DateTime']>;
};

export type PinOrder = {
  itemId: Scalars['ID'];
  order: Scalars['Int'];
};

export enum Platform {
  Android = 'ANDROID',
  Huawei = 'HUAWEI',
  Ios = 'IOS'
}

/**
 * answer in poll.
 *
 * V1 supports only image answers
 */
export type PollAnswer = {
  __typename?: 'PollAnswer';
  id: Scalars['ID'];
  imageUrl?: Maybe<Scalars['ImageUrl']>;
  votesCount: Scalars['Int'];
};

/**
 * input for the poll answer
 * V1 supports only image
 */
export type PollAnswerInput = {
  answerType: PollAnswerType;
  imageContent?: InputMaybe<Scalars['Upload']>;
};

/** type of the answer in a poll post */
export enum PollAnswerType {
  /** v1 only supports images */
  Image = 'IMAGE'
}

export type PollPostContent = {
  __typename?: 'PollPostContent';
  answers: Array<PollAnswer>;
  question: Scalars['String'];
  /**
   * optional id of the answer given user voted. null if user didn't vote
   * @deprecated Use `Post.context.pollAnswerId` instead
   */
  votedAnswer?: Maybe<Scalars['ID']>;
  votesTotal: Scalars['Int'];
};

/** input for creating poll post */
export type PollPostContentInput = {
  /**
   * list of answers for a poll.
   * V1 supports only two answers, left and right
   * V1 supports only images
   */
  answers: Array<PollAnswerInput>;
  question: Scalars['String'];
};

export type PopulatedData = {
  __typename?: 'PopulatedData';
  avatar: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  relation: Scalars['Boolean'];
  type: Scalars['String'];
};

export type Post = Node & {
  __typename?: 'Post';
  /** user that created the post */
  author: PublicProfile;
  authorId: Scalars['ID'];
  /** circle in which the given post was posted */
  circle?: Maybe<Circle>;
  circleId: Scalars['ID'];
  commentsConnection: CommentsConnection;
  /** @deprecated Use `counters.comments` instead */
  commentsCount: Scalars['Int'];
  context: PostContext;
  counters: ContentStatsForContent;
  createdAt: Scalars['DateTime'];
  /** @deprecated Use `context.Reaction` instead */
  iReacted: Scalars['Boolean'];
  /** @deprecated Use `context.Saved` instead */
  iSaved: Scalars['Boolean'];
  id: Scalars['ID'];
  imageContent?: Maybe<ImagePostContent>;
  languageCode: Scalars['String'];
  /** @deprecated Use `counters.reactions` instead */
  likesCount: Scalars['Int'];
  linkContent?: Maybe<LinkPostContent>;
  mentions: Mentions;
  pin?: Maybe<Pin>;
  pollContent?: Maybe<PollPostContent>;
  postReactionsConnection: ReactionsConnection;
  /** @deprecated Use `createAt` instead. */
  postedAt: Scalars['DateTime'];
  /** @deprecated Use `counters.saves` instead */
  savesCount: Scalars['Int'];
  shareLink: Scalars['String'];
  /** @deprecated Use `counters.shares` instead */
  sharesCount: Scalars['Int'];
  shortId?: Maybe<Scalars['String']>;
  sound?: Maybe<Sound>;
  soundId: Scalars['String'];
  textContent?: Maybe<TextPostContent>;
  title: Scalars['String'];
  type: PostType;
  videoContent?: Maybe<VideoPostContent>;
  /** @deprecated Use `counters.impressions` instead */
  viewsCount: Scalars['Int'];
};


export type PostCommentsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type PostPostReactionsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};

export type PostContext = {
  __typename?: 'PostContext';
  pollAnswerId: Scalars['ID'];
  reaction: Scalars['String'];
  saved: Scalars['Boolean'];
};

/** input for reporting given post */
export type PostReportInput = {
  /** optional comment for the report. used only when reason is 'OTHER' */
  comment?: InputMaybe<Scalars['String']>;
  postId: Scalars['ID'];
  reason: PostReportReason;
};

export enum PostReportReason {
  Other = 'OTHER',
  Spam = 'SPAM'
}

export enum PostType {
  Image = 'IMAGE',
  Link = 'LINK',
  Poll = 'POLL',
  Text = 'TEXT',
  Video = 'VIDEO'
}

export type PostsConnection = {
  __typename?: 'PostsConnection';
  edges?: Maybe<Array<PostsEdge>>;
  pageInfo: PageInfo;
};

export type PostsEdge = {
  __typename?: 'PostsEdge';
  cursorId: Scalars['ID'];
  node: Post;
};

export enum PostsSortingType {
  LastUsed = 'LAST_USED',
  New = 'NEW',
  PopularityAllTime = 'POPULARITY_ALL_TIME',
  Rising = 'RISING',
  TrendingThisMonth = 'TRENDING_THIS_MONTH',
  TrendingThisWeek = 'TRENDING_THIS_WEEK'
}

/** privacy settings for given user */
export type PrivacySettings = {
  __typename?: 'PrivacySettings';
  directMessagesFromAccountsYouFollow: Scalars['Boolean'];
};

/** input for updating privacy settings for given user */
export type PrivacySettingsInput = {
  directMessagesFromAccountsYouFollow: Scalars['Boolean'];
};

/** User info with additional fields available only for the current user */
export type PrivateProfile = User & {
  __typename?: 'PrivateProfile';
  age: Scalars['Int'];
  bio?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  email?: Maybe<Scalars['String']>;
  followers: Scalars['Int'];
  followersConnection: UsersConnection;
  fullName: Scalars['String'];
  id: Scalars['ID'];
  isVerified: Scalars['Boolean'];
  languages?: Maybe<Array<Scalars['String']>>;
  likes: Scalars['Int'];
  /** total amount of melons owned by user */
  melonsAmount: Scalars['Int'];
  meta: ProfileMeta;
  phone?: Maybe<Scalars['String']>;
  profileImage: Scalars['ImageUrl'];
  savedPostsConnection: PostsConnection;
  /** list of user's seeds */
  seeds?: Maybe<SeedsConnection>;
  /** total amount of seeds owned by user */
  seedsAmount?: Maybe<Scalars['Int']>;
  shareLink: Scalars['String'];
  username: Scalars['String'];
  views: Scalars['Int'];
};


/** User info with additional fields available only for the current user */
export type PrivateProfileFollowersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


/** User info with additional fields available only for the current user */
export type PrivateProfileSavedPostsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


/** User info with additional fields available only for the current user */
export type PrivateProfileSeedsArgs = {
  cursor?: InputMaybe<CursorInput>;
};

export type ProfileMeta = {
  __typename?: 'ProfileMeta';
  pendingSteps?: Maybe<Array<ProfileSetupStep>>;
};

export enum ProfileSetupStep {
  Age = 'Age',
  Circles = 'Circles',
  Email = 'Email',
  Languages = 'Languages',
  Phone = 'Phone',
  ProfileImage = 'ProfileImage',
  Username = 'Username'
}

export type PublicCircleLandingPageResponse = {
  __typename?: 'PublicCircleLandingPageResponse';
  description: Scalars['String'];
  id: Scalars['ID'];
  image: Scalars['ImageUrl'];
  languageCode: Scalars['String'];
  membersCount: Scalars['Int'];
  metaWords: Array<Scalars['String']>;
  name: Scalars['String'];
  postsCount: Scalars['Int'];
  shareLink: Scalars['String'];
  viewsCount: Scalars['Int'];
};

export type PublicLandingPage = {
  __typename?: 'PublicLandingPage';
  bio: Scalars['String'];
  followers: Scalars['Int'];
  fullName: Scalars['String'];
  isVerified: Scalars['Boolean'];
  likes: Scalars['Int'];
  profileImage: Scalars['String'];
  shareLink: Scalars['String'];
  userId: Scalars['String'];
  username: Scalars['String'];
  views: Scalars['Int'];
};

/** Public profile that can be queried by everyone */
export type PublicProfile = User & {
  __typename?: 'PublicProfile';
  age: Scalars['Int'];
  bio?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  followers: Scalars['Int'];
  followersConnection: UsersConnection;
  followsMe: Scalars['Boolean'];
  fullName: Scalars['String'];
  id: Scalars['ID'];
  isBlocked: Scalars['Boolean'];
  isFollowing: Scalars['Boolean'];
  isVerified: Scalars['Boolean'];
  likes: Scalars['Int'];
  profileImage: Scalars['ImageUrl'];
  shareLink: Scalars['String'];
  username: Scalars['String'];
  views: Scalars['Int'];
};


/** Public profile that can be queried by everyone */
export type PublicProfileFollowersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};

/** input used in putCircleIntoGroup mutation */
export type PutCircleIntoGroupInput = {
  circleId: Scalars['ID'];
  groupId: Scalars['ID'];
};

export type Query = {
  __typename?: 'Query';
  /** Retrieve all the globally blacklisted words. */
  blacklistListGlobalWords: WordsList;
  blacklistListUrls: UrlsList;
  /** list of blocked users by the current user */
  blockedUsersConnection: UsersConnection;
  /** get custom black listed words by circle */
  blwConnection?: Maybe<BlwConnectionResponse>;
  /** returns given chat by id */
  chat?: Maybe<Chat>;
  /** get chat feed connection with pagination */
  chatFeedConnection: ChatExcerptsConnection;
  /** get chat members connection with pagination */
  chatMembersConnection: ChatMembersConnection;
  /** get chat messages connection with pagination */
  chatMessagesConnection: ChatMessagesConnection;
  /** get chat participants connection with pagination */
  chatParticipantsConnection: UsersConnection;
  /** get chat settings */
  chatSettings: ChatSettings;
  /** gets list of all chats for given user, paginated */
  chatsConnection: ChatsConnection;
  /** checks whether given username is already taken or not */
  checkUsername: UsernameAvailabilityPayload;
  /** Deprecated: use getCircleApps instead. */
  circleApps: CircleAppsResponse;
  circleCountData?: Maybe<Array<CountData>>;
  /** list of feeds for circle */
  circleFeed?: Maybe<Feed>;
  /** get list of circle join requests */
  circleJoinRequestsConnection: CircleJoinRequestsConnectionResponse;
  circleMetricsData?: Maybe<Array<CircleMetricsData>>;
  /**
   * list of circle posts
   * @deprecated Use `sortedCirclePostsConnection` instead
   */
  circlePostsConnection: PostsConnection;
  circleReportsAdminPanel: CircleReports;
  /** get circle reports related to particular circle */
  circleReportsConnection: CircleReports;
  /** Get seeds connection for circle */
  circleSeedsConnection: SeedsConnection;
  /** gets list of circles, paginated (bad practice with a lot params passing, but we have contract with UI already, unfortunately) */
  circlesConnection: CirclesResponse;
  /** get posts in a collection */
  collectionPostsConnection: PostsConnection;
  /** gets list of collections, paginated */
  collectionsConnection: CollectionsConnection;
  /** comment reaction */
  commentReactionsConnection: ReactionsConnection;
  /** get stats for circle */
  contentStatsForCircle: ContentStatsForCircle;
  /** get stats for content */
  contentStatsForContent: ContentStatsForContent;
  /** get stats for user */
  contentStatsForProfile: ContentStatsForProfile;
  /** get list default circle options */
  defaultCircleConfigOptions: DefaultCircleConfigOptionsResponse;
  /** Returns a value for a given key */
  documentGet: Scalars['String'];
  /** Returns all the keys in the database */
  documentGetAllKeys?: Maybe<Array<Scalars['String']>>;
  /**
   * get members with election + cursor + order by votes
   * @deprecated Use 'getCircleVotes' instead
   */
  electionParticipantsConnection: CircleParticipants;
  /** details of the specified feed */
  feed?: Maybe<Feed>;
  /** list of feed groups */
  feedGroups?: Maybe<Array<FeedGroup>>;
  /** list of feed posts */
  feedPostsConnection: PostsConnection;
  /** list of feeds available for current user */
  feedsConnection: FeedsConnection;
  /** list of followers of a given user */
  followersConnection: UsersConnection;
  getActionReasons?: Maybe<Array<ActionReason>>;
  getActionStatistics: StatisticsResponse;
  getApp: App;
  getAppCounters: AppCounters;
  getAppPermissions?: Maybe<Array<AppPermission>>;
  getAppPermissionsByAppID?: Maybe<Array<AppPermission>>;
  getAppReview: AppReview;
  getAppReviews: GetAppReviewsResponse;
  getAppSubscriptions?: Maybe<Array<AppSubscription>>;
  getAppSubscriptionsByAppID?: Maybe<Array<AppSubscription>>;
  getAppTags?: Maybe<Array<AppTag>>;
  getAppTagsByAppID?: Maybe<Array<AppTag>>;
  getAppTokens?: Maybe<Array<AppToken>>;
  getAppUserContext: AppUserContext;
  getApps?: Maybe<Array<App>>;
  getAppsEnabledByDefault: GetAppsEnabledByDefaultResponse;
  /** get attachment meta by id */
  getAttachment: Attachment;
  /** get attachment metas by ids */
  getAttachments?: Maybe<Array<Attachment>>;
  /** returns a vote object for specific candidate */
  getCandidateVote: DirectorVote;
  getCircleApps: GetCircleAppsResponse;
  /** get circle by ID */
  getCircleById: Circle;
  /** get circle by name */
  getCircleByName: Circle;
  /** get circle custom roles */
  getCircleCustomRoles: CircleCustomRolesResponse;
  /** gets circle feed */
  getCircleFeed: CircleFeed;
  /** get circle member custom roles */
  getCircleMemberCustomRoles: CircleMemberCustomRoles;
  /** get permissions */
  getCirclePermissions: CirclePermissions;
  /** return a list of all votes in specific circle grouped by candidate */
  getCircleVotes: Array<Maybe<DirectorVote>>;
  getCirclesForTags?: Maybe<Array<Scalars['ID']>>;
  /** get collection by id */
  getCollection: Collection;
  /** get collection counters */
  getCollectionCounter: CollectionCounter;
  /** get a single comment */
  getComment?: Maybe<Comment>;
  /** get comment context */
  getCommentContext: CommentContext;
  /** get comment pin */
  getCommentPin?: Maybe<Pin>;
  /** get comments */
  getComments: CommentsConnection;
  /** gets contact info by id */
  getContactById: UserContact;
  /** get Discord config */
  getDiscordConfig: DiscordConfigResponse;
  /** @deprecated Use 'getGovernance' instead */
  getElection: Election;
  getEndpoints?: Maybe<Array<Endpoint>>;
  getFeaturedApps: GetFeaturedAppsResponse;
  /** get featured circles */
  getFeaturedCircles: GetFeaturedCirclesResponse;
  /** gets user feed */
  getFeed: UserFeed;
  getGendersForTag?: Maybe<Array<Scalars['String']>>;
  /** get governance object for a circle */
  getGovernance: Governance;
  getLastUsedApps: GetLastUsedAppsResponse;
  /** get last viewed circles */
  getLastViewedCircles: GetLastViewedCirclesResponse;
  getLinkedAccounts: LinkAccountsPayload;
  /** get circle members with cursor */
  getMembers: CircleParticipants;
  /** get meta words for circle */
  getMetaWordsForCircle?: Maybe<Array<Scalars['String']>>;
  getModeratorActions: ModeratorActionsConnection;
  getModerators?: Maybe<Array<Moderator>>;
  /** get my seeds count in specific circle */
  getMySeedsCount: Scalars['Int'];
  /** get all votes of bearer user in specific circle */
  getMyVotes: Array<Maybe<DirectorVote>>;
  getNotificationSettings: NotificationsSettings;
  /** Get an offer */
  getOffer: Offer;
  getPermissions?: Maybe<Array<Permission>>;
  /** gets pinned comments for a given post */
  getPinnedComments?: Maybe<Array<Maybe<Comment>>>;
  /** get pinned posts */
  getPinnedPosts?: Maybe<Array<Maybe<Post>>>;
  /** single post by id */
  getPost: Post;
  /** get post context */
  getPostContext: PostContext;
  /** get post pin */
  getPostPin?: Maybe<Pin>;
  /** single post by id without authentication */
  getPostPreview: Post;
  getRecommendedChats: GetRecommendedChatsResponse;
  getRecommendedCircles: GetRecommendedCirclesResponse;
  /** expose a number of X messages before and after a specific message from a circle */
  getRelatedMessages?: Maybe<Array<ChatMessage>>;
  getReports: ReportsResponse;
  getSavedApps: GetSavedAppsResponse;
  getSavedSearchResults: GetSavedSearchResultsResponse;
  getSearchResultPayload?: Maybe<SearchResultPayload>;
  /** returns captcha site key */
  getSignInCaptchaParams: GetSignInCaptchaParamsPayload;
  getSignInCaptchaParamsV3: GetSignInCaptchaParamsPayload;
  /** get slice by id */
  getSliceById: Slice;
  /** single sound by id */
  getSound: Sound;
  getTags?: Maybe<Array<Tag>>;
  getTrendingApps: GetTrendingAppsResponse;
  /** get unassigned circle custom roles */
  getUnassignedCircleCustomRoles?: Maybe<Array<CircleCustomRole>>;
  /** get user circle list */
  getUserCircles: CirclesResponse;
  /** get user contacts */
  getUserContacts?: Maybe<GetUserContactsResponse>;
  /** get user custom roles in circle */
  getUserCustomRolesInCircle: CircleUserCustomRolesResponse;
  /** Get amount of melons available on user account */
  getUserMelons: Scalars['Int'];
  getUserProfileMeta?: Maybe<ProfileMeta>;
  getVerifiedUsers: VerifiedUsersResponse;
  /** list of preferred languages we support for circles/chats etc. */
  languages: Array<Maybe<Language>>;
  /** queries for url's metadata to show link preview */
  linkMetaData: LinkMetaDataResponse;
  /** list groups */
  listGroups: Groups;
  listLanguages?: Maybe<Array<PLanguage>>;
  /** returns given message by their respective ids */
  message?: Maybe<ChatMessage>;
  myApps?: Maybe<Array<App>>;
  /** gets my profile info */
  myProfile: PrivateProfile;
  /** gets list of notifications, paginated */
  notificationGet: NotificationsConnection;
  notificationGetSettings: NotificationsSettings;
  notificationGetUnreadCount: NotificationUnreadCountResponse;
  /** get on-boarding circles */
  onBoardingCirclesConnection: OnBoardingCirclesConnectionResponse;
  /** list of popular feeds */
  popularFeed?: Maybe<Feed>;
  /** reactions for a post */
  postReactionsConnection: ReactionsConnection;
  /** allows for searching trough posts */
  postsConnection: PostsConnection;
  /** gets user's sorting type for a given circle */
  postsSortingType: PostsSortingType;
  /** get user publicly for landing page */
  profileGetUserForLandingPage: PublicLandingPage;
  /** get userID by it nickname publicly for landing page */
  profileGetUserIDByName: UserIdPayload;
  /** get circle landing page */
  publicCircleLandingPage?: Maybe<PublicCircleLandingPageResponse>;
  reportReasons?: Maybe<Array<ReportReason>>;
  reportsCount: Scalars['Int'];
  /** get saved posts */
  savedPostsConnection: PostsConnection;
  searchApps: SearchAppResponse;
  /** get and out all users that are in current circle */
  searchUsersConnection: SearchUsersConnectionResponse;
  searchUsersForDirectChat: UsersForDirectChat;
  /** search for a director candidate to vote */
  searchVoteCandidates: VoteCandidatesConnection;
  /** gets settings of authenticated user */
  settings: UserSettings;
  /** gets list of slices, paginated */
  slicesConnection: SlicesResponse;
  /** get slice members with cursor, paginated */
  slicesMembersConnection: SliceMemberResponse;
  /** list of posts ordered by sorting type */
  sortedCirclePostsConnection: SortedCirclePostsConnection;
  /** list of sounds that can be used in 'thought' types of posts */
  soundsConnection: SoundsConnection;
  trackedCirclesList?: Maybe<Array<Circle>>;
  /** get list of unread chat meta data */
  unreadChats?: Maybe<Array<ChatUnreadMetaData>>;
  /** gets user info by id */
  user: PublicProfile;
  userCountData?: Maybe<Array<CountData>>;
  /** list of feeds for current user */
  userFeed?: Maybe<Feed>;
  userMentionsConnection: MentionsConnection;
  /** list of user posts */
  userPostsConnection: PostsConnection;
  /** Get seeds connection for user */
  userSeedsConnection: SeedsConnection;
  usersConnection: UsersConnection;
  /** list of users that reacted with this reaction */
  usersReactedOnMessage: UsersConnection;
  validateUsername: SuccessPayload;
};


export type QueryBlacklistListGlobalWordsArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryBlacklistListUrlsArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryBlockedUsersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryBlwConnectionArgs = {
  blwConnectionInput?: InputMaybe<BlwConnectionRequest>;
  cursor?: InputMaybe<CursorInput>;
};


export type QueryChatArgs = {
  id: Scalars['ID'];
};


export type QueryChatFeedConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryChatMembersConnectionArgs = {
  chatId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryChatMessagesConnectionArgs = {
  chatId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryChatParticipantsConnectionArgs = {
  chatId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryChatSettingsArgs = {
  chatId: Scalars['ID'];
};


export type QueryChatsConnectionArgs = {
  chatTypes?: InputMaybe<Array<ChatType>>;
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryCheckUsernameArgs = {
  username: Scalars['String'];
};


export type QueryCircleAppsArgs = {
  input: CircleAppsInput;
};


export type QueryCircleCountDataArgs = {
  input: CountDataInput;
};


export type QueryCircleFeedArgs = {
  circleId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
};


export type QueryCircleJoinRequestsConnectionArgs = {
  circleJoinRequestsConnectionInput: CircleJoinRequestsConnectionRequest;
};


export type QueryCircleMetricsDataArgs = {
  input: CircleMetricsDataInput;
};


export type QueryCirclePostsConnectionArgs = {
  circleId: Scalars['String'];
  cursor?: InputMaybe<CursorInput>;
};


export type QueryCircleReportsAdminPanelArgs = {
  cursor?: InputMaybe<CursorInput>;
  filter?: InputMaybe<CircleReportsFilterBy>;
};


export type QueryCircleReportsConnectionArgs = {
  circleReportsConnectionInput?: InputMaybe<CircleReportsConnectionInput>;
};


export type QueryCircleSeedsConnectionArgs = {
  circleID: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
};


export type QueryCirclesConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  groupId?: InputMaybe<Scalars['String']>;
  isStrict?: InputMaybe<Scalars['Boolean']>;
  searchQuery?: InputMaybe<Scalars['String']>;
  sort?: InputMaybe<CircleSortBy>;
};


export type QueryCollectionPostsConnectionArgs = {
  collectionId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
};


export type QueryCollectionsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  returnSavedPostsCollection?: InputMaybe<Scalars['Boolean']>;
  sortBy?: InputMaybe<CollectionsSortType>;
  userId: Scalars['ID'];
  withPreviewPosts: Scalars['Boolean'];
};


export type QueryCommentReactionsConnectionArgs = {
  commentId: Scalars['String'];
  cursor?: InputMaybe<CursorInput>;
};


export type QueryContentStatsForCircleArgs = {
  circleID: Scalars['String'];
};


export type QueryContentStatsForContentArgs = {
  contentID: Scalars['String'];
};


export type QueryContentStatsForProfileArgs = {
  userID: Scalars['String'];
};


export type QueryDocumentGetArgs = {
  key: Scalars['String'];
};


export type QueryElectionParticipantsConnectionArgs = {
  electionParticipantsConnectionInput?: InputMaybe<ElectionParticipantsConnectionInput>;
};


export type QueryFeedArgs = {
  id: Scalars['ID'];
};


export type QueryFeedPostsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  feedId: Scalars['String'];
};


export type QueryFeedsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryFollowersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
  userId: Scalars['ID'];
};


export type QueryGetActionStatisticsArgs = {
  moderator_id: Scalars['String'];
};


export type QueryGetAppArgs = {
  id: Scalars['ID'];
};


export type QueryGetAppCountersArgs = {
  id: Scalars['ID'];
};


export type QueryGetAppPermissionsArgs = {
  ids?: InputMaybe<Array<Scalars['ID']>>;
};


export type QueryGetAppPermissionsByAppIdArgs = {
  appID: Scalars['ID'];
};


export type QueryGetAppReviewArgs = {
  reviewID: Scalars['ID'];
};


export type QueryGetAppReviewsArgs = {
  appID: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
};


export type QueryGetAppSubscriptionsArgs = {
  ids?: InputMaybe<Array<Scalars['ID']>>;
};


export type QueryGetAppSubscriptionsByAppIdArgs = {
  appID: Scalars['ID'];
};


export type QueryGetAppTagsArgs = {
  ids?: InputMaybe<Array<Scalars['ID']>>;
};


export type QueryGetAppTagsByAppIdArgs = {
  appID: Scalars['ID'];
};


export type QueryGetAppTokensArgs = {
  appID: Scalars['ID'];
};


export type QueryGetAppUserContextArgs = {
  appID: Scalars['ID'];
};


export type QueryGetAppsArgs = {
  ids: Array<Scalars['ID']>;
};


export type QueryGetAppsEnabledByDefaultArgs = {
  in?: InputMaybe<GetAppsEnabledByDefaultInput>;
};


export type QueryGetAttachmentArgs = {
  id: Scalars['String'];
};


export type QueryGetAttachmentsArgs = {
  ids?: InputMaybe<Array<Scalars['String']>>;
};


export type QueryGetCandidateVoteArgs = {
  candidateId: Scalars['ID'];
  circleId: Scalars['ID'];
};


export type QueryGetCircleAppsArgs = {
  input: GetCircleAppsInput;
};


export type QueryGetCircleByIdArgs = {
  getCircleByIdInput: GetCircleByIdRequest;
};


export type QueryGetCircleByNameArgs = {
  getCircleByNameInput?: InputMaybe<GetCircleByNameRequest>;
};


export type QueryGetCircleCustomRolesArgs = {
  circleId: Scalars['String'];
};


export type QueryGetCircleFeedArgs = {
  circleId: Scalars['String'];
  limit: Scalars['Int'];
};


export type QueryGetCircleMemberCustomRolesArgs = {
  circleId: Scalars['String'];
  userId: Scalars['String'];
};


export type QueryGetCirclePermissionsArgs = {
  circleId: Scalars['String'];
};


export type QueryGetCircleVotesArgs = {
  circleId: Scalars['ID'];
};


export type QueryGetCirclesForTagsArgs = {
  tagIDs: Array<Scalars['ID']>;
};


export type QueryGetCollectionArgs = {
  collectionId: Scalars['ID'];
  withPreviewPosts: Scalars['Boolean'];
};


export type QueryGetCollectionCounterArgs = {
  collectionId: Scalars['ID'];
};


export type QueryGetCommentArgs = {
  id: Scalars['ID'];
};


export type QueryGetCommentContextArgs = {
  commentId: Scalars['ID'];
};


export type QueryGetCommentPinArgs = {
  commentId: Scalars['ID'];
};


export type QueryGetCommentsArgs = {
  data: GetCommentsInput;
};


export type QueryGetContactByIdArgs = {
  contactID: Scalars['ID'];
};


export type QueryGetDiscordConfigArgs = {
  circleId: Scalars['String'];
};


export type QueryGetElectionArgs = {
  electionInput: ElectionInput;
};


export type QueryGetFeaturedAppsArgs = {
  in?: InputMaybe<GetFeaturedAppsInput>;
};


export type QueryGetFeaturedCirclesArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryGetFeedArgs = {
  limit: Scalars['Int'];
};


export type QueryGetGendersForTagArgs = {
  tagID: Scalars['ID'];
};


export type QueryGetGovernanceArgs = {
  circleId: Scalars['ID'];
};


export type QueryGetLastUsedAppsArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryGetLastViewedCirclesArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryGetMembersArgs = {
  getMembersInput?: InputMaybe<GetMembersInput>;
};


export type QueryGetMetaWordsForCircleArgs = {
  circleID: Scalars['String'];
};


export type QueryGetModeratorActionsArgs = {
  action?: InputMaybe<Scalars['String']>;
  cursor?: InputMaybe<CursorInput>;
  filter?: InputMaybe<Scalars['String']>;
  moderator_id?: InputMaybe<Scalars['String']>;
};


export type QueryGetMySeedsCountArgs = {
  circleID: Scalars['ID'];
};


export type QueryGetMyVotesArgs = {
  circleId: Scalars['ID'];
};


export type QueryGetOfferArgs = {
  offerId: Scalars['ID'];
};


export type QueryGetPinnedCommentsArgs = {
  postId: Scalars['String'];
};


export type QueryGetPinnedPostsArgs = {
  circleId: Scalars['String'];
};


export type QueryGetPostArgs = {
  postId?: InputMaybe<Scalars['String']>;
  returnDeleted?: InputMaybe<Scalars['Boolean']>;
  shortId?: InputMaybe<Scalars['String']>;
};


export type QueryGetPostContextArgs = {
  postId: Scalars['ID'];
};


export type QueryGetPostPinArgs = {
  postId: Scalars['ID'];
};


export type QueryGetPostPreviewArgs = {
  postId: Scalars['String'];
};


export type QueryGetRecommendedChatsArgs = {
  input: GetRecommendedChatsInput;
};


export type QueryGetRecommendedCirclesArgs = {
  input: GetRecommendedCirclesInput;
};


export type QueryGetRelatedMessagesArgs = {
  marginSize: Scalars['Int'];
  messageId: Scalars['ID'];
};


export type QueryGetReportsArgs = {
  cursor?: InputMaybe<CursorInput>;
  entity: ReportEntity;
  status: ReportStatus;
};


export type QueryGetSavedAppsArgs = {
  in?: InputMaybe<GetSavedAppsInput>;
};


export type QueryGetSavedSearchResultsArgs = {
  cursor?: InputMaybe<CursorInput>;
  kind: SearchEntityKind;
};


export type QueryGetSearchResultPayloadArgs = {
  entityID: Scalars['ID'];
  kind: SearchEntityKind;
};


export type QueryGetSliceByIdArgs = {
  getSliceByIdInput: GetSliceByIdInputRequest;
};


export type QueryGetSoundArgs = {
  soundId: Scalars['String'];
};


export type QueryGetTagsArgs = {
  gender?: InputMaybe<Scalars['String']>;
};


export type QueryGetTrendingAppsArgs = {
  in?: InputMaybe<GetTrendingAppsInput>;
};


export type QueryGetUnassignedCircleCustomRolesArgs = {
  circleId: Scalars['String'];
  userId: Scalars['String'];
};


export type QueryGetUserCirclesArgs = {
  getUserCircles: GetUserCirclesRequest;
};


export type QueryGetUserContactsArgs = {
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryGetUserCustomRolesInCircleArgs = {
  circleId: Scalars['String'];
  userId: Scalars['String'];
};


export type QueryGetUserMelonsArgs = {
  userId: Scalars['ID'];
};


export type QueryGetUserProfileMetaArgs = {
  id: Scalars['ID'];
};


export type QueryGetVerifiedUsersArgs = {
  cursor?: InputMaybe<CursorInput>;
  filter?: InputMaybe<Scalars['String']>;
};


export type QueryLinkMetaDataArgs = {
  link: Scalars['String'];
};


export type QueryListGroupsArgs = {
  listGroupsRequest?: InputMaybe<ListGroupsInput>;
};


export type QueryListLanguagesArgs = {
  filter: LanguageFilter;
};


export type QueryMessageArgs = {
  messageId: Scalars['ID'];
};


export type QueryNotificationGetArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QueryPostReactionsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  postId: Scalars['String'];
};


export type QueryPostsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryPostsSortingTypeArgs = {
  circleId: Scalars['String'];
};


export type QueryProfileGetUserForLandingPageArgs = {
  userId: Scalars['ID'];
};


export type QueryProfileGetUserIdByNameArgs = {
  userName: Scalars['String'];
};


export type QueryPublicCircleLandingPageArgs = {
  circleId: Scalars['String'];
};


export type QueryReportReasonsArgs = {
  entity: ReportEntity;
};


export type QueryReportsCountArgs = {
  status: ReportStatus;
};


export type QuerySavedPostsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};


export type QuerySearchAppsArgs = {
  search?: InputMaybe<SearchInput>;
};


export type QuerySearchUsersConnectionArgs = {
  circleID?: InputMaybe<Scalars['String']>;
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QuerySearchUsersForDirectChatArgs = {
  cursor?: InputMaybe<CursorInput>;
  query?: InputMaybe<Scalars['String']>;
};


export type QuerySearchVoteCandidatesArgs = {
  circleId: Scalars['ID'];
  cursor?: InputMaybe<CursorInput>;
  search?: InputMaybe<Scalars['String']>;
};


export type QuerySlicesConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  sliceConnectionInput?: InputMaybe<SliceConnectionRequest>;
};


export type QuerySlicesMembersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  slicesMembersConnectionInput?: InputMaybe<SlicesMembersConnectionRequest>;
};


export type QuerySortedCirclePostsConnectionArgs = {
  circleId: Scalars['String'];
  cursor?: InputMaybe<CursorInput>;
  returnPinned?: InputMaybe<Scalars['Boolean']>;
  sortingType: PostsSortingType;
};


export type QuerySoundsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryUserArgs = {
  id: Scalars['ID'];
};


export type QueryUserCountDataArgs = {
  input: CountDataInput;
};


export type QueryUserFeedArgs = {
  cursor?: InputMaybe<CursorInput>;
  userId: Scalars['ID'];
};


export type QueryUserMentionsConnectionArgs = {
  meta?: InputMaybe<NotifyMeta>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryUserPostsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  userId: Scalars['String'];
};


export type QueryUserSeedsConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  userID: Scalars['ID'];
};


export type QueryUsersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
  searchQuery?: InputMaybe<Scalars['String']>;
};


export type QueryUsersReactedOnMessageArgs = {
  cursor?: InputMaybe<CursorInput>;
  messageID: Scalars['ID'];
  reaction: Scalars['String'];
};


export type QueryValidateUsernameArgs = {
  username: Scalars['String'];
};

export type Reaction = {
  __typename?: 'Reaction';
  author?: Maybe<User>;
  authorId: Scalars['ID'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  reaction: Scalars['String'];
};

export type ReactionInput = {
  id: Scalars['String'];
  react: Scalars['Boolean'];
  reaction: Scalars['String'];
};

export type ReactionsConnection = {
  __typename?: 'ReactionsConnection';
  edges?: Maybe<Array<Maybe<ReactionsEdge>>>;
  pageInfo: PageInfo;
};

export type ReactionsEdge = {
  __typename?: 'ReactionsEdge';
  cursorId: Scalars['ID'];
  node: Reaction;
};

export enum Reason {
  GraphicContent = 'GRAPHIC_CONTENT',
  Other = 'OTHER',
  SexualContent = 'SEXUAL_CONTENT',
  Spam = 'SPAM'
}

export type Relations = {
  __typename?: 'Relations';
  followedBy?: Maybe<Scalars['Boolean']>;
  following?: Maybe<Scalars['Boolean']>;
};

export type RemoveCustomBlWordInput = {
  circleId: Scalars['ID'];
  words?: InputMaybe<Array<Scalars['String']>>;
};

export type RemoveUsersFromSliceRequest = {
  sliceId: Scalars['ID'];
  userIds: Array<Scalars['ID']>;
};

export type Report = {
  __typename?: 'Report';
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  entityID: Scalars['ID'];
  entityType: ReportEntity;
  id: Scalars['ID'];
  reason: Scalars['String'];
  reporter?: Maybe<Reporter>;
  status: ReportStatus;
};

export enum ReportAction {
  Ban = 'BAN',
  Delete = 'DELETE',
  NotExplicit = 'NOT_EXPLICIT'
}

export enum ReportEntity {
  All = 'ALL',
  Chat = 'CHAT',
  Circle = 'CIRCLE',
  Collection = 'COLLECTION',
  Comment = 'COMMENT',
  Message = 'MESSAGE',
  Post = 'POST',
  Slice = 'SLICE',
  User = 'USER'
}

export type ReportInput = {
  anyId: Scalars['ID'];
  circleId: Scalars['ID'];
  comment: Scalars['String'];
  contentAuthorId: Scalars['String'];
  reason: Reason;
  reportType: ReportType;
};

export type ReportReason = {
  __typename?: 'ReportReason';
  id: Scalars['ID'];
  reason: Scalars['String'];
};

export enum ReportStatus {
  AllStatus = 'AllStatus',
  Pending = 'Pending',
  Resolved = 'Resolved',
  Unresolved = 'Unresolved'
}

export enum ReportType {
  Comment = 'COMMENT',
  Message = 'MESSAGE',
  Post = 'POST',
  User = 'USER',
  UserTemp = 'USER_TEMP'
}

export type ReportedEntity = {
  __typename?: 'ReportedEntity';
  entityID: Scalars['String'];
  entityType: ReportEntity;
  reports?: Maybe<Array<Report>>;
  reportsCount: Scalars['Int'];
};

export type Reporter = {
  __typename?: 'Reporter';
  age: Scalars['Int'];
  email: Scalars['String'];
  fullName: Scalars['String'];
  isBanned: Scalars['Boolean'];
  isVerified: Scalars['Boolean'];
  phone: Scalars['String'];
  profileImageUrl: Scalars['String'];
  username: Scalars['String'];
};

export type ReportsResponse = {
  __typename?: 'ReportsResponse';
  pageInfo: PageInfo;
  reportedEntities?: Maybe<Array<ReportedEntity>>;
};

export type ResolveCircleReportInput = {
  circleId: Scalars['ID'];
  fullFill?: InputMaybe<Scalars['Boolean']>;
  reportId: Scalars['ID'];
};

export type ResolveReportInput = {
  circleId: Scalars['ID'];
  fullFill?: InputMaybe<Scalars['Boolean']>;
  reportId: Scalars['ID'];
};

export enum ResolveStatus {
  Resolved = 'RESOLVED',
  Unresolved = 'UNRESOLVED'
}

/** General response payload, all response payloads should follow this structure */
export type ResponsePayload = {
  __typename?: 'ResponsePayload';
  errors?: Maybe<Array<Maybe<Error>>>;
};

export type RevokeDiscordWebhookInput = {
  circleId: Scalars['ID'];
};

/** input for setUsersRoleInCircle, a list of role assignments to given users */
export type RoleAssignmentsInput = {
  roleAssignments?: InputMaybe<UserRoleAssignmentInput>;
};

export enum RulesType {
  Democratic = 'DEMOCRATIC',
  Director = 'DIRECTOR',
  None = 'NONE'
}

export type SaveMetaWordsInput = {
  circleId: Scalars['ID'];
  words: Array<Scalars['String']>;
};

export type SearchAppResponse = {
  __typename?: 'SearchAppResponse';
  edges?: Maybe<Array<AppEdge>>;
  pageInfo: PageInfo;
};

export type SearchEntityId = {
  __typename?: 'SearchEntityID';
  id: Scalars['ID'];
};

export type SearchEntityKey = SearchEntityId;

export enum SearchEntityKind {
  App = 'App',
  Chat = 'Chat',
  Circle = 'Circle',
  Post = 'Post',
  Profile = 'Profile'
}

export type SearchInput = {
  cursor?: InputMaybe<CursorInput>;
  nameStartsWith: Scalars['String'];
  orderBy?: AppOrder;
  tagIds?: InputMaybe<Array<Scalars['ID']>>;
};

export type SearchResult = {
  __typename?: 'SearchResult';
  createdAt: Scalars['DateTime'];
  entityKey: SearchEntityKey;
  kind: SearchEntityKind;
  payload?: Maybe<SearchResultPayload>;
};

export type SearchResultEdge = {
  __typename?: 'SearchResultEdge';
  cursorId: Scalars['ID'];
  node: SearchResult;
};

export type SearchResultPayload = App | Chat | Circle | Post | ProfileMeta;

export type SearchUsersConnectionResponse = {
  __typename?: 'SearchUsersConnectionResponse';
  edges?: Maybe<Array<UsersEdge>>;
  pageInfo: PageInfo;
};

/** Seed type containing info about amount, circle to which it belongs to and the user possessing the seeds */
export type Seed = {
  __typename?: 'Seed';
  amountAvailable: Scalars['Int'];
  amountLocked: Scalars['Int'];
  amountTotal: Scalars['Int'];
  circle: Circle;
  circleId: Scalars['ID'];
  owner: User;
  ownerId: Scalars['ID'];
};

/** Connection for circle or user seeds */
export type SeedsConnection = {
  __typename?: 'SeedsConnection';
  edges?: Maybe<Array<SeedsEdge>>;
  pageInfo: PageInfo;
  totalSeedsAmount?: Maybe<Scalars['Int']>;
};

/** Edge container for circle or user seeds data */
export type SeedsEdge = {
  __typename?: 'SeedsEdge';
  cursorId: Scalars['ID'];
  node: Seed;
};

/** Input data for sending an exchange offer */
export type SendOfferInput = {
  circleId: Scalars['ID'];
  melonsAmount: Scalars['Int'];
  offerType: OfferType;
  recipientId: Scalars['ID'];
  seedsAmount: Scalars['Int'];
};

export type SendPushToCircleInput = {
  body: Scalars['String'];
  circle_id: Scalars['String'];
  image_url: Scalars['String'];
  post_id: Scalars['String'];
  title: Scalars['String'];
};

export type SetCircleRulesInput = {
  circleId: Scalars['ID'];
  rulesText: Scalars['String'];
  rulesType: RulesType;
};

export enum SignInMethod {
  Email = 'EMAIL',
  Phone = 'PHONE',
  Unknown = 'UNKNOWN'
}

export type SignInModerationInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type SignInWithPhoneNumberPayload = {
  __typename?: 'SignInWithPhoneNumberPayload';
  sessionInfo?: Maybe<Scalars['String']>;
};

export type SignInWithUsernamePayload = {
  __typename?: 'SignInWithUsernamePayload';
  maskedIdentifier: Scalars['String'];
  sessionInfo?: Maybe<Scalars['String']>;
  signInMethod: SignInMethod;
};

export type SignUpGuestPayload = {
  __typename?: 'SignUpGuestPayload';
  accessJwt: Scalars['String'];
  refreshJwt: Scalars['String'];
  userId: Scalars['String'];
};

export type SingleChatInput = {
  /** size must be equal to 2 (single chat between two users) */
  userIds: Array<Scalars['ID']>;
};

export type Slice = Node & {
  __typename?: 'Slice';
  chat?: Maybe<Chat>;
  circleId: Scalars['ID'];
  description?: Maybe<Scalars['String']>;
  discoverable: Scalars['Boolean'];
  iJoined: Scalars['Boolean'];
  iRequestedToJoin: Scalars['Boolean'];
  id: Scalars['ID'];
  image: Scalars['ImageUrl'];
  membersCount: Scalars['Int'];
  name: Scalars['String'];
  ownerId: Scalars['ID'];
  private: Scalars['Boolean'];
  role: SliceRole;
  roomsCount: Scalars['Int'];
  rules?: Maybe<Scalars['String']>;
  shareLink: Scalars['String'];
};

export type SliceConnectionRequest = {
  circleId: Scalars['ID'];
  searchQuery?: InputMaybe<Scalars['String']>;
};

export type SliceEdge = {
  __typename?: 'SliceEdge';
  cursorId: Scalars['ID'];
  node: Slice;
};

export type SliceInput = {
  circleID?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  discoverable?: InputMaybe<Scalars['Boolean']>;
  image?: InputMaybe<Scalars['Upload']>;
  name?: InputMaybe<Scalars['String']>;
  private?: InputMaybe<Scalars['Boolean']>;
  rules?: InputMaybe<Scalars['String']>;
};

export type SliceMember = {
  __typename?: 'SliceMember';
  publicProfile?: Maybe<PublicProfile>;
  role: SliceRole;
};

export type SliceMemberEdge = {
  __typename?: 'SliceMemberEdge';
  cursorId: Scalars['ID'];
  node: SliceMember;
};

export type SliceMemberResponse = {
  __typename?: 'SliceMemberResponse';
  edges?: Maybe<Array<SliceParticipantEdge>>;
  pageInfo: PageInfo;
};

export type SliceParticipant = {
  __typename?: 'SliceParticipant';
  bannedAt?: Maybe<Scalars['DateTime']>;
  joinedAt?: Maybe<Scalars['DateTime']>;
  role: SliceRole;
  sliceId: Scalars['ID'];
  user: MemberProfile;
  userId: Scalars['ID'];
};

export type SliceParticipantEdge = {
  __typename?: 'SliceParticipantEdge';
  cursorId: Scalars['ID'];
  node: SliceParticipant;
};

export enum SliceRole {
  Member = 'MEMBER',
  None = 'NONE',
  Owner = 'OWNER',
  Pending = 'PENDING'
}

export type SliceUpdateInput = {
  description?: InputMaybe<Scalars['String']>;
  discoverable?: InputMaybe<Scalars['Boolean']>;
  image?: InputMaybe<Scalars['Upload']>;
  name?: InputMaybe<Scalars['String']>;
  private?: InputMaybe<Scalars['Boolean']>;
  rules?: InputMaybe<Scalars['String']>;
};

export type SlicesMembersConnectionRequest = {
  roles?: InputMaybe<Array<InputMaybe<SliceRole>>>;
  sliceId: Scalars['ID'];
};

export type SlicesResponse = {
  __typename?: 'SlicesResponse';
  edges?: Maybe<Array<SliceEdge>>;
  pageInfo: PageInfo;
};

export type SortedCirclePostsConnection = {
  __typename?: 'SortedCirclePostsConnection';
  circleId: Scalars['ID'];
  edges?: Maybe<Array<PostsEdge>>;
  pageInfo: PageInfo;
  pinnedPosts?: Maybe<Array<Maybe<Post>>>;
  returnPinned?: Maybe<Scalars['Boolean']>;
  sortingType: PostsSortingType;
};

/** sound that can be used in 'thought' post */
export type Sound = {
  __typename?: 'Sound';
  creator?: Maybe<Scalars['String']>;
  /** duration of the sound in milliseconds */
  duration: Scalars['Int'];
  icon: Scalars['String'];
  id: Scalars['String'];
  title: Scalars['String'];
  url: Scalars['String'];
  /** how many times given sound was used */
  usesCount: Scalars['Int'];
};

export type SoundsConnection = {
  __typename?: 'SoundsConnection';
  edges?: Maybe<Array<SoundsEdge>>;
  pageInfo: PageInfo;
};

export type SoundsEdge = {
  __typename?: 'SoundsEdge';
  cursorId: Scalars['ID'];
  node: Sound;
};

export type StatisticsResponse = {
  __typename?: 'StatisticsResponse';
  statistics: ActionStatistic;
};

/** graphql subscriptions: https://www.apollographql.com/docs/react/data/subscriptions/ */
export type Subscription = {
  __typename?: 'Subscription';
  /** listens to comment additions, returns the newly added comment */
  commentAdded: Comment;
  /** listens to comment removals, returns id of the removed comment */
  commentRemoved: Scalars['ID'];
  /** listens to comment updates, returns the updated comment */
  commentUpdated: Comment;
};


/** graphql subscriptions: https://www.apollographql.com/docs/react/data/subscriptions/ */
export type SubscriptionCommentAddedArgs = {
  postId: Scalars['ID'];
};


/** graphql subscriptions: https://www.apollographql.com/docs/react/data/subscriptions/ */
export type SubscriptionCommentRemovedArgs = {
  postId: Scalars['ID'];
};


/** graphql subscriptions: https://www.apollographql.com/docs/react/data/subscriptions/ */
export type SubscriptionCommentUpdatedArgs = {
  postId: Scalars['ID'];
};

/** ISuccessPayload default realization */
export type SuccessPayload = ISuccessPayload & {
  __typename?: 'SuccessPayload';
  success: Scalars['Boolean'];
};

export type Tag = {
  __typename?: 'Tag';
  genders?: Maybe<Array<Scalars['String']>>;
  id: Scalars['ID'];
  name: Scalars['String'];
};

export enum TextPostColor {
  Blue = 'blue',
  Green = 'green',
  None = 'none',
  Purple = 'purple',
  Red = 'red',
  Yellow = 'yellow'
}

export type TextPostContent = {
  __typename?: 'TextPostContent';
  /** color name, app displays colors as gradients though */
  color: TextPostColor;
  more: Scalars['String'];
  /** optional sound */
  sound: Sound;
  text: Scalars['String'];
};

/** input for creating text post */
export type TextPostContentInput = {
  /** name of the color to use, app displays those as gradients */
  color?: InputMaybe<TextPostColor>;
  /** optional 'more' info */
  more?: InputMaybe<Scalars['String']>;
  text: Scalars['String'];
};

export type TransferInput = {
  circleId: Scalars['ID'];
  recipientId: Scalars['ID'];
  seedsAmount: Scalars['Int'];
};

export type UpdateAppInput = {
  description?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  imageUrl?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
  webhookUrl?: InputMaybe<Scalars['String']>;
};

export type UpdateCircleCustomRoleInput = {
  canAttachFiles: Scalars['Boolean'];
  canEmbedLinks: Scalars['Boolean'];
  canManageCircle: Scalars['Boolean'];
  canManageComments: Scalars['Boolean'];
  canManageMessages: Scalars['Boolean'];
  canManagePosts: Scalars['Boolean'];
  canManageReports: Scalars['Boolean'];
  canManageRoles: Scalars['Boolean'];
  canManageUsers: Scalars['Boolean'];
  canPost: Scalars['Boolean'];
  canSendMsg: Scalars['Boolean'];
  circleId: Scalars['ID'];
  color: Scalars['String'];
  emoji?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  roleId: Scalars['ID'];
};

/** input used in createCircle mutation */
export type UpdateCirclesInput = {
  circleId: Scalars['ID'];
  payload: CirclesInput;
};

export type UpdateCollectionInput = {
  description?: InputMaybe<Scalars['String']>;
  isPublic?: InputMaybe<Scalars['Boolean']>;
  title?: InputMaybe<Scalars['String']>;
};

/** UpdateCommentInput is input used to update a comment */
export type UpdateCommentInput = {
  id: Scalars['ID'];
  text: Scalars['String'];
};

export type UpdateEndpointPermissionsInput = {
  endpoint: Scalars['String'];
  required_permissions?: InputMaybe<Array<Scalars['String']>>;
};

export type UpdateModeratorPermissionInput = {
  custom_indexes?: InputMaybe<Array<Scalars['String']>>;
  full_access?: InputMaybe<Scalars['Boolean']>;
  moderator_permission_id: Scalars['String'];
};

export type UpdateProfileInput = {
  info: UserObject;
};

export type UpdateSliceInput = {
  payload: SliceUpdateInput;
  sliceId: Scalars['ID'];
};

export type UrlsList = {
  __typename?: 'UrlsList';
  pageInfo: PageInfo;
  urls?: Maybe<Array<Scalars['String']>>;
};

/** User info */
export type User = {
  bio?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  followers: Scalars['Int'];
  followersConnection: UsersConnection;
  fullName: Scalars['String'];
  id: Scalars['ID'];
  isVerified: Scalars['Boolean'];
  likes: Scalars['Int'];
  profileImage?: Maybe<Scalars['ImageUrl']>;
  shareLink: Scalars['String'];
  username: Scalars['String'];
  views: Scalars['Int'];
};


/** User info */
export type UserFollowersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};

export type UserAuthPayload = {
  __typename?: 'UserAuthPayload';
  authInfo: AuthInfo;
  user: PrivateProfile;
};

export type UserContact = {
  __typename?: 'UserContact';
  id: Scalars['String'];
  name: Scalars['String'];
  phone: ContactPhoneNumber;
};

export type UserContactsInput = {
  familyName: Scalars['String'];
  fullName: Scalars['String'];
  hasAvatar: Scalars['Boolean'];
  hasJobTitle: Scalars['Boolean'];
  phoneNumber: Array<ContactPhoneNumberInput>;
};

export type UserFeed = {
  __typename?: 'UserFeed';
  edges?: Maybe<Array<UserFeedEdge>>;
};

export type UserFeedEdge = {
  __typename?: 'UserFeedEdge';
  node: Post;
};

export type UserIdPayload = {
  __typename?: 'UserIDPayload';
  userId: Scalars['String'];
};

export type UserMention = {
  __typename?: 'UserMention';
  label: Scalars['String'];
  userId: Scalars['ID'];
};

export type UserObject = {
  age?: InputMaybe<Scalars['Int']>;
  bio?: InputMaybe<Scalars['String']>;
  countryCode?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  languageCodes?: InputMaybe<Array<Scalars['String']>>;
  name?: InputMaybe<Scalars['String']>;
  notificationsEnabled?: InputMaybe<Scalars['Boolean']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  profileImage?: InputMaybe<Scalars['String']>;
  username?: InputMaybe<Scalars['String']>;
};

/** Input for updating user profile image */
export type UserProfileImageInput = {
  profileImage: Scalars['Upload'];
};

/** Input for editing user profile */
export type UserProfileInput = {
  profileImage?: InputMaybe<Scalars['Upload']>;
  username?: InputMaybe<Scalars['String']>;
};

/** specifies the role for given user in circle */
export type UserRoleAssignmentInput = {
  circleId: Scalars['ID'];
  moderatorRoleSettings?: InputMaybe<ModeratorRoleSettingsInput>;
  role: CircleRole;
  userId: Scalars['ID'];
};

/** settings for current user */
export type UserSettings = {
  __typename?: 'UserSettings';
  blockedUsersConnection: UsersConnection;
  inviteLink?: Maybe<Scalars['String']>;
  languages?: Maybe<Array<Scalars['String']>>;
  privacy: PrivacySettings;
  shareLink?: Maybe<Scalars['String']>;
};


/** settings for current user */
export type UserSettingsBlockedUsersConnectionArgs = {
  cursor?: InputMaybe<CursorInput>;
};

/** input for updating settings for given user. Everything is nullable and only explicitly set up fields need to be updated on backend */
export type UserSettingsInput = {
  inviteLink?: InputMaybe<Scalars['String']>;
  languages?: InputMaybe<Array<Scalars['String']>>;
  privacy?: InputMaybe<PrivacySettingsInput>;
  shareLink?: InputMaybe<Scalars['String']>;
};

/** User data used in registration */
export type UserSignUpInput = {
  age: Scalars['Int'];
  countryCode: Scalars['String'];
  email: Scalars['String'];
  languageCodes?: InputMaybe<Array<Scalars['String']>>;
  notificationsEnabled: Scalars['Boolean'];
  phone: Scalars['String'];
  profileImage?: InputMaybe<Scalars['Upload']>;
  username: Scalars['String'];
};

export type UserStats = {
  __typename?: 'UserStats';
  followers: Scalars['Int'];
  following: Scalars['Int'];
};

export enum UserType {
  Contact = 'CONTACT',
  Picnic = 'PICNIC'
}

export type UsernameAvailabilityPayload = {
  __typename?: 'UsernameAvailabilityPayload';
  available: Scalars['Boolean'];
  username: Scalars['String'];
};

/** Pagination connection for Users list */
export type UsersConnection = {
  __typename?: 'UsersConnection';
  edges?: Maybe<Array<UsersEdge>>;
  pageInfo: PageInfo;
};

/** Pagination edge for Users list */
export type UsersEdge = {
  __typename?: 'UsersEdge';
  cursorId: Scalars['ID'];
  node: User;
  relations?: Maybe<Relations>;
};

export type UsersForDirectChat = {
  __typename?: 'UsersForDirectChat';
  edges?: Maybe<Array<UsersEdge>>;
  pageInfo: PageInfo;
};

export type VerifiedUsersResponse = {
  __typename?: 'VerifiedUsersResponse';
  pageInfo: PageInfo;
  users?: Maybe<Array<PublicProfile>>;
};

export type VideoPostContent = {
  __typename?: 'VideoPostContent';
  downloadUrl?: Maybe<Scalars['String']>;
  /** video duration in ms */
  duration?: Maybe<Scalars['Int']>;
  /** video caption */
  text: Scalars['String'];
  /** video thumbnail */
  thumbnailUrl?: Maybe<Scalars['ImageUrl']>;
  url: Scalars['String'];
};

/** input for creating video post */
export type VideoPostContentInput = {
  hlsTemplateOverride?: InputMaybe<Scalars['String']>;
  text?: InputMaybe<Scalars['String']>;
  videoFile: Scalars['Upload'];
};

export enum Visibility {
  Closed = 'CLOSED',
  Opened = 'OPENED',
  Private = 'PRIVATE'
}

/** object describing candidate for circle director vote */
export type VoteCandidate = {
  __typename?: 'VoteCandidate';
  margin: Scalars['Float'];
  position: Scalars['Int'];
  user: PublicProfile;
  userId: Scalars['ID'];
  votesCount: Scalars['Int'];
  votesPercent: Scalars['Float'];
};

/** pagination connection for VoteCandidate list */
export type VoteCandidatesConnection = {
  __typename?: 'VoteCandidatesConnection';
  edges?: Maybe<Array<VoteCandidatesEdge>>;
  pageInfo: PageInfo;
};

/** pagination edge for VoteCandidate list */
export type VoteCandidatesEdge = {
  __typename?: 'VoteCandidatesEdge';
  cursorId: Scalars['ID'];
  node: VoteCandidate;
};

export type VoteInPollInput = {
  postId: Scalars['ID'];
  variantId: Scalars['ID'];
};

export type WordsList = {
  __typename?: 'WordsList';
  pageInfo: PageInfo;
  words?: Maybe<Array<Scalars['String']>>;
};

export type SignInFirebaseMutationVariables = Exact<{
  firebaseUserID: Scalars['ID'];
  firebaseToken: Scalars['String'];
}>;


export type SignInFirebaseMutation = { __typename?: 'Mutation', signInWithFirebase: { __typename?: 'UserAuthPayload', authInfo: { __typename?: 'AuthInfo', accessToken: string }, user: { __typename?: 'PrivateProfile', id: string, email?: string | null, username: string, fullName: string, profileImage: any } } };

export type MyProfileQueryVariables = Exact<{ [key: string]: never; }>;


export type MyProfileQuery = { __typename?: 'Query', myProfile: { __typename?: 'PrivateProfile', id: string, username: string, fullName: string } };

export type GetAppTagsQueryVariables = Exact<{
  ids?: InputMaybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type GetAppTagsQuery = { __typename?: 'Query', getAppTags?: Array<{ __typename?: 'AppTag', id: string, name: string }> | null };

export type GetAppsQueryVariables = Exact<{
  ids: Array<Scalars['ID']> | Scalars['ID'];
}>;


export type GetAppsQuery = { __typename?: 'Query', getApps?: Array<{ __typename?: 'App', id: string, name: string, description: string, url: string, imageUrl: string, score: number, tags?: Array<{ __typename?: 'AppTag', id: string, name: string }> | null }> | null };

export type MyAppsQueryVariables = Exact<{ [key: string]: never; }>;


export type MyAppsQuery = { __typename?: 'Query', myApps?: Array<{ __typename?: 'App', id: string, name: string, description: string, url: string, imageUrl: string, webhookUrl?: string | null, deletedAt?: any | null, score: number, tags?: Array<{ __typename?: 'AppTag', id: string, name: string }> | null, permissions?: Array<{ __typename?: 'AppPermission', id: string, uxName: string, dxName: string, description: string, descriptors: Array<string> }> | null, subscriptions?: Array<{ __typename?: 'AppSubscription', id: string, descriptor: string }> | null }> | null };

export type CreateAppMutationVariables = Exact<{
  in: CreateAppInput;
}>;


export type CreateAppMutation = { __typename?: 'Mutation', createApp: { __typename?: 'App', id: string, name: string, description: string, url: string, imageUrl: string, createdAt: any } };

export type UpdateAppMutationVariables = Exact<{
  in: UpdateAppInput;
}>;


export type UpdateAppMutation = { __typename?: 'Mutation', updateApp: { __typename?: 'SuccessPayload', success: boolean } };

export type DeleteAppMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteAppMutation = { __typename?: 'Mutation', deleteApp: { __typename?: 'SuccessPayload', success: boolean } };

export type AddTagToAppMutationVariables = Exact<{
  appID: Scalars['ID'];
  tagID: Scalars['ID'];
}>;


export type AddTagToAppMutation = { __typename?: 'Mutation', addTagToApp: { __typename?: 'SuccessPayload', success: boolean } };

export type RemoveTagFromAppMutationVariables = Exact<{
  appID: Scalars['ID'];
  tagID: Scalars['ID'];
}>;


export type RemoveTagFromAppMutation = { __typename?: 'Mutation', removeTagFromApp: { __typename?: 'SuccessPayload', success: boolean } };

export type GetAppTokensQueryVariables = Exact<{
  appID: Scalars['ID'];
}>;


export type GetAppTokensQuery = { __typename?: 'Query', getAppTokens?: Array<{ __typename?: 'AppToken', tokenID: string, appID: string, name: string, createdAt: any, expiresAt: any, revokedAt?: any | null }> | null };

export type GenerateAppTokenMutationVariables = Exact<{
  appID: Scalars['ID'];
  name: Scalars['String'];
}>;


export type GenerateAppTokenMutation = { __typename?: 'Mutation', generateAppToken: { __typename?: 'GeneratedToken', tokenID: string, jwtToken: string } };

export type IssueAppTokenMutationVariables = Exact<{
  tokenID: Scalars['ID'];
}>;


export type IssueAppTokenMutation = { __typename?: 'Mutation', issueAppToken: { __typename?: 'IssuedToken', jwtToken: string } };

export type RevokeAppTokenMutationVariables = Exact<{
  tokenID: Scalars['ID'];
}>;


export type RevokeAppTokenMutation = { __typename?: 'Mutation', revokeAppToken: { __typename?: 'SuccessPayload', success: boolean } };

export type GetAllPermissionsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllPermissionsQuery = { __typename?: 'Query', getAppPermissions?: Array<{ __typename?: 'AppPermission', id: string, description: string, dxName: string, uxName: string, descriptors: Array<string> }> | null };

export type AddPermissionToAppMutationVariables = Exact<{
  appID: Scalars['ID'];
  permissionID: Scalars['ID'];
}>;


export type AddPermissionToAppMutation = { __typename?: 'Mutation', addPermissionToApp: { __typename?: 'SuccessPayload', success: boolean } };

export type RemovePermissionFromAppMutationVariables = Exact<{
  appID: Scalars['ID'];
  permissionID: Scalars['ID'];
}>;


export type RemovePermissionFromAppMutation = { __typename?: 'Mutation', removePermissionFromApp: { __typename?: 'SuccessPayload', success: boolean } };

export type GetAllSubscriptionsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllSubscriptionsQuery = { __typename?: 'Query', getAppSubscriptions?: Array<{ __typename?: 'AppSubscription', id: string, descriptor: string }> | null };

export type AddSubscriptionToAppMutationVariables = Exact<{
  appID: Scalars['ID'];
  subscriptionID: Scalars['ID'];
}>;


export type AddSubscriptionToAppMutation = { __typename?: 'Mutation', addSubscriptionToApp: { __typename?: 'SuccessPayload', success: boolean } };

export type RemoveSubscriptionFromAppMutationVariables = Exact<{
  appID: Scalars['ID'];
  subscriptionID: Scalars['ID'];
}>;


export type RemoveSubscriptionFromAppMutation = { __typename?: 'Mutation', removeSubscriptionFromApp: { __typename?: 'SuccessPayload', success: boolean } };

export const SignInFirebaseDocument = gql`
    mutation signInFirebase($firebaseUserID: ID!, $firebaseToken: String!) {
  signInWithFirebase(
    credentials: {accessToken: $firebaseToken, thirdPartyUserid: $firebaseUserID}
  ) {
    authInfo {
      accessToken
    }
    user {
      id
      email
      username
      fullName
      profileImage
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class SignInFirebaseGQL extends Apollo.Mutation<SignInFirebaseMutation, SignInFirebaseMutationVariables> {
    override document = SignInFirebaseDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MyProfileDocument = gql`
    query myProfile {
  myProfile {
    id
    username
    fullName
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class MyProfileGQL extends Apollo.Query<MyProfileQuery, MyProfileQueryVariables> {
    override document = MyProfileDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppTagsDocument = gql`
    query getAppTags($ids: [ID!]) {
  getAppTags(ids: $ids) {
    id
    name
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAppTagsGQL extends Apollo.Query<GetAppTagsQuery, GetAppTagsQueryVariables> {
    override document = GetAppTagsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppsDocument = gql`
    query getApps($ids: [ID!]!) {
  getApps(ids: $ids) {
    id
    name
    description
    url
    imageUrl
    tags {
      id
      name
    }
    score
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAppsGQL extends Apollo.Query<GetAppsQuery, GetAppsQueryVariables> {
    override document = GetAppsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MyAppsDocument = gql`
    query myApps {
  myApps {
    id
    name
    description
    url
    imageUrl
    webhookUrl
    deletedAt
    score
    tags {
      id
      name
    }
    permissions {
      id
      uxName
      dxName
      description
      descriptors
    }
    subscriptions {
      id
      descriptor
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class MyAppsGQL extends Apollo.Query<MyAppsQuery, MyAppsQueryVariables> {
    override document = MyAppsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateAppDocument = gql`
    mutation createApp($in: CreateAppInput!) {
  createApp(in: $in) {
    id
    name
    description
    url
    imageUrl
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateAppGQL extends Apollo.Mutation<CreateAppMutation, CreateAppMutationVariables> {
    override document = CreateAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateAppDocument = gql`
    mutation updateApp($in: UpdateAppInput!) {
  updateApp(in: $in) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateAppGQL extends Apollo.Mutation<UpdateAppMutation, UpdateAppMutationVariables> {
    override document = UpdateAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const DeleteAppDocument = gql`
    mutation deleteApp($id: ID!) {
  deleteApp(appID: $id) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DeleteAppGQL extends Apollo.Mutation<DeleteAppMutation, DeleteAppMutationVariables> {
    override document = DeleteAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AddTagToAppDocument = gql`
    mutation addTagToApp($appID: ID!, $tagID: ID!) {
  addTagToApp(appID: $appID, tagID: $tagID) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AddTagToAppGQL extends Apollo.Mutation<AddTagToAppMutation, AddTagToAppMutationVariables> {
    override document = AddTagToAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveTagFromAppDocument = gql`
    mutation removeTagFromApp($appID: ID!, $tagID: ID!) {
  removeTagFromApp(appID: $appID, tagID: $tagID) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveTagFromAppGQL extends Apollo.Mutation<RemoveTagFromAppMutation, RemoveTagFromAppMutationVariables> {
    override document = RemoveTagFromAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppTokensDocument = gql`
    query getAppTokens($appID: ID!) {
  getAppTokens(appID: $appID) {
    tokenID
    appID
    name
    createdAt
    expiresAt
    revokedAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAppTokensGQL extends Apollo.Query<GetAppTokensQuery, GetAppTokensQueryVariables> {
    override document = GetAppTokensDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GenerateAppTokenDocument = gql`
    mutation generateAppToken($appID: ID!, $name: String!) {
  generateAppToken(appID: $appID, name: $name) {
    tokenID
    jwtToken
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GenerateAppTokenGQL extends Apollo.Mutation<GenerateAppTokenMutation, GenerateAppTokenMutationVariables> {
    override document = GenerateAppTokenDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const IssueAppTokenDocument = gql`
    mutation issueAppToken($tokenID: ID!) {
  issueAppToken(tokenID: $tokenID) {
    jwtToken
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IssueAppTokenGQL extends Apollo.Mutation<IssueAppTokenMutation, IssueAppTokenMutationVariables> {
    override document = IssueAppTokenDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RevokeAppTokenDocument = gql`
    mutation revokeAppToken($tokenID: ID!) {
  revokeAppToken(tokenID: $tokenID) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RevokeAppTokenGQL extends Apollo.Mutation<RevokeAppTokenMutation, RevokeAppTokenMutationVariables> {
    override document = RevokeAppTokenDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAllPermissionsDocument = gql`
    query getAllPermissions {
  getAppPermissions {
    id
    description
    dxName
    uxName
    descriptors
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAllPermissionsGQL extends Apollo.Query<GetAllPermissionsQuery, GetAllPermissionsQueryVariables> {
    override document = GetAllPermissionsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AddPermissionToAppDocument = gql`
    mutation addPermissionToApp($appID: ID!, $permissionID: ID!) {
  addPermissionToApp(appID: $appID, permissionID: $permissionID) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AddPermissionToAppGQL extends Apollo.Mutation<AddPermissionToAppMutation, AddPermissionToAppMutationVariables> {
    override document = AddPermissionToAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemovePermissionFromAppDocument = gql`
    mutation removePermissionFromApp($appID: ID!, $permissionID: ID!) {
  removePermissionFromApp(appID: $appID, permissionID: $permissionID) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemovePermissionFromAppGQL extends Apollo.Mutation<RemovePermissionFromAppMutation, RemovePermissionFromAppMutationVariables> {
    override document = RemovePermissionFromAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAllSubscriptionsDocument = gql`
    query getAllSubscriptions {
  getAppSubscriptions {
    id
    descriptor
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetAllSubscriptionsGQL extends Apollo.Query<GetAllSubscriptionsQuery, GetAllSubscriptionsQueryVariables> {
    override document = GetAllSubscriptionsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AddSubscriptionToAppDocument = gql`
    mutation addSubscriptionToApp($appID: ID!, $subscriptionID: ID!) {
  addSubscriptionToApp(appID: $appID, subscriptionID: $subscriptionID) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AddSubscriptionToAppGQL extends Apollo.Mutation<AddSubscriptionToAppMutation, AddSubscriptionToAppMutationVariables> {
    override document = AddSubscriptionToAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveSubscriptionFromAppDocument = gql`
    mutation removeSubscriptionFromApp($appID: ID!, $subscriptionID: ID!) {
  removeSubscriptionFromApp(appID: $appID, subscriptionID: $subscriptionID) {
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveSubscriptionFromAppGQL extends Apollo.Mutation<RemoveSubscriptionFromAppMutation, RemoveSubscriptionFromAppMutationVariables> {
    override document = RemoveSubscriptionFromAppDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
import { HttpClient } from '@angular/common/http';
import { isPlatformBrowser } from '@angular/common';
import {
  APP_INITIALIZER,
  ENVIRONMENT_INITIALIZER,
  EnvironmentProviders,
  inject,
  makeEnvironmentProviders, PLATFORM_ID,
  Provider
} from '@angular/core';
import { RuntimeConfigLoaderService } from 'runtime-config-loader';
import { lastValueFrom } from 'rxjs';

import { _DEFAULT_CONFIG_URL, APP_CONFIG, CONFIG_URL, CORE_PROVIDER, LOCAL_STORAGE } from '../tokens';
import { Config, ConfigService } from './config';
import { PlatformService } from './platform.service';
import { CoreConfig } from './core.module';
import { NoopStorageAdapter } from './web-storage';


function coreProviderFactory(): void {
  inject(PlatformService);
  inject(CONFIG_URL);
  inject(RuntimeConfigLoaderService);
  inject(ConfigService);
}

export function initConfig(configSvc: ConfigService): () => Promise<Config> {
  return () => lastValueFrom(configSvc.loadConfig()).then(result => {
    console.info(`runtime config initialized`);
    return result;
  });
}

const ENVIRONMENT_CORE_PROVIDER: Provider[] = [
  {provide: CORE_PROVIDER, useFactory: coreProviderFactory},
  {
    provide: APP_INITIALIZER,
    multi: true,
    useFactory: initConfig,
    deps: [ConfigService]
  },
  {
    provide: ENVIRONMENT_INITIALIZER,
    multi: true,
    useFactory() {
      return () => inject(CORE_PROVIDER);
    }
  }
];

export function _provideCore(config?: Partial<CoreConfig>): Provider[] {
  return [
    {
      provide: LOCAL_STORAGE,
      useFactory: (platformID: string, noopStorage: NoopStorageAdapter) =>
        isPlatformBrowser(platformID) ? localStorage : noopStorage,
      deps: [PLATFORM_ID, NoopStorageAdapter]
    },
    {
      provide: _DEFAULT_CONFIG_URL,
      useValue: './assets/config.json'
    },
    {
      provide: CONFIG_URL,
      useFactory() {
        return config?.configUrl ? config.configUrl : inject(_DEFAULT_CONFIG_URL);
      },
      deps: [_DEFAULT_CONFIG_URL]
    },

    {
      provide: RuntimeConfigLoaderService,
      useFactory(http: HttpClient, configUrl: string) {
        return new RuntimeConfigLoaderService(http, {configUrl});
      },
      deps: [HttpClient, CONFIG_URL]
    },
    {
      provide: APP_CONFIG,
      useFactory: (configSvc: ConfigService) => configSvc.config,
      deps: [ConfigService]
    }
  ];
}

export function provideCore(config?: CoreConfig): EnvironmentProviders {
  return makeEnvironmentProviders([
    ..._provideCore(config),
    ENVIRONMENT_CORE_PROVIDER
  ]);
}

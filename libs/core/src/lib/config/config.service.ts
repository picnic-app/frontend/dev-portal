import { Injectable } from '@angular/core';
import { Observable, of, tap, map, take } from 'rxjs';
import { RuntimeConfigLoaderService } from 'runtime-config-loader';

import { PlatformService } from '../platform.service';
import { Config } from './config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private _loaded = false;

  constructor(private _platform: PlatformService, private _runtime: RuntimeConfigLoaderService) {
  }

  loadConfig(reload = false): Observable<Config> {
    if (!this._platform.isBrowser) {
      return of({});
    }

    if (this._loaded && !reload) {
      return this.config$;
    }

    return this._runtime.loadConfig().pipe(
      take(1),
      map(() => this._runtime.getConfig()),
      tap(() => this._loaded = true)
    );
  }

  get config(): Config {
    return this.getConfig();
  }

  get config$(): Observable<Config> {
    return this._runtime.configSubject.asObservable();
  }

  getConfig(): Config {
    if (!this._platform.isBrowser) {
      return {};
    }
    return this._runtime.getConfig();
  }
}

export type Config = Partial<ConfigValues>

export interface ConfigValues {
  apiBaseURI: string;
  fbAPIKey: string;
  envID: string;
}

import {isPlatformBrowser} from '@angular/common';
import {Inject, Injectable, PLATFORM_ID} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlatformService {

  get isBrowser(): boolean {
    return isPlatformBrowser(this.platformID);
  }

  constructor(@Inject(PLATFORM_ID) private platformID: any) {
  }
}

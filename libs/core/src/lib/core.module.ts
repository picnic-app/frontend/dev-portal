import {
  ModuleWithProviders,
  NgModule
} from '@angular/core';
import {CommonModule} from '@angular/common';
import {_provideCore} from './core.provider';

export interface CoreConfig {
  configUrl: string | string[];
}

@NgModule({
  imports: [CommonModule]
})
export class CoreModule {

  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [..._provideCore()]
    };
  }
}

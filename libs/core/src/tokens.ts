import { InjectionToken } from '@angular/core';
import { Config } from './lib/config';


export const LOCAL_STORAGE = new InjectionToken<Storage>(
  '@portal/core Local Storage'
);
export const ENV_CONFIG = new InjectionToken<any>(
  '@portal/core Env Config'
);

export const APP_CONFIG = new InjectionToken<Config>(
  '@portal/core App Config'
);

export const CONFIG_URL: InjectionToken<string> = new InjectionToken(
  '@portal/core Core Config Path'
);

export const _DEFAULT_CONFIG_URL: InjectionToken<string> = new InjectionToken(
  '@portal/core Default Core Config Path'
);

export const _CONFIG_PATH: InjectionToken<string> = new InjectionToken(
  '@portal/core Resolved Core Config Path'
);

export const CORE_PROVIDER = new InjectionToken<any>(
  '@portal/core Core Provider'
);
